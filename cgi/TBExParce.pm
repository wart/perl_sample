package TBExParce;
# ---------------------------------------------------
# use TBExParce;
# $a='1111[#aaa#]2222[#bbbb#]33333[#END#]4444[#END#]5555';
# $tbp=TBExParce->new($a);
# $tb=$tbp->parce();
#    foreach $r(keys %$tb)
#    {
#           print $r.": \n";
#           print($tb->{$r}."\n");
#    };
# ---------------------------------------------------
sub new
{
    my $class           =  shift;
    my $src             =  shift;
    my $self            =  bless {} ,$class;

       $self->{$data}   =  $src;
       $self->{len}     =  length($self->{$data});
       $self->{cor_pos} = -1;
       $self->{mode}    =  0;
       $self->{repeat}  =  1;
       $self->{rettype} =  0;
       $self->{stackdata}=[''];
       $self->{stackname}=['main'];
       $self->{stacklen}=1;
       $self->{TB}= {};
    return $self;
};

# ---------------------------------------------------

sub thread
{
    my ($self,$tag,$m,$rt,$rt2) = @_;
    my $i                       = index ($self->{$data},$tag,$self->{cor_pos});
    $self->{cor_pos}            = ($self->{cor_pos}<0) ? 0 : $self->{cor_pos};
    if($i>=0)
    {
        $self->{mode}           = $m;
        $self->{rettype}        = $rt;
        if ($self->{cor_pos}   != $i)
        {
            my $ret             = substr($self->{$data},$self->{cor_pos},$i-$self->{cor_pos});
            $self->{cor_pos}    = $i+length($tag);
            return $ret;
        }
        else
        {
            $self->{cor_pos}    = $i+length($tag);
            $self->{repeat}     = 1;
            return '';
        };
    }
    else
    {
        my $ret                 = substr($self->{$data},$self->{cor_pos});
        $self->{cor_pos}        = $self->{len};
        $self->{rettype}        = $rt2;
        return $ret;
    };
};

# ---------------------------------------------------
sub get
{
    my $self          = shift;
    my $tag_begin     = '[#';
    my $tag_end       = '#]';
    my $ret           = '';
    if ($self->{len} <= $self->{cor_pos}) { $self->{rettype}=3;return '';};
    $self->{repeat}   =1;
    while($self->{repeat}>0)
    {
        $self->{repeat}=0;
             if($self->{mode}==0){   $ret=$self->thread($tag_begin,1,0,0);}
          elsif($self->{mode}==1){   $ret=$self->thread($tag_end,  0,1,2);};
    };
    return $ret;
};

# ---------------------------------------------------
sub parce
{
    my $self  = shift;
    while($self->{rettype}<2)
    {
        my $sD=$self->get();
        if($self->{rettype}==0)
        {
            $self->{stackdata}->[$self->{stacklen}-1].=$sD;
        }
        elsif($self->{rettype}==1)
        {
            if($sD eq 'END')
            {
              if($self->{stacklen}>1)
              {
                    $self->{stacklen}-=1;

                    my $bn=$self->{stackname}->[$self->{stacklen}];
                    my $c= chop $bn;
                    if ($c eq '!')
                    {
                       $self->{stackdata}->[$self->{stacklen}-1].='{%PRM:'.$bn.'%}';
                    }else
                    {
                        $bn.=$c;
                    };

                    $self->{TB}->{$bn}=$self->{stackdata}->[$self->{stacklen}];
              }else
              {
                  $self->{rettype}=2;
              };
            }else
            {
                $self->{stackname}->[$self->{stacklen}]=$sD;
                $self->{stackdata}->[$self->{stacklen}]='';
                $self->{stacklen}+=1;
            };
        };
    };
    $self->{TB}->{'main'}=$self->{stackdata}->[0];

    return $self->{TB};
};
# ---------------------------------------------------

1;
