
$MainConnection;
#--------------------------------------------
sub DBConnect  #
{
    my $params = $_[0];#shift;
    my $constr='DBI:mysql:database='.$params->{database}.'; host='.$params->{host};
    if($params->{port} ne '')
    {
        $constr.=';port='.$params->{port};
    };
#    print($constr,$params->{login},$params->{password},"\n");
    $MainConnection =DBI->connect($constr,$params->{login},$params->{password})|| return 0;
    if ($params->{charset} ne '')
    {
        my $cs = $MainConnection->prepare("SET NAMES $params->{charset}");
           $cs->execute();
           $cs->finish();
    };
    return 1;
};
#--------------------------------------------
sub DBClose
{
    $MainConnection->disconnect();
};
#--------------------------------------------
sub DBstatement
{
    my($proccall,$query,$args,$order)=@_;
    $query =~ s/\n/ /g;
#     $REQ{sys_message}.="\n<br>\n".$proccall.' - '.$query;
 #   $DEBUG_L.="\n<br>\n".$proccall.'=='.$query;

    my $ps;
    my $E='$Q->execute(';
    my $AMD;
    if (ref($args) eq 'ARRAY')
    {
        if ($order eq '')
        {
            $AMD='AI';
            $cnt=@$args;
            for (my $i=0;$i<$cnt;++$i)
            {
                if ($i!=0){$E.=',';};
                $E.='$args->['.$i.']';
            };
        }else
        {
            $AMD='AN';
            my  $i=0;
            foreach my $v(split(',',$order))
            {
                if ($i!=0){$E.=',';};
                $E.='$args->['.$v.']';
                $i++;
            };
        };
    }elsif(ref($args) eq 'HASH')
    {
        if ($order ne '')
        {
            $AMD='HS';
            my $i=0;
            foreach my $v(split(',',$order))
            {
                if ($i!=0){$E.=',';};
                $E.='$'."args->{'".$v."'}";
                $i++;
            };
        }else
        {
            $AMD='EMP';
        };
    };

    $E.=') ||                                     out_debug("DBstatement1($proccall)($query)($AMD)$E: ".$DBI::errstr);';
    eval('$ps="'.$query.'";');                    evalERR  ("DBstatement2($proccall)($query)");
      my $Q= $MainConnection->prepare($ps)     || out_debug("DBstatement3($proccall)($ps): ".$DBI::errstr);
    eval($E);                                     evalERR  ("DBstatement4($proccall)($ps)");
#    $DEBUG_L.=$ps.'-'.$E.'-<br><br>';
    return $Q;
};
#--------------------------------------------
sub DBexec
{
    my($proccall,$query,@qargs)=@_;
    my $q=0;
    if (ref($qargs[0]) eq 'ARRAY')
    {
      $q=DBstatement($proccall,$query,$qargs[0]);
    }else{
      $q=DBstatement($proccall,$query,\@qargs);
    };
    my $rws;
    my @rs;
#    print $query."\n";
    if ($query =~ /^\s*select/ig)
    {
        $rws=$q->rows;
#        print $query."\n";

        @rs=$q->fetchrow_array();
    }else
    {
        $rws=$DBI::errstr eq '';
    };
    $q->finish();
    my @w=($rws,@rs);
    return wantarray ? @w :"@w";
};
#--------------------------------------------

#--------------------------------------------
sub DBlast_insert_id
{
    my ($e,$r)=DBexec('last_insert_id','select LAST_INSERT_ID()');
    return $r;
};
#--------------------------------------------
sub DBFetchHash($$$)
{
    my ($pref,$st,$dst)=@_;
    my $FN = $st->{NAME};
    if(my @R=$st->fetchrow_array())
    {
      my $FNm=0;
      foreach $k (@$FN)
      {
        $dst->{$pref.$k}=$R[$FNm++];
      };
      return 1;
    };
    return 0;
};
#--------------------------------------------
sub DBFetchHashQ  #($$$$$$)
{
    my ($proccall,$query,$args,$dst,$order,$pref)=@_;
    my $st=DBstatement($proccall,$query,$args,$order);
    return DBFetchHash($pref,$st,$dst);
};
#--------------------------------------------
sub DBCountPresent
{
    my($proccall,$query,@qargs)=@_;
    $q=DBstatement($proccall,$query,\@qargs);
    if($q->rows>0)
    {
        @rs=$q->fetchrow_array();
        $q->finish();

        return $rs[0]>0;
    };
    $q->finish();
    return 0;
};
#--------------------------------------------
sub DBSingleValue
{
    my($proccall,$query,@qargs)=@_;

    $q=DBstatement($proccall,$query,\@qargs);
    if($q->rows==1)
    {
        @rs=$q->fetchrow_array();
        $q->finish();
        return $rs[0];
    };
    $q->finish();
    return -1;
};
#--------------------------------------------
1;







