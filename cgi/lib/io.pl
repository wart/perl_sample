#!/usr/bin/perl
#***************
 use CGI;
 use CGI::Carp qw(fatalsToBrowser);
 use DBI;
 use Image::Magick;
 use MIME::Lite;
 $CGI::DISABLE_UPLOADS=0;
 $MAIN_CGI=new CGI;
 @prm_cookie=();
 @prm_req   =();
 @prm_sess  =();
 $DEBUG_L='';
#-------------------------------------------------
sub evalERR
{
     if ((($@ ne '')||($! ne '')||($? !=0))&&($! ne 'Bad file descriptor'))
     {
         $DEBUG_L.=$_[0].'=('.$!.')['.$?.']'.$@.'====='.$_[1]."<br>\n";
     };
     $@='';
     $!='';
     $?=0;
};
#-------------------------------------------------
sub get_query
{
    @ps=$MAIN_CGI->param();
    foreach $t(@ps)
    {
        add_prm_req($t,$MAIN_CGI->param($t));
        out_debug($t.':'.$MAIN_CGI->param($t));
    };
    $cookie_data=$ENV{'HTTP_COOKIE'};
    @c=split ('\; ',$cookie_data);
    foreach my $itm(@c)
    {
        if ($itm=~/=/)
        {
            my ($c_n,$c_v)=split('=', $itm);
            add_cookie($c_n,$c_v);
            out_debug('COC:: '.$c_n.':'.$c_v);
        };
    };
    add_prm_sess('ip',$ENV{'REMOTE_ADDR'});
};
#-------------------------------------------------
sub getREQarray($)
{
  my @c=split (',',$_[0]);
  my $i=0;
  foreach my $itm(@c)
  {
    $c[$i]=$REQ{$itm};
    $i++;
  };
  return @c;
};
#-------------------------------------------------
sub ClearREQ($)
{
  my @c=split (',',$_[0]);
  foreach my $itm(@c)
  {
     $REQ{$itm}='';
  };
};
#-------------------------------------------------
sub gen_url($)
{
 my @a=split(',',$_[0]);
 my $ord=0;
 my $ret='';
 foreach my $c (@a)
 {
   if (($REQ{$c} ne '')&&($REQ{$c} ne '0'))
   {
    if($ord>0){$ret.='&';};
    $ret.=$c.'='.$REQ{$c};
    $ord++;
   };
 };
 return $ret;
};
#-------------------------------------------------
sub get_date
{
    my $testtime = time;
    my ($sec,$min,$hour,$mday,$mon,$year) = (localtime(time))[0,1,2,3,4,5];
    $mon++;
    if($hour<10){$hour="0$hour";};
    if($min <10){$min ="0$min" ;};
    if($sec <10){$sec ="0$sec" ;};
    $year+=1900;
    add_prm_sess('datetime',"$mday/$mon/$year $hour\:$min\:$sec");
    add_prm_sess('db-time' ,"$year-$mon-$mday $hour\:$min\:$sec");
    add_prm_sess('time-file',"$year-$mon-$mday".'_'."$hour$min$sec");
    add_prm_sess('log-file',"$year-$mon-$mday.log");
    add_prm_sess('tmp-file',$hour.'_'.$min.'_'.$sec.'_'.int(rand()*1000000));
    return $SESS{'datetime'};
};
#-------------------------------------------------
sub pres_prm_abs  # list name, prm name
{
    my $p=0;
    eval('foreach $e( @'.$_[0].'){if ($e eq $_[1]){$p=1;};};');
    evalERR('pres_prm_abs');
    return $p;
};
#-------------------------------------------------
sub add_prm_abs # hashname,list name, prm name, prm value
{
    eval(
     'my $p=0;'.
     '$'.$_[0].'{$_[2]}=$_[3];'.
     'foreach $e( @'.$_[1].'){'.
     '  if ($e eq $_[2]){'.
     '    $p=1;'.
     '  };'.
     '};'.
     'if ($p!=1){'.
     '  push @'.$_[1].',$_[2];'.
     '};'
     );
    evalERR('add_prm_abs'.$_[0].' '.$_[1].' '.$_[2].' '.$_[3]);
    return 1;
};
#-------------------------------------------------
sub add_prm_req  {return add_prm_abs('REQ'    ,'prm_req'     ,$_[0],$_[1]);};
sub add_cookie   {       add_prm_abs('COOKIE' ,'prm_cookie'  ,$_[0],$_[1]);
                         add_prm_req(                         $_[0],$_[1]);};
sub add_prm_sess {return add_prm_abs('SESS'   ,'prm_sess'    ,$_[0],$_[1]);};
sub iscookie     {return pres_prm_abs('prm_cookie'  ,$_[0]);};
#-------------------------------------------------
$SESS{'sys-message'}='';
#-------------------------------------------------
sub msg_add{ $SESS{'sys-message'}.=fill_template($_[0]).'<br>'."\n";return 1;};
#-------------------------------------------------
$DEF_OUTPUT='';
#-------------------------------------------------
sub out_print
{
    my @prms=@_;
    foreach $t(@prms)
    {
        $DEF_OUTPUT.=$t;
    };
};
#------------------------------------------------------------------------------
sub out_file
{
  out_print(fill_template(LoadTemplate($_[0]),$_[1]));
};
#-------------------------------------------------
sub out_debug
{
    my @prms=@_;
    foreach $t(@prms)
    {
        $DEBUG_L.=$t.'<br>'."\n";
    };
};
#-------------------------------------------------
sub out_put
{
    print $DEF_OUTPUT;
};
#-------------------------------------------------
sub put_header($) #CT
{
    foreach $cn(@prm_cookie)
    {
        if ($REQ{$cn} ne '')
        {
            out_print("Set-Cookie: ".$cn."=".$REQ{$cn}."\; path=/\; Max-Age: $CONF{'cookie-time'}\;\n");
        };
    };
    my $t=shift;
    if ($t eq '') {$t='text/html';};
    out_print ('Content-Type: '.$t.'; charset='.$CONF{charset}."\n\n");
    return 1;
};
#-------------------------------------------------
sub fs_recurs($$$$) # path funcdir funcfile args
{
    my ($path,$funcdir,$funcfile,$arg) = @_;
    opendir(DIR,"$path");
    my @dir_mas=readdir(DIR);
    closedir(DIR);
    foreach my $line(@dir_mas)
    {
        if ("$line" ne "." && "$line" ne "..")
        {
            if ( -d "$path/$line")
            {
                fs_recurs("$path/$line",$funcdir,$funcfile,$args);
            }else{
                if (ref($funcfile) eq 'CODE'){$funcfile->($arg,"$path/$line");};
            };
        };
    };
    if (ref($funcdir) eq 'CODE') {$funcdir->($arg,"$path/$line");};
    return 1;
};
#-------------------------------------------------
sub chmod_sing{ chmod $_[0],$_[1];};
sub chmod_recursive($$)
{
    my ($chmod,$path)=@_;
    return fs_recurs($path.'/',\chmod_sing,\chmod_sing,$chmod);
};
#-------------------------------------------------
sub RDE
{
  return  fs_recurs($_[0].'.',sub {
     rmdir($_[1]);
  },sub {
     unlink($_[1]);
  },0);
};
#-------------------------------------------------
sub getFileExt
{
    my $fn=shift;
    my $ext='';
    my @ww=split(/\./,$fn);
    my $sz=@ww;
    if ($sz>1)
    {
        $ext=$ww[$sz-1];
    };
    return $ext;
};
#-------------------------------------------------
sub upload_keep_as # pname,keepname
{
    my ($pname,$keepname)=@_;
    my $fh=0;
    if($fh=$MAIN_CGI->upload($pname))
    {
        my ($br, $buf);
        open(UPL, "+>".$keepname);
        binmode(UPL);
        binmode($fh);
        while($br = read($fh, $buf, 1024))
        {
            print UPL $buf;
        };
        close UPL;
        return 1;
    };
    return 0;
};
#-------------------------------------------------------
sub my_image_resize
{
  my ($i,$s)=@_;
  my ($ox,$oy)=$i->Get('base-columns','base-rows'); #определяем ширину и высоту изображения
#  $REQ{sys_message}.=$ox.'x'.$oy;

  if(($ox<$s)||($oy<$s))
  {
    return 0;
  };
  my ($nx,$ny)=($ox>$oy)?(int(($ox/$oy)*$s),$s):($s,int(($oy/$ox)*$s));
  $i->Resize( width=>$nx, height=>$ny,filter=>Gaussian,blur=>1);
  return 1;
};
#-------------------------------------------------------
sub my_image_crop
{
  my ($i,$s)=@_;
  my ($nx,$ny)=$i->Get('base-columns','base-rows');
  my ($cx,$cy)=(int(($nx-$s)/2),int(($ny-$s)/2));
  $i->Crop(x=>$cx, y=>$cy);
  $i->Crop(size=>$s.'x'.$s);
  return 1;
};
#-------------------------------------------------------
sub my_image_crop_to_aspect
{
  my ($i,$ax,$ay)=@_;
  my $asp=$ax/$ay;
  my ($nx,$ny)=$i->Get('base-columns','base-rows');
  my ($sx,$sy)=(($nx / $ny)>$asp)?(int($ny*$asp), $ny         ):
                                  (          $nx, int($nx/$asp));

  if(($sx<$ax)||($sy<$ay))
  {
      return 0;
  };
  my ($ox,$oy)=(int(($nx-$sx)/2),int(($ny-$sy)/2));
  $i->Crop(x=>$ox,
           y=>$oy);
  $i->Crop($sx.'x'.$sy);

#$REQ{sys_message}.="<br>$ax,$ay <br> $asp <br> $nx,$ny<br> $sx,$sy<br>$ox,$oy<br>$xx<br>";

  return 1;
};
#-------------------------------------------------------
1;
