#------------------------------------------------------------------------------
# LoadTemplate
# LoadBlockTemplate
# fill_template
# ARGSTHROW($$$$) # src,dst, lst,names
# generate_pages($) # main template, item template ,item all template, div template, item cor template,item cor alltemplate, corpage,pages
# handle_paging($$)
# DBSELECTONEROW($$)
# DBSELECTHASH($$)
# DBTEMPLATEFILL
#------------------------------------------------------------------------------
sub LoadTemplate($)
{
    my $ds = int(-s $_[0]);
    my $dat='';
    if ($ds>0)
    {
        open(fd,"+<$_[0]");
        evalERR('LoadTemplate-1',$_[0]);
        sysread(fd,$dat,$ds);
        evalERR('LoadTemplate-2',$_[0]);
        close(fd);
    };
    return $dat;
};
#------------------------------------------------------------------------------
use TBExParce;
#------------------------------------------------------------------------------
sub LoadBlockTemplate
{
  my $d=LoadTemplate($_[0]);
  my $p=TBExParce->new($d);
  my $t=$p->parce();
  return  $t;
};
#------------------------------------------------------------------------------
sub TestBlockTemplate
{
   my $tb=$_[0];
   my $ret='';
    foreach $r(keys %$tb)
    {
            $ret.=$r.": \n";
            $ret.=$tb->{$r}."\n";
    };
    return $ret;
};
#------------------------------------------------------------------------------
sub TemplateInsertBlock  # main,blockname,blockdata
{
  return join($_[2],split('{%PRM:'.$_[1].'%}',$_[0]));
};
#------------------------------------------------------------------------------
sub tmpl_if
{
  my ($i,$a,$b,$p)=@_;
  return ((($p->{$i}+0)!=0)?$a:$b);
};
#------------------------------------------------------------------------------
sub tmpl_ifs
{
  my ($i,$a,$b,$p)=@_;
  return (($p->{$i} ne '')?$a:$b);
};
#------------------------------------------------------------------------------
sub tmpl_ifc
{
  my ($i,$c,$a,$b,$p)=@_;
  return (($p->{$i} eq $c)?$a:$b);
};#------------------------------------------------------------------------------
sub tmpl_ifv
{
  my ($i,$c,$a,$b,$p)=@_;
  return (($p->{$i} eq $p->{$c})?$a:$b);
};#------------------------------------------------------------------------------

sub tmpl_exec
{
  my ($exp,$prms,$rrr,@cuts) = @_;
  my $return=''.$exp;
  eval($exp);
  evalERR('tmpl_exec');
  return fill_template($return,$prms);
};
#------------------------------------------------------------------------------
sub fill_template        # (engine,params)
{
    my ($ret, $prms, @cuts) = @_;
    $ret =~ s/\<%IF:(.*?):(.*?):(.*?)\%>/&tmpl_if($1,$2,$3,$prms)/gcse;
    $ret =~ s/\<%IFS:(.*?):(.*?):(.*?)\%>/&tmpl_ifs($1,$2,$3,$prms)/gcse;
    $ret =~ s/\<%IFC:(.*?):(.*?):(.*?):(.*?)\%>/&tmpl_ifc($1,$2,$3,$4,$prms)/gcse;
    $ret =~ s/\<%IFV:(.*?):(.*?):(.*?):(.*?)\%>/&tmpl_ifv($1,$2,$3,$4,$prms)/gcse;

    $ret =~ s/\<%exec (.*?)\%>/&tmpl_exec($1,$prms)/gcse;


    $ret =~ s/\{%REQ:(.*?)%\}/$REQ{$1}/gcse;
    $ret =~ s/\{%COOKIE:(.*?)%\}/$COOKIE{$1}/gcse;
    $ret =~ s/\{%TMP:(.*?)%\}/$TMP{$1}/gcse;
    $ret =~ s/\{%CONF:(.*?)%\}/$CONF{$1}/gcse;
    if(ref($prms) eq 'HASH')
    {
      $ret =~ s/\{%PRM:(.*?)%\}/&fill_template($prms->{$1},$prms)/gcse;
 #     $ret =~ s/\{%PRM:(.*?)%\}/$prms->{$1}/gcse;
    };
    $ret =~ s/ +/ /g;
    $ret =~ s/\r+//g;
    $ret =~ s/\n+/\n/g;
    return $ret;
};
#------------------------------------------------------------------------------
sub isnotsys($)
{
  foreach my $i (
  (OneRowSelects,SubTemplDB,RetStorage,SubTemplFill,throwArgs,throwArgsAs,throwArgsUp,throwArgsUpAs,PREPAREFILL,DivTemplate,CaseArg,CaseVariant,Template,RETTEMPFLD,
CB_SQL,CB_SQLARGSARDER,FLDPREF,CB_BEFORE,CB_WORKBEF,CB_WORKAFT,CB_AFTER,CB_EMPTY,
OR_throwArgs,OR_throwArgsAs,OR_SQL,OR_SQLARGSARDER,OR_FLDPREF,OR_throwArgsUp,OR_throwArgsUpAs))
  {
    if ($_[0] eq $i)
    {return 0;};
  };
  return 1;
};

sub ARGSTHROW($$$$) # src,dst, lst,names
{
  my ($src,$dst,$lst,$nms)=@_;
  if($lst ne '')
  {
    if($lst eq '*')
    {
      foreach my $k ( keys %$src)
      {
        if (isnotsys($k))
        {
          $dst->{$nms.$k}=$src->{$k};
        };
      };
    }else
    {
      my @itms=split(',',$lst);
      if($nms ne '')
      {
        my @names=split(',',$nms);
        my $i=0;
        foreach my $it (@itms)
        {
          $dst->{$names[$i]}=$src->{$it};
          $i++;
        };
      }
      else
      {
        foreach my $it (@itms)
        {
          $dst->{$it}=$src->{$it};
        };
      };
    };
  };
};

#------------------------------------------------------------------------------
sub prepare_page($$$)
{
  my $P=shift;my $pp=shift; my $cc=shift;
  if (ref($P->{PagePrep}) eq 'CODE')
  {
    $P->{PagePrep}->($P,$pp,$cc);
  };
};
#------------------------------------------------------------------------------
sub default_prepare_page
{
  $_[0]->{link}=$_[0]->{PAGEURL}.$_[1];
  $_[0]->{cap} =$_[2];
};
#------------------------------------------------------------------------------
sub generate_pages($) # main template, item template ,item all template, div template, item cor template,item cor alltemplate, corpage,pages
{
    my $args=$_[0];
    my $cp;$cs,$ce;
    my $ret='';$ccp=-1;
    my $ord=0;

    if ($args->{t_corpage} eq 'all')
    {
      $cs=0;
      $ce=$args->{t_max_pages};
      if($ce>$args->{t_pages}-1){ $ce=$args->{t_pages}-1;};
    }else{
      $ccp=$args->{t_corpage}+0;
      my $d= int($args->{t_max_pages} /2);
      $cs=$args->{t_corpage}-$d; if ($cs<0){$cs=0;};
      $ce=$args->{t_corpage}+$d; if ($ce>($args->{t_pages}-1)){$ce=$args->{t_pages}-1;};
    };

    if ($cs>0)
    {
      prepare_page($args,0,'...');
      $ret.=fill_template($args->{t_it},$args);
      $ord++;
    };

    for ($cp=$cs;$cp<=$ce;$cp++)
    {
      if ($ord>0){$ret.=fill_template($args->{t_div},$args);};
      prepare_page($args,$cp,$cp+1);
      $ret.=fill_template(($cp==$ccp)?$args->{t_cit}:$args->{t_it},$args);
      $ord++;
    };
    if ($ce<$args->{t_pages}-1)
    {
      if ($ord>0){$ret.=fill_template($args->{t_div},$args);};
      prepare_page($args,$args->{t_pages}-1,'...');
      $ret.=fill_template($args->{t_it},$args);
      $ord++;
    };
    if ($ord>0){$ret.=fill_template($args->{t_div},$args);};

    $ret.=fill_template(($args->{t_corpage} eq 'all')?$args->{t_citAll}:$args->{t_itAll},$args);
    $args->{$args->{t_main_body}}=$ret;
    return fill_template($args->{t_main},$args);
};
#------------------------------------------------------------------------------
sub handle_paging($$)
{
  my ($args,$sel) =@_;
  my $paging=0;
  my $P=$sel->{PAGING};


  if (ref ($P) eq 'HASH')
  {
    $paging=1;
    my $St=DBstatement('handle_paging',"select count(1) from (".$sel->{CB_SQL}.") cnt",$sel,$sel->{CB_SQLARGSARDER});
    if ($St->rows==1)
    {
      my @pages=$St->fetchrow_array();
      if($pages[0]>$P->{PAGESIZE})
      {
        $P->{t_pages}=int ($pages[0] / $P->{PAGESIZE});
        if(($P->{t_pages} % $P->{PAGESIZE})>0){$P->{t_pages}++;};
        $P->{t_corpage}=$REQ{$P->{PAGEPARAM}};
        if (!($P->{t_corpage} eq 'all'))
        {
          $P->{t_corpage}+=0;
          if (($P->{t_corpage}<0)||($P->{t_corpage}>$P->{t_pages})){$P->{t_corpage}=0;};
          my $pageofs=$P->{PAGESIZE}*$P->{t_corpage};
          $sel->{CB_SQL}='select * from ('.$sel->{CB_SQL}.') ss limit '.$pageofs.','.$P->{PAGESIZE};
        };
      }else{$paging=0};
    }else{  $paging=0;};
    $St->finish();
  };
  if($paging!=0)
  {
    my $tmp=generate_pages($P);
    foreach my $a (split(',',$P->{PAGEPLACES}))
    {
      $args->{$a}=$tmp;
    };
  };
};
#------------------------------------------------------------------------------
# DBSELECTONEROW
# {
#   OR_SQL
#   OR_SQLARGSARDER
#   OR_FLDPREF
# }
sub DBSELECTONEROW($$)
{
  my ($args,$ret)=@_;
  my %RET;
  ARGSTHROW($ret,$args,$args->{OR_throwArgs},$args->{OR_throwArgsAs});
  my $Statement=DBstatement('DBSELECTONEROW',$args->{OR_SQL},$args,$args->{OR_SQLARGSARDER});
  if($Statement->rows!=0)
  {
     DBFetchHash(args->{OR_FLDPREF},$Statement,\%RET);
  };
  $Statement->finish();
  ARGSTHROW(\%RET,$ret,$args->{OR_throwArgsUp},$args->{OR_throwArgsUpAs});
};
#------------------------------------------------------------------------------
# DBSELECTHASH
# {
#   CB_WORK
#   CB_BEFORE
#   CB_AFTER
#   CB_EMPTY
#   CB_SQL
#   CB_SQLARGSARDER
#   FLDPREF
#   corrow
#   row
#   RETTEMPFLD
# }
#------------------------------------------------------------------------------
sub handle_template_jobs
{
    my $jobenv = shift;
    my $jobs   = shift;
   if( ref($jobs) eq 'CODE')
   {
     $jobs->($jobenv);
   }elsif( ref($jobs) eq 'ARRAY')
   {
     foreach my $j ( @{$jobs})
     {
        if( ref($j) eq 'CODE')
        {
            $j->($jobenv);
        };
     };
   };
};
#------------------------------------------------------------------------------
sub DBSELECTHASH($$)
{
   my ($args,$top)=@_;

   ARGSTHROW($top,$args,$args->{throwArgs},$args->{throwArgsAs});
   $args->{RETTEMPFLD}='';
   my $Statement=DBstatement('DBSELECTHASH',$args->{CB_SQL},$args,$args->{CB_SQLARGSARDER});

   $args->{corrow}=0;
   $args->{corrow1}=1;
   $args->{rows}=$Statement->rows;
   if($args->{rows}!=0)
   {
      handle_template_jobs($args,$args->{CB_BEFORE});
      while(DBFetchHash(args->{FLDPREF},$Statement,$args)>0)
      {
         handle_template_jobs($args,$args->{CB_WORKBEF});
         $args->{RETTEMPFLD}.=DBTEMPLATEFILL($args);
         handle_template_jobs($args,$args->{CB_WORKAFT});
         $args->{corrow}++;
         $args->{corrow1}++;
      };
      handle_template_jobs($args,$args->{CB_AFTER});
  }
  else
  {
      handle_template_jobs($args,$args->{CB_EMPTY});

  };
#  print '-----------------------------'."\n".$args->{RETTEMPFLD}.'-----------------------------'."\n";

  $Statement->finish();
  ARGSTHROW($args,$top,$args->{throwArgsUp},$args->{throwArgsUpAs});
  return $args->{RETTEMPFLD};
};
#------------------------------------------------------------------------------
# DBTEMPLATEFILL
# {
#   DBSELECTHASH
#   {
#     CB_SQL
#     CB_SQLARGSARDER
#
#     CB_WORK
#     CB_BEFORE
#     CB_AFTER
#     CB_EMPTY
#
#     FLDPREF
#     --------------
#     corrow
#     row
#   }
#   RetStorage
#   DivTemplate
#   Template
#   throwArgs
#   throwArgsAs
#   OneRowSelects =
#   (
#      DBSELECTONEROW
#      {
#        OR_SQL
#        OR_SQLARGSARDER
#        OR_FLDPREF
#        --------------
#        OR_throwArgs
#        OR_throwArgsAs
#        OR_throwArgsUp
#        OR_throwArgsUpAs
#      }
#   )
#   SubTemplDB=(DBTEMPLATEFILL [, ...])
#   CaseArg
#   CaseVariant={ name:DBTEMPLATEFILL,...}
# }

#------------------------------------------------------------------------------
sub DBTEMPLATEFILL($)
{
    my $args=$_[0];
    my $MYargs=$_[1];
    my $ret='';

   # TestBlockTemplate($args);

    # ONE ROW DATA
    foreach my $CORS (@{$args->{OneRowSelects}})
    {
       if (ref($CORS) eq 'HASH')
       {
          DBSELECTONEROW($CORS,$args);
       };
    };
    # SUB DB TEMPLATES HANDLE
    foreach my $CST (@{$args->{SubTemplDB}})
    {
       if (ref($CST) eq 'HASH')
       {
         handle_paging($args,$CST);
         $args->{$CST->{RetStorage}}=DBSELECTHASH($CST,$args);
       };
    };

    # SUB FILL TEMPLATES HANDLE
    foreach my $CSF (@{$args->{SubTemplFill}})
    {
       if (ref($CSF) eq 'HASH')
       {
          ARGSTHROW($args,$CSF,$CSF->{throwArgs},$CSF->{throwArgsAs});
          $args->{$CSF->{RetStorage}}=DBTEMPLATEFILL($CSF,$args);
       };
    };
    #Prepare Fill Template
    handle_template_jobs($args,$args->{PREPAREFILL});
    #Fill Template
    if (($args->{corrow}!=0)&&($args->{DivTemplate} ne ''))
    {
      $ret=fill_template($args->{DivTemplate},$args);
    };

    if ($args->{CaseArg} ne '')
    {
       my $scase= $args->{CaseVariant}->{$args->{$args->{CaseArg}}};
       if( ref($scase) eq 'HASH')
       {
         ARGSTHROW($args,$scase,$scase->{throwArgs},$scase->{throwArgsAs});
         $ret.=DBTEMPLATEFILL($scase,$args);
       }else
       {
            $ret.=fill_template($args->{Template},$args);
       };
    }else
    {
         $ret.=fill_template($args->{Template},$args);
    };
    ARGSTHROW($args,$MYargs,$args->{throwArgsUp},$args->{throwArgsUpAs});
    return $ret;
};
#------------------------------------------------------------------------------
1;
