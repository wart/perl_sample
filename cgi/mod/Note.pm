package Note;
# ---------------------------------------------------
# use TBExParce;
# $a='1111[#aaa#]2222[#bbbb#]33333[#END#]4444[#END#]5555';
# $tbp=TBExParce->new($a);
# $tb=$tbp->parce();
#    foreach $r(keys %$tb)
#    {
#           print $r.": \n";
#           print($tb->{$r}."\n");
#    };
# ---------------------------------------------------
sub new
{
    my $class           =  shift;
    my $src             =  shift;
    my $self            =  bless $src ,$class;
    return $self;
};

# ---------------------------------------------------
#   $Ttiny_MCE->{news_editor}.
#   fill_template($Tpages->{add_form},
sub genList()
{
  my $t = shift;  my $l = $t->{List}; my $g = $t->{Gen}; my $a = $t->{Action}; my $pg=$t->{Paging};

  return ::DBTEMPLATEFILL(
  {
    Template        => ($t->isAdm()!=0)?$l->{BodyAdm}:$l->{Body},
    $t->{TParams}->{HiddenAdd} =>  ::gen_hiddens({   $g->{ParamID} => $g->{List},$a->{ParamID} => $a->{Add}}),

    $t->{TParams}->{LinkEdit}   =>'?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{Edit}.'&'.$t->{ParamID}.'=',
    $t->{TParams}->{LinkDelete} =>'?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{List}.'&'.$t->{Action}->{ParamID}.'='.$t->{Action}->{Delete}.'&'.$t->{ParamID}.'=',

    SubTemplDB      =>
    [{
       RetStorage   => $l->{ITEMS_RET},
       Template     => ($t->isAdm()!=0)?$l->{ItemAdm}:$l->{Item},
       PAGING       => ($pg->{Size}>0) ?(::gen_page_prm('?'. $g->{ParamID}.'='. $g->{List}.'&'.
                                        $pg->{Param}  .'=',$pg->{Size})):'',
       CB_SQL       => $l->{SQL},
       this         => $t,
       PREPAREFILL  => sub
       {
         my $a=shift;
         my $t=$a->{this};
         $a->{$t->{TParams}->{LinkView}}   = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{View}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}};
         $a->{$t->{TParams}->{LinkEdit}}   = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{Edit}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}};
         $a->{$t->{TParams}->{LinkDelete}} = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{List}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}}.'&'.$t->{Action}->{ParamID}.'='.$t->{Action}->{Delete};
       }
     }]
   });
};
#--------------------------------------------
sub isAdm
{
  my $t=shift;
  if(ref($t->{AdminChk}) eq 'CODE')
  {
     return $t->{AdminChk}->();
  };
  return 0;
};
#--------------------------------------------

sub genView
{
  my $t = shift;
   return ::DBTEMPLATEFILL(
   {
     Template      => $t->{View}->{Body},
     this          => $t,
     OneRowSelects => [
                       {
                         $t->{ParamID}   => $::REQ{$t->{ParamID}},
                         OR_SQL          => $t->{View}->{SQL},
                         OR_SQLARGSARDER => $t->{ParamID},
                         OR_throwArgsUp  => '*',
                       }
                      ],
     PREPAREFILL => sub
     {
       my $a=shift;
       my $t=$a->{this};
         $a->{$t->{TParams}->{LinkEdit}}   = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{Edit}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}};
         $a->{$t->{TParams}->{LinkDelete}} = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{List}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}}.'&'.$t->{Action}->{ParamID}.'='.$t->{Action}->{Delete};
     }
   });
};
#--------------------------------------------
#      return $Ttiny_MCE->{news_editor}.fill_template(,$P);
sub genEdit
{
  my $t = shift;

   if($t->isAdm()!=0)
   {
       if(::DBSingleValue('Note::genEdit',$t->{Edit}->{ChkSQL},$::REQ{$t->{ParamID}})>0)
       {
          return ::DBTEMPLATEFILL(
          {
             Template      => $t->{Edit}->{Body},
             this          => $t,
             $t->{TParams}->{HiddenEdt} =>  ::gen_hiddens({   $t->{Gen}->{ParamID} => $t->{Gen}->{Edit},
                                                                   $t->{Action}->{ParamID} => $t->{Action}->{Edit},
                                                                   $t->{ParamID}}),
             OneRowSelects => [
                       {
                         $t->{ParamID}   => $::REQ{$t->{ParamID}},
                         OR_SQL          => $t->{Edit}->{SQL},
                         OR_SQLARGSARDER => $t->{ParamID},
                         OR_throwArgsUp  => '*'
                       }
                      ],
             PREPAREFILL => sub
             {
                my $a=shift;
                my $t=$a->{this};
                $a->{$t->{TParams}->{LinkDelete}} = '?'.$t->{Gen}->{ParamID}.'='.$t->{Gen}->{List}.'&'.$t->{ParamID}.'='.$a->{$t->{SQLID}}.'&'.$t->{Action}->{ParamID}.'='.$t->{Action}->{Delete};
             }
          });
       }else
       {
         if(ref($t->{Edit}->{OnNotFound}) eq 'CODE'){return $t->{Edit}->{OnNotFound}->(); }else{ return 'Object Not Exists.';};
       };
   }
   else
   {
     if(ref($t->{Edit}->{OnAccDeny}) eq 'CODE'){ return $t->{Edit}->{OnAccDeny}->();}else{ return 'Access Deny.';};
   };
};
#--------------------------------------------
sub chkNulls #
{
  my $a = shift; my $sm=shift;
  foreach $g(keys %{$a})
  {
    if( $::REQ{$g} eq '')
    {
      $::REQ{$sm}.=$a->{$g};
      return 0;
    };
  };
  return 1;
};
#--------------------------------------------
sub Call
{
  if(ref($_[0]) eq 'CODE'){return $_[0]->(); };
  return $_[1];
};
#--------------------------------------------
sub doEdit
{
   my $t = shift;
   if($t->isAdm()!=0){
       if(::DBSingleValue('Note::doEdit',$t->{Edit}->{ChkSQL},$::REQ{$t->{ParamID}})>0){
          if(chkNulls($t->{Edit}->{Checks},$t->{SysMsgParamID})!=0){
             my @ua =::getREQarray($t->{Edit}->{UpdSQLArgs});
             ::DBexec('Note::doEdit',$t->{Edit}->{UpdSQL},\@ua);
             $::REQ{$t->{SysMsgParamID}}.=Call($t->{Edit}->{OnDone},'Object Updated.');
          };
       }else{$::REQ{$t->{SysMsgParamID}}.=Call($t->{Edit}->{OnNotFound},'Object Not Exists.');};
   }else{    $::REQ{$t->{SysMsgParamID}}.=Call($t->{Edit}->{OnAccDeny},'Access Deny.');};
};
#--------------------------------------------
sub doAdd
{
   my $t = shift;
   if($t->isAdm()!=0){
      if(chkNulls($t->{Add}->{Checks},$t->{SysMsgParamID})!=0)
      {
            my @ua =::getREQarray($t->{Add}->{SQLArgs});
            ::DBexec('Note::doEdit',$t->{Add}->{SQL},\@ua);
            $::REQ{$t->{SysMsgParamID}}.=Call($t->{Add}->{OnDone},'Object Added.');
      };
   }else{    $::REQ{$t->{SysMsgParamID}}.=Call($t->{Add}->{OnAccDeny},'Access Deny.');};
};
#--------------------------------------------
sub doDelete
{
   my $t = shift;
   if($t->isAdm()!=0){
      my @ua =::getREQarray($t->{Delete}->{SQLArgs});
      ::DBexec('Note::doDelete',$t->{Delete}->{SQL},\@ua);
      $::REQ{$t->{SysMsgParamID}}.=Call($t->{Delete}->{OnDone},'Object Delete.');
   }else{    $::REQ{$t->{SysMsgParamID}}.=Call($t->{Delete}->{OnAccDeny},'Access Deny.');};
};
#--------------------------------------------
1;