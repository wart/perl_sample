

#$ACTS{upload_avatar} =\&act_upload_avatar;
#$ACTS{upd_user_info}=\&act_upd_user_info;

#$ACTS{addtown}=\&act_addtown;
#$ACTS{adduref}=\&act_adduref;

#$ACTS{reg}=\&act_reg;
#$ACTS{acc_activate}=\&act_acc_activate;
#$ACTS{pwdrecovery}=\&act_pwdrecovery;
$ACTS{changepwd}    =\&act_changepwd;

$ACTS{paper_add}=\&act_papers_add;
$ACTS{paper_edit}=\&act_papers_edit;
$ACTS{paper_del}=\&act_papers_delete;

$PROJPREACTION={};
#=========================================================
sub  handle_actions
{
   if ( ref($PROJPREACTION) eq 'CODE')
   {
       $PROJPREACTION->();
   };
   if ( ref($ACTS{$REQ{'action'}}) eq 'CODE')
   {
      return $ACTS{$REQ{'action'}}->();
   };
   $DEBUG_L.="---ACTION:$REQ{action}---";
};
#=========================================================
1;