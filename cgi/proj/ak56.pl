#--------------------------------------------------------------
  require "./proj/ak_actions.pl";
  require "./proj/gen/ak56/cat.pl";
  require "./proj/gen/ak56/subcat.pl";
  require "./proj/gen/ak56/typeref.pl";
  require "./proj/gen/ak56/search.pl";

  require "./proj/gen/ak56/manscat.pl";
  require "./proj/gen/ak56/man.pl";
  require "./proj/gen/ak56/mantref.pl";

  require "./proj/gen/ak56/prods.pl";
  require "./proj/gen/ak56/pdetail.pl";
  require "./proj/gen/ak56/tr_grp.pl";

  require "./proj/gen/ak56/pimage.pl";

  require "./proj/gen/ak56/catprm.pl";
  require "./proj/gen/ak56/bucket.pl";
  require "./proj/helper/ak56/select.pl";
  require "./proj/helper/ak56/submans.pl";
  require "./proj/helper/ak56/side.pl";
  require "./proj/helper/ak56/menu.pl";
#--------------------------------------------------------------
  add_cookie('jsess','guest');
  add_prm_req('basket_id',0);
#--------------------------------------------------------------
  $GEN{main}=\&CatList;
  $GEN{cat}=\&CatList;
  $GEN{subcat}=\&SubCatList;
  $GEN{typeref}=\&TypeRefList;

  $GEN{manscat}=\&ManSubCatList;
  $GEN{mantref}=\&ManTypeRefList;
  $GEN{man}=\&ManList;

  $GEN{prods}=\&ProdsList;
  $GEN{pdetail}=\&ProdDetail;
  $GEN{catprm}=\&catprm;

  $GEN{tr_grp}=\&tr_grp;
  $GEN{pimage}=\&ProdImage;

  $GEN{bucket}=\&BucketView;

  $GEN{search}=\&SearchProdsList;


  $GENNAV{cat}     = 'page';
  $GENNAV{subcat}  = 'page,cat';
  $GENNAV{typeref} = 'page,subcat';
  $GENNAV{manscat} = 'man,subcat,typeref';
  $GENNAV{mantref} = 'man,subcat';
  $GENNAV{man}     = 'page';
  $GENNAV{prods}   = 'grp,man,typeref,page';
  $GENNAV{pdetail} = 'prod';
  $GENNAV{catprm}  = 'page,typeref';
  $GENNAV{tr_grp}  = 'page,typeref';
  $GENNAV{pimage}  = 'prod,pic_id';
  $GENNAV{bucket}  = '';
  $GENNAV{search}  = 'page,filter';



#--------------------------------------------------------------
sub init_basketB
{
     add_cookie('jsess',rand()*1000000);
     DBexec('init_basketB','insert into t_basket(skey,dtc,ip) values(?,sysdate(),?)',$COOKIE{jsess},$SESS{'ip'});
     $REQ{basket_id}=DBlast_insert_id();

};
#--------------------------------------------------------------
sub init_basket
{
   DBexec('init_basket1','delete from t_basket where dtc<DATE_SUB(sysdate(),interval 15 minute)');
   if ($COOKIE{jsess} eq 'guest')
   {
      init_basketB();
   }
   else
   {
      if(DBSingleValue('init_basket2','select count(1) from t_basket where skey=? and ip=?',$COOKIE{jsess},$SESS{'ip'})>0)
      {
        $REQ{basket_id}=DBSingleValue('init_basket3','select id from t_basket where skey=? and ip=?',$COOKIE{jsess},$SESS{'ip'});
        DBexec('init_basket4','update t_basket set dtc=sysdate() where skey=? and ip=?',$COOKIE{jsess},$SESS{'ip'});
      }else
      {
         init_basketB();
      };
   };
};
#--------------------------------------------------------------
$PROJPREACTION= sub
{

 if(isAdmin())
 {
   $ACTS{add_cat}=\&add_cat;
   $ACTS{upd_cat}=\&upd_cat;
   $ACTS{del_cat}=\&del_cat;

   $ACTS{add_subcat}=\&add_subcat;
   $ACTS{upd_subcat}=\&upd_subcat;
   $ACTS{del_subcat}=\&del_subcat;

   $ACTS{add_typeref}=\&add_typeref;
   $ACTS{del_typeref}=\&del_typeref;
   $ACTS{upd_typeref}=\&upd_typeref;

   $ACTS{add_man}=\&add_man;
   $ACTS{upd_man}=\&upd_man;
   $ACTS{del_man}=\&del_man;

   $ACTS{add_catprm}=\&add_catprm;
   $ACTS{upd_catprm}=\&upd_catprm;
   $ACTS{del_catprm}=\&del_catprm;
   $ACTS{del_prm}=\&del_prm;
   $ACTS{add_prm}=\&add_prm;

   $ACTS{add_prod}=\&add_prod;
   $ACTS{del_prod}=\&del_prod;
   $ACTS{upd_prod}=\&upd_prod;
   $ACTS{upd_prod_prm}=\&upd_prod_prm;

   $ACTS{add_grp}=\&add_grp;
   $ACTS{del_grp}=\&del_grp;
   $ACTS{upd_grp}=\&upd_grp;


   $ACTS{add_pod}=\&add_prodofday;
   $ACTS{del_pod}=\&del_prodofday;

   $ACTS{add_pic}=\&add_pic;
   $ACTS{del_pic}=\&del_pic;

   # trying picture upload
   {
     add_prm_req('UPPIC_ID',0);
     $REQ{'UpPicCI'}+=0;
     if(($REQ{'UpPicPI'} ne '')&&($REQ{'UpPicCI'}!=0))
     {
        $REQ{'UPPIC_ID'}=ak_upload_picture($REQ{'UpPicPI'},$REQ{'UpPicCI'},$CONF{htdocs_path}.$CONF{pic_path});

     };
   };
   #-----------------------
#=========================================================
 }
 else
 {
   $ACTS{buck_add}=\&buck_add;
   $ACTS{buck_del}=\&buck_del;
   $ACTS{dobucket}=\&dobucket;
   if ($SESS{'ip'} ne '')
   {
     init_basket();
   };
 };
 if($REQ{gen} eq ''){$REQ{gen}='main';};

 if($REQ{gen} eq 'pdetail')
 {
  add_prm_req(typeref,DBSingleValue('getProdtype_for_side','select id_type_grp from t_products where id=?',$REQ{prod}));
 };
};
#--------------------------------------------------------------
sub ak_upload_picture($$$)  #fparam ,path
{
    my ($fparam,$piccat,$save_path)=@_;$piccat+=0;
    my $ext      = getFileExt($REQ{$fparam});
    my $tmp_file ='./tmp/'.$SESS{'tmp-file'}.'.'.$ext;
    if ($ext ne ''){
        if(upload_keep_as($fparam,$tmp_file)){
            DBexec('upload_picture','insert into t_pictures(visible,extens,cat) values(1,?,?)',$ext,$piccat);
            my $id=DBlast_insert_id();
            if ($id!=0){
                if ($piccat!=7){
                  my $image = Image::Magick->new;
                  my $x = $image->Read($tmp_file);
                  @modes= ({},[80],[80],[80],[80],[600,304,180,80]);
                  if ( ref($modes[$piccat]) eq 'ARRAY')
                  {
                     my ($nx,$ny)=$image->Get('base-columns','base-rows');
                     my ($sx,$sy)=(($nx/$ny)>1) ?  ($ny,$ny) :($nx,$nx);
                     my ($ox,$oy)=(int(($nx-$sx)/2),int(($ny-$sy)/2));
                     $image->Crop(x=>$ox,y=>$oy);
                     $image->Crop($sx.'x'.$sy);
                     foreach my $g(@{$modes[$piccat]})
                     {
                        $image->Resize( width=>$g, height=>$g,filter=>Gaussian,blur=>1);
                        $x = $image->Write($save_path."/$g/$id.$ext");
                     };
                  };
                };
                unlink($tmp_file);
                return $id;
            };
        };
    };
    unlink($tmp_file);
    return 0;
};
#--------------------------------------------------------------
@sq=('select id_type_grp,name from t_products where id=?',
     'select owner,name FROM t_prod_type_ref_grp where id=?',
     'select owner,name FROM t_prod_type_ref where id=?',
     'select owner,name FROM t_prod_type_subcat where id=?',
     'SELECT name FROM t_prod_type_cat where id=?'
     );
#--------------------------------------------------------------
$BREADCRUMB{pdetail}= sub
{
  my ($r,$n0,$i1,$n1,$i2,$n2,$i3,$n3)=DBexec('BreadCrumb_pdetail',
  'select p.name,t.id,t.name,sc.id,sc.name,c.id,c.name from t_products p,t_prod_type_ref_grp tg,t_prod_type_ref t,t_prod_type_subcat sc,t_prod_type_cat c where p.id_type_grp=tg.id and tg.owner=t.id and t.owner=sc.id and c.id=sc.owner and p.id=?',$REQ{prod});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     { link => '?gen=subcat&cat='    .$i3  , cap => $n3 },
                     { link => '?gen=typeref&subcat='.$i2  , cap => $n2 },
                     { link => '?gen=prods&typeref=' .$i1  , cap => $n1 },
                     {                                       cap => $n0 }
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{tr_grp}=
$BREADCRUMB{prods}= sub
{
  my ($r,$n1,$i2,$n2,$i3,$n3)=DBexec('BreadCrumb_prods',
  'select t.name,sc.id,sc.name,c.id,c.name from t_prod_type_ref t,t_prod_type_subcat sc,t_prod_type_cat c where t.owner=sc.id and c.id=sc.owner and t.id=?',$REQ{typeref});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     { link => '?gen=subcat&cat='    .$i3  , cap => $n3 },
                     { link => '?gen=typeref&subcat='.$i2  , cap => $n2 },
                     {                                       cap => $n1 }
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{tr_grp}= sub
{
  my ($r,$n1,$i2,$n2,$i3,$n3)=DBexec('BreadCrumb_prods',
  'select t.name,sc.id,sc.name,c.id,c.name from t_prod_type_ref t,t_prod_type_subcat sc,t_prod_type_cat c where t.owner=sc.id and c.id=sc.owner and t.id=?',$REQ{typeref});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     { link => '?gen=subcat&cat='    .$i3  , cap => $n3 },
                     { link => '?gen=typeref&subcat='.$i2  , cap => $n2 },
                     { link => '?gen=prods&typeref=' .$REQ{typeref},cap => $n1 },
                     {                                       cap => '������'}
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{catprm}= sub
{
  my ($r,$n1,$i2,$n2,$i3,$n3)=DBexec('BreadCrumb_prods',
  'select t.name,sc.id,sc.name,c.id,c.name from t_prod_type_ref t,t_prod_type_subcat sc,t_prod_type_cat c where t.owner=sc.id and c.id=sc.owner and t.id=?',$REQ{typeref});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     { link => '?gen=subcat&cat='    .$i3  , cap => $n3 },
                     { link => '?gen=typeref&subcat='.$i2  , cap => $n2 },
                     { link => '?gen=prods&typeref=' .$REQ{typeref},cap => $n1 },
                     {                                       cap => '��������������'}
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{typeref}= sub
{
  my ($r,$n2,$i3,$n3)=DBexec('BreadCrumb_typeref',
  'select sc.name,c.id,c.name from t_prod_type_subcat sc,t_prod_type_cat c where c.id=sc.owner and sc.id=?',$REQ{subcat});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     { link => '?gen=subcat&cat='    .$i3  , cap => $n3 },
                     {                                       cap => $n2 }
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{subcat}= sub
{
  my ($r,$n3)=DBexec('BreadCrumb_subcat',
  'select c.name from t_prod_type_cat c where c.id=?',$REQ{cat});
  return nav_array( [{ link => '?'                         , cap => '�������'},
                     { link => '?gen=cat'                  , cap => '��������'},
                     {                                       cap => $n3 }
                    ]);
};
#--------------------------------------------------------------
$BREADCRUMB{cat}= sub
{
  return nav_array( [{ link => '?'     , cap => '�������'},
                     {  cap => '��������'} ]);
};
#--------------------------------------------------------------
$BREADCRUMB{search}= sub
{
  return nav_array( [{ link => '?'     , cap => '�������'},{  cap => '�����: '.$REQ{filter}} ]);
};
#--------------------------------------------------------------
$BREADCRUMB{bucket}= sub
{
  return nav_array( [{ link => '?'     , cap => '�������'},
                     {                   cap => '��� �������'} ]);
};
#--------------------------------------------------------------
1;