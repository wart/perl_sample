sub isOper
{
  return isAdmin();
};
#--------------------------------------
sub add_cat()
{
  $REQ{'UPPIC_ID'}+=0;
  my ($ok,@ret)=DBexec('add_cat','insert into t_prod_type_cat(shortname,name,pic_id)values(?,?,?)',$REQ{cat_shortname},$REQ{cat_name},$REQ{UPPIC_ID});
  $REQ{cat}=DBlast_insert_id();
};
#--------------------------------------
sub  upd_cat()
{
  $REQ{'UPPIC_ID'}+=0;
  if ($REQ{'UPPIC_ID'}!=0)
  {
    my ($ok,@ret)=DBexec('upd_cat1','update t_prod_type_cat    set shortname=?,name=?,pic_id=? where id=?',$REQ{cat_shortname},$REQ{cat_name},$REQ{UPPIC_ID},$REQ{cat_id});
  }else{
    my ($ok,@ret)=DBexec('upd_cat2','update t_prod_type_cat    set shortname=?,name=? where id=?',$REQ{cat_shortname},$REQ{cat_name},$REQ{cat_id});
  };
};
#--------------------------------------
sub  del_cat()
{
  $REQ{cat_id}+=0;
  if($REQ{cat_id}!=0)
  {
    my ($ok,@ret)=DBexec('del_cat','delete from t_prod_type_cat where id=?',$REQ{cat_id});
  };
};
#--------------------------------------
sub  add_subcat()
{
  $REQ{'UPPIC_ID'}+=0;
  my ($ok,@ret)=DBexec('add_subcat','insert into t_prod_type_subcat(name,owner,pic_id)values(?,?,?)',$REQ{subcat_name},$REQ{subcat_owner},$REQ{UPPIC_ID});
  $REQ{subcat}=DBlast_insert_id();
};
#--------------------------------------
sub  upd_subcat()
{
  $REQ{'UPPIC_ID'}+=0;
  if ($REQ{'UPPIC_ID'}!=0)
  {
    my ($ok,@ret)=DBexec('upd_subcat1','update t_prod_type_subcat set name=?,pic_id=? where id=?',$REQ{subcat_name},$REQ{UPPIC_ID},$REQ{subcat_id});
  }else{
    my ($ok,@ret)=DBexec('upd_subcat2','update t_prod_type_subcat set name=? where id=?',$REQ{subcat_name},$REQ{subcat_id});
  };
};
#--------------------------------------
sub  del_subcat()
{
  $REQ{subcat_id}+=0;
  if($REQ{subcat_id}!=0)
  {
    my ($ok,@ret)=DBexec('del_subcat','delete from t_prod_type_subcat where id=?',$REQ{subcat_id});
  };
};
#--------------------------------------
sub  add_typeref()
{
  $REQ{'UPPIC_ID'}+=0;
  my ($ok,@ret)=DBexec('add_typeref','insert into t_prod_type_ref(name,owner,pic_id)values(?,?,?)',$REQ{typeref_name},$REQ{typeref_owner},$REQ{UPPIC_ID});
  $REQ{typeref}=DBlast_insert_id();
};
#--------------------------------------
sub  del_typeref()
{
  $REQ{typeref_id}+=0;
  if($REQ{typeref_id}!=0)
  {
    my ($ok,@ret)=DBexec('del_typeref','delete from t_prod_type_ref where id=?',$REQ{typeref_id});
  };
};
#--------------------------------------
sub  upd_typeref()
{
  $REQ{'UPPIC_ID'}+=0;
  if ($REQ{'UPPIC_ID'}!=0)
  {
    my ($ok,@ret)=DBexec('upd_typeref1','update t_prod_type_ref set name=?,pic_id=? where id=?',$REQ{typeref_name},$REQ{UPPIC_ID},$REQ{typeref_id});
  }else{
    my ($ok,@ret)=DBexec('upd_typeref2','update t_prod_type_ref set name=? where id=?',$REQ{typeref_name},$REQ{typeref_id});
  };
};
#--------------------------------------
sub add_prodofday
{
   my ($ok,@ret)=DBexec('add_prodofday','insert into t_prodofday(pid) values(?)',$REQ{prodofday}+0);
};
#--------------------------------------
sub del_prodofday
{
   my ($ok,@ret)=DBexec('del_prodofday','delete from t_prodofday where pid=?',$REQ{prodofday}+0);
};
#--------------------------------------
sub chk_prodofday
{
  return (DBSingleValue('chk_prodofday','select count(1) from t_prodofday where pid=?',$_[0])>0);
};
#--------------------------------------
sub  add_man()
{
  $REQ{'UPPIC_ID'}+=0;
  my ($ok,@ret)=DBexec('add_man','insert into t_man(name,note,pic_id) values(?,?,?)',$REQ{man_name},$REQ{man_note},$REQ{UPPIC_ID});
  $REQ{man}=DBlast_insert_id();
};
#--------------------------------------
sub  upd_man()
{
  $REQ{'UPPIC_ID'}+=0;
  if ($REQ{'UPPIC_ID'}!=0)
  {
    my ($ok,@ret)=DBexec('upd_man1','update t_man set name=?, note=?,pic_id=? where id=?',$REQ{man_name},$REQ{man_note},$REQ{UPPIC_ID},$REQ{man});
  }else{
    my ($ok,@ret)=DBexec('upd_man2','update t_man set name=?, note=? where id=?',$REQ{man_name},$REQ{man_note},$REQ{man});
  };
};
#--------------------------------------
sub  del_man()
{
  $REQ{man_id}+=0;
  if($REQ{man_id}!=0)
  {
    my ($ok,@ret)=DBexec('del_man','delete from t_man where id=?',$REQ{man_id});
  };
};
#--------------------------------------
sub upd_prod()
{
  $REQ{'UPPIC_ID'}+=0;
  if ($REQ{'UPPIC_ID'}!=0)
  {
    my ($ok,@ret)=DBexec('upd_prod1','update t_products set name=?,id_man=?,pricev=?,pricep=?,pic_id=?,d24=? where id=?',
                            $REQ{prod_name},$REQ{prod_man}+0,$REQ{prod_pricev}+0,$REQ{prod_pricep}+0, $REQ{UPPIC_ID},$REQ{prod_d24}+0,$REQ{prod}+0);
  }else{
    my ($ok,@ret)=DBexec('upd_prod2','update t_products set name=?,id_man=?,pricev=?,pricep=?,d24=? where id=?',
                            $REQ{prod_name},$REQ{prod_man}+0,$REQ{prod_pricev}+0,$REQ{prod_pricep}+0,$REQ{prod_d24}+0,$REQ{prod}+0);
  };
};
#--------------------------------------
sub upd_prod_prm()
{
 my $prod=$REQ{prod}+0;
  out_debug('prod: '.$prod);

  foreach my $a (keys %REQ)
  {
    if( $a=~ /prm_(.*)/)
    {
      my $pid=$1+0;
      out_debug('----');
      out_debug('pid:'.$pid);
      out_debug('a:'.$a);
      out_debug('prm:'.$REQ{$a});

      my ($ok,@ret)=DBexec('upd_prod_prm','select count(1) cnt from t_prod_param where id_prm=? and id_prod=?',$pid,$prod);

      if ($REQ{$a} ne '')
      {
        if ($ret[0]!=0)
        {
          my ($ok,@r)=DBexec('upd_prod_prm','update t_prod_param set val=? where id_prm=? and id_prod=?',$REQ{$a},$pid,$prod);
        }
        else
        {
          my ($ok,@r)=DBexec('upd_prod_prm','insert  into t_prod_param(id_prm,id_prod,val) value(?,?,?)',$pid,$prod,$REQ{$a});
        };
      }
      else
      {
        my ($ok,@r)=DBexec('upd_prod_prm','delete from t_prod_param where id_prm=? and id_prod=?',$pid,$prod);
      };
    };
  };
};
#--------------------------------------
sub add_catprm()
{
 my $tr=$REQ{typeref}+0;

  if ($REQ{catprm_name} ne '')
  {
    my ($ok,@r)=DBexec('add_catprm','insert into t_prod_prm_cat(id_ref_typeref,name,pic_id) values(?,?,?)',$tr,$REQ{catprm_name},0);
  };
};
#--------------------------------------
sub upd_catprm()
{
  my $tr=$REQ{typeref}+0;

  if ($REQ{catprm_name} ne '')
  {
    my ($ok,@r)=DBexec('upd_catprm1','update t_prod_prm_cat set name=? where id=?',$REQ{catprm_name},$REQ{catprm_id}+0);
  };
  foreach my $a (keys %REQ)
  {
    if( $a=~ /prm_(.*)/)
    {
      my $pid=$1+0;

      if ($REQ{$a} ne '')
      {
        my ($ok,@r)=DBexec('upd_catprm2','update t_prod_prm_ref set name=?, is_main=? where id=?',$REQ{$a},$REQ{'prmchk_'.$pid}+0,$pid);
      };
    };
  };
};
#--------------------------------------
sub del_catprm()
{
my ($ok,@r)=DBexec('del_catprm','delete from t_prod_prm_cat where id=?',$REQ{catprm_id}+0);
};
#--------------------------------------
sub del_prm()
{
  my ($ok,@r)=DBexec('del_prm','delete from t_prod_prm_ref where id=?',$REQ{prm_id}+0);

};
#--------------------------------------
sub add_prm()
{
  if ($REQ{prm_name} ne '')
  {
    my ($ok,@r)=DBexec('add_prm','insert into t_prod_prm_ref(id_cat,id_pic,name,is_main) values(?,?,?,?)',
                          $REQ{catprm_id}+0,0,$REQ{prm_name},$REQ{prm_ismain}+0);
  };
};
#--------------------------------------
sub add_prod()
{
  $REQ{'UPPIC_ID'}+=0;

  if ($REQ{prod_name} ne '')
  {
    my ($ok,@ret)=DBexec('add_prod','insert into t_products(id_man,id_type_grp,name,pricev,pricep,pic_id) value(?,?,?,?,?,?)',

    1,  #$REQ{prod_man}+0,

    $REQ{prod_grp}+0,$REQ{prod_name},$REQ{prod_pricev}+0,$REQ{prod_pricep}+0,$REQ{UPPIC_ID});
    $REQ{prod}=DBlast_insert_id();
  }else{$REQ{gen}='';};
};
#--------------------------------------
sub del_prod()
{
 my $prod= $REQ{prod_id}+0;
 my ($ok,@r)=DBexec('del_prod1','delete from t_prod_param  where id_prod=?',$prod);
  ($ok,@r)=DBexec('del_prod2','delete from t_products  where id=?',$prod);
};
#--------------------------------------
sub add_grp()
{
  my $tr=$REQ{typeref}+0;
  if ($tr>0)
  {
    if ($REQ{grp_name} ne '')
    {
      my ($ok,@r)=DBexec('add_grp','insert into t_prod_type_ref_grp(name,owner) values(?,?)',$REQ{grp_name},$tr);
    };
  };
};
#--------------------------------------
sub del_grp()
{
    my ($ok,@r)=DBexec('del_grp','delete from t_prod_type_ref_grp where id=?',$REQ{grp_id}+0);
};
#--------------------------------------
sub upd_grp()
{

  foreach my $a (keys %REQ)
  {
    if( $a=~ /grp_(.*)/)
    {
      my $gid=$1+0;

      if ($REQ{$a} ne '')
      {
        my ($ok,@r)=DBexec('upd_grp','update t_prod_type_ref_grp set name=? where id=?',$REQ{$a},$gid);
      };
    };
  };
};
#--------------------------------------
sub add_pic()
{
 my $prod=$REQ{prod}+0;
 $REQ{'UPPIC_ID'}+=0;
 if(($REQ{'UPPIC_ID'}>0)&&($prod>0))
 {
   my ($ok,@r)=DBexec('add_pic','insert into t_prod_image(prod_id,pic_id) values(?,?)',$prod,$REQ{'UPPIC_ID'});
   $REQ{pic_id}=$REQ{'UPPIC_ID'};
 };
};
#--------------------------------------
sub del_pic()
{
  my $prod=$REQ{prod}+0;
  my $pic_id=$REQ{pic_id}+0;
  my ($ok,@r)=DBexec('del_pic','delete from  t_prod_image where prod_id=? and pic_id=?',$prod,$pic_id);

 $REQ{pic_id}=0;
};
#--------------------------------------
sub buck_add
{
  my $prod=$REQ{prod}+0;
  my $bid=$REQ{basket_id}+0;
  if (($prod>0)&&($bid>0))
  {
    DBexec('buck_add','insert into t_basket_data(pid,bid) values(?,?)',$prod,$bid);
  };
};
#--------------------------------------
sub buck_del
{
  my $prod=$REQ{prod}+0;
  my $bid=$REQ{basket_id}+0;
  if (($prod>0)&&($bid>0))
  {
    DBexec('buck_del','delete from t_basket_data where pid=? and bid=?',$prod,$bid);
  };
};
#--------------------------------------
use MIME::Lite;

sub dobucket
{
       my $bk=$REQ{basket_id}+0;
       my %top;
        my %SM;
        $SM{Template}='<tr><td><a href="'.$CONF{URL}.'?gen=pdetail&prod_id={%PRM:prod_id%}">{%PRM:prod_name%}</a></td><td>{%PRM:prod_pricev%}</td></tr>'."\n";

        $SM{CB_SQL}= 'SELECT prod_id,prod_pricev,prod_pricep,prod_name,prod_pic FROM v_basket_detail where basket_id='.$bk;

#'  SELECT prod.id as prod_id, prod.pricev as prod_pricev, prod.pricep as prod_pricep, prod.name as prod_name, '.
#"       concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as prod_pic ".'  FROM t_products as prod  join t_basket_data bsk on bsk.pid=prod.id '.
#'       left join t_pictures pic on prod.pic_id=pic.id and pic.visible=1 where bsk.bid='.$bk;





my $table='���: '.   $REQ{fio}.'<br>'.
          '�����: '.$REQ{adress}.'<br>'.
          '�������: '.  $REQ{tel}.'<br>'.
          'E-Mail: '. $REQ{email}.'<br><br>'.
'<table border=1>'.DBSELECTHASH(\%SM,\%top)."</table>\n";

$DEBUG_L.=$table;
$msg = MIME::Lite->new (
     From =>'����� � ����� <oxdf_wlad@www.mail.ru>',
     To =>'oxdf_wlad@www.mail.ru',
     Subject =>'Zakaz N'.$bk,
     Data =>$table,
     Type    => 'text/html; charset=windows-1251'
#     Encoding =>'CP1251'
   );
$msg->send;
};
#--------------------------------------
1;
