
$sql_admin=LoadBlockTemplate('./sql/admin.bsq');
$Tadmin  = LoadBlockTemplate('./interface/'.$CONF{interface}.'/admin');
#--------------------------------------------------------------
sub form_admin_add #navcap , mtpl
{
  if(isAdmin())
  {
    my $F=
    {
        Template   => $Tadmin->{addform},
        formcap    => $_[0],
        ADDFIELDS  => gen_hiddens({gen=>'',action=>$REQ{gen}}),
        SubTemplDB =>
        [{
                RetStorage      => 'dataplace',
                Template        => $Tadmin->{dataSingle},
                CB_SQL          => $sql_admin->{$_[1]}
        }]
    };
    return DBTEMPLATEFILL($F),
  }else
  {
    return center_part(0);
  };
};
#--------------------------------------------------------------
sub gen_addtown
{
  return form_admin_add('���������� ������',list_town);
};
#--------------------------------------------------------------
sub gen_adduref
{
  return form_admin_add('���������� ���� �������������',list_uref);
};
#--------------------------------------------------------------
sub action_admin_add
{
  if(isAdmin())
  {
    if($REQ{add_cap} ne '')
    {
        DBexec('action_admin_add',$sql_admin->{$_[1]},$REQ{add_cap});
        $REQ{sys_message}=$_[0].' ��������';
        $REQ{add_cap}='';
    }else
    {
        $REQ{sys_message}='�� ��������� ������������';
    };
  };
};
#--------------------------------------------------------------
sub act_addtown
{
  action_admin_add('�����',add_town);
};
#--------------------------------------------------------------
sub act_adduref
{
  action_admin_add('��� ������������',add_uref);
};
#--------------------------------------------------------------
1;