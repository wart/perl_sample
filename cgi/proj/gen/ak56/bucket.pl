$Tbucket  = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/bucket');

#=====================================================
sub prepare_bucket_link
{
  my $c=DBSingleValue('prepare_bucket_link','select count(1) from t_basket_data where pid=? and bid=?',$_[0]->{$_[0]->{throwArgs}},$REQ{basket_id}+0);
  if($c>0)
  {
    $_[0]->{Template}   = $Tbucket->{bucket_del};
    $_[0]->{ActionLink} = $_[0]->{LinkTmp}.$_[0]->{$_[0]->{throwArgs}}.'&action=buck_del';
  }
  else
  {
    $_[0]->{Template}   = $Tbucket->{bucket_add};
    $_[0]->{ActionLink} = $_[0]->{LinkTmp}.$_[0]->{$_[0]->{throwArgs}}.'&action=buck_add';
  };
};
#=====================================================
sub gen_bucket_link #  prod_id param name ,  gen   ,   RetStorage
{
  return ($USRPRM{isOper}==0)?
  {
     throwArgs     => ($_[0] eq '')?'prod_id':$_[0],
     RetStorage    => 'BUCKET',#$_[2],
     LinkTmp       => '?gen='.$_[1].'&'.gen_url('typeref,man,grp,filter,page').'&prod=',
     PREPAREFILL => \&prepare_bucket_link
  }:'';
};
#=====================================================
sub BucketView #catid
{
  $bk=$REQ{basket_id}+0;
  if ($bk==0) {return '';};
  my $c=DBSingleValue('BucketView','select count(1) from t_basket_data bsk  where bsk.bid='.$bk);

  if ($c==0) {return '������� �����';};

  #-----------------------
  return
  DBTEMPLATEFILL(
  {
    DivTemplate => '',
    Template    => $Tbucket->{BODY}.(($c>0)?$Tbucket->{SubmitForm}:''),
    hiddens     => ($ccc>0)?gen_hiddens('gen,action:dobucket'):'',
    SubTemplDB  => [{
#                     PAGING          =>  gen_page_prm('?'.gen_url('gen').'&page=',10),
                      DivTemplate     => $Tbucket->{PROD_DIV},
                      Template        => $Tbucket->{PROD_DESC},
                      RetStorage      => 'body',
                      KILLSMBFLDS     => 'prod_name',
                      TOTP            => 0,
                      TOTV            => 0,
                      PREPAREFILL     => sub{
                                            $_[0]->{link_prod}='?gen=pdetail&prod='.$_[0]->{prod_id};
                                            $_[0]->{link_prod_pic}=$CONF{pic_path_180}.$_[0]->{prod_pic};
                                         },
                      CB_WORKAFT      => sub{
                                           $_[0]->{TOTP}+=$_[0]->{prod_pricep};
                                           $_[0]->{TOTV}+=$_[0]->{prod_pricev};
                                         },
                      throwArgsUp     => 'TOTP,TOTV',
                      CB_SQL          => 'SELECT prod_id,prod_pricev,prod_pricep,prod_name,prod_pic FROM v_basket_detail where basket_id='.$bk,
                      SubTemplFill => [gen_bucket_link('prod_id','bucket','BUKET'),
                                       put_prices()
                                       ],

                      SubTemplDB      => [{
                                            RetStorage      => 'ATTRS',
                                            DivTemplate     => $Tbucket->{ATTR_DIV},
                                            Template        => $Tbucket->{ATTR_DESC},
                                            CB_SQL          => 'SELECT prm_name, prm_val from v_prod_main_params where prod_id=? limit 10',
                                            CB_SQLARGSARDER => 'prod_id',
                                            FLDPREF         => '',
                                            throwArgs       => 'prod_id'
                                         }]
                   }]
  });
};
#---------------------------------------------------------
1;
