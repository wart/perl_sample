$Tcat  = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/cat');
#---------------------------------------------------------
sub CatList()
{
  return DBTEMPLATEFILL(
  {
    DivTemplate => '',
    Template    => isOper() ? $Tcat->{BODY_ADM} : $Tcat->{BODY_USR},
    link_delcat => '?gen=cat&action=del_cat&cat_id=',
    hiddens     => isOper()?gen_hiddens('MAX_FILE_SIZE:1000000,action:add_cat,UpPicCI:1,UpPicPI:cat_pic,gen:subcat'):'',
    SubTemplDB  => [{
                      RetStorage      => 'body',
                      linksubcat      => '?gen=subcat&cat=',
                      pic_path        => $CONF{pic_path_80},
                      CB_SQLARGSARDER => '',
                      FLDPREF         => '',
                      PAGING          => gen_page_prm('?gen=cat&page=',10),
                      DivTemplate     => $Tcat->{ITEM_DIV},
                      Template        => isOper() ? $Tcat->{ITEM_ADM} : $Tcat->{ITEM_USR},
                      PREPAREFILL     => isOper()?\&kill_smb  :'',
                      KILLSMBFLDS     => isOper()? 'cat_name' :'',
                      CB_SQL          => 'select cat_id, cat_name, cat_pic from v_cat_list',
                      SubTemplDB      => [{
                                            link_typeref    => '?gen=typeref&subcat=',
                                            RetStorage      => 'ITEMS',
                                            CB_SQLARGSARDER => 'cat_id',
                                            FLDPREF         => '',
                                            throwArgs       => 'cat_id',
                                            throwArgsAs     => '',
                                            DivTemplate     => $Tcat->{SI_DIV},
                                            Template        => $Tcat->{SI_ITEM},
                                            CB_SQL          => 'select subcat_id, subcat_name, subcat_pic from v_subcat_list   where subcat_owner=? limit 5'
                                         }]
                   }]
    });
};
#---------------------------------------------------------
1;
