$Tcatprm  = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/catprm');
#=====================================================
sub catprm_ismain($)
{
 $_[0]->{checked}=($_[0]->{prm_main}==1)?' checked':'';
};
#=====================================================
sub catprm($) #catid
{
   my $tr =$REQ{typeref}+0;
   return DBTEMPLATEFILL(
   {
      link_subcat       => '?gen=typeref&subcat=',
      link_delprm       => '?gen=catprm&typeref='.$tr.'&action=del_prm&prm_id=',
      link_delcatprm    => '?gen=catprm&typeref='.$tr.'&action=del_catprm&catprm_id=',
      pic_path          => $CONF{pic_path_80},
      hiddens           => gen_hiddens('action:add_catprm,gen,typeref,page'),
      Template          => $Tcatprm->{BODY},
      DivTemplate       => '',

      OneRowSelects     => [{
                               OR_SQL         => 'SELECT typeref_name, typeref_owner, typeref_pic FROM v_prod_type_ref_detail where typeref_id='.$tr,
                               OR_throwArgsUp => 'typeref_name,typeref_pic,typeref_owner'
                           }],
      SubTemplDB        => [{
                               RetStorage     => 'body',
                               CB_SQL         => 'SELECT catprm_id, catprm_name, catprm_pic FROM v_prod_prm_cat_list where catprm_typeref='.$tr,
                               DivTemplate    => $Tcatprm->{CAT_DIV},
                               Template       => $Tcatprm->{CAT_ITEM},
                               hiddens_upd    => gen_hiddens('action:upd_catprm,gen,typeref,page'),
                               hiddens_add    => gen_hiddens('action:add_prm,gen,typeref,page'),
                               PAGING         => gen_page_prm('?gen=catprm&typeref='.$tr.'&page=',4),
                               PREPAREFILL    => \&kill_smb,
                               KILLSMBFLDS    => 'catprm_name',
                               SubTemplDB     => [{
                                                    RetStorage      => 'params',
                                                    CB_SQLARGSARDER => 'catprm_id',
                                                    FLDPREF         => '',
                                                    throwArgs       => 'catprm_id',
                                                    CB_SQL          => 'SELECT prm_id, prm_name, prm_main, prm_pic from v_prod_prm_ref_list where prm_cat_id=?',
                                                    PREPAREFILL     => \&kill_smb,
                                                    KILLSMBFLDS     => 'prm_name:prm_name_del',
                                                    CB_WORKBEF      => \&catprm_ismain,
                                                    DivTemplate     => $Tcatprm->{PRM_DIV},
                                                    Template        => $Tcatprm->{PRM_ITEM}
                                                 }]
                           }]
   });
};
#---------------------------------------------------------
1;
