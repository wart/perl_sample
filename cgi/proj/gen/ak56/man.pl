$Tman = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/man');
#---------------------------------------------------------------
sub prep_link_mantref
{
  $_[0]->{link_mantref} = '?gen=mantref&man='.$_[0]->{man_id}.'&subcat='.$_[0]->{subcat_id};
};
#---------------------------------------------------------------
sub ManList()
{
  return DBTEMPLATEFILL(
  {
    Template    =>  isOper() ?$Tman->{BODY_ADM} : $Tman->{BODY_USR},

    $Tman->{BODY_BEGIN}.((isOper())?$Tman->{BODY_ADM}:'').$Tman->{BODY_END},

    hiddens     => (isOper())?gen_hiddens('MAX_FILE_SIZE:1000000,action:add_man,UpPicCI:4,UpPicPI:man_pic,gen:manscat'):'',
    linkdel     => (isOper())?'?gen=man&action=del_man&man_id=':'',
    DivTemplate => '',
    SubTemplDB  => [{
                      RetStorage      => 'body',
                      pic_path        => $CONF{pic_path_80},
                      link_manscat    => '?gen=manscat&man=',
                      CB_SQLARGSARDER => '',
                      FLDPREF         => '',
                      PAGING          => gen_page_prm('?gen=man&page=',10),
                      DivTemplate     => $Tman->{ITEM_DIV},
                      Template        => isOper() ? $Tman->{ITEM_ADM} : $Tman->{ITEM_USR},
                      CB_SQL          => 'select man_id,man_name,man_pic from v_man_list',
                      SubTemplDB      => [{
                                            RetStorage      => 'ITEMS',
                                            DivTemplate     => $Tman->{SUB_DIV},
                                            Template        => $Tman->{SUB_ITEM},
                                            CB_SQL          => 'select subcat_id,subcat_name,subcat_pic from v_man_subcats where man_id=?',
                                            PREPAREFILL     => \&prep_link_mantref,
                                            CB_SQLARGSARDER => 'man_id',
                                            FLDPREF         => '',
                                            throwArgs       => 'man_id',
                                            throwArgsAs     => ''
                                         }]
                   }]
  });
};
#---------------------------------------------------------
1;
