$Tmanscat = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/manscat');
#---------------------------------------------------------------
sub ManSubCatList($) #catid
{
  #-----------------------
   my $man    = $REQ{man}    +0;
   my $subcat = $REQ{subcat} +0;
   my $tr     = $REQ{typeref}+0;
   return DBTEMPLATEFILL(
   {
     Template      => isOper() ? $Tmanscat->{BODY_ADM} : $Tmanscat->{BODY_USR},
     hiddens       => (isOper())?gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_man,UpPicCI:4,UpPicPI:man_pic,gen:manscat,man:'.$man):'',
     DivTemplate   => '',
     man_id        => $man,
     pic_path      => $CONF{pic_path_80},
     OneRowSelects => [{
                         OR_SQL          => 'select man_name,man_note,man_pic from v_man_detail where man_id=?',
                         OR_SQLARGSARDER => 'man_id',
                         OR_FLDPREF      => '',
                         OR_throwArgs    => 'man_id',
                         OR_throwArgsUp  => 'man_name,man_pic,man_note'
                      }],
     SubTemplDB    => [{
                         RetStorage      => 'body',
                         CB_SQLARGSARDER => 'man_id',
                         throwArgs       => 'man_id',
                         pic_path        => $CONF{pic_path_80},
                         FLDPREF         => '',
                         CB_SQL          => 'select subcat_id,subcat_name,subcat_pic from v_man_subcats where man_id=?',
                         link_tmp        => '?gen=mantref&man='.$man.'&subcat=',
                         PREPAREFILL     => sub
                                            {
                                              $_[0]->{link_mantref}=$_[0]->{link_tmp}.$_[0]->{subcat_id};
                                            },
                         DivTemplate     => $Tmanscat->{ITEM_DIV},
                         Template        => $Tmanscat->{ITEM_BODY},
                         SubTemplDB      => [{
                                               RetStorage      => 'ITEMS',
                                               CB_SQLARGSARDER => 'subcat_id,man_id',
                                               throwArgs       => 'subcat_id,man_id',
                                               FLDPREF         => '',
                                               throwArgsAs     => '',
                                               link_tmp        => '?gen=prods&man='.$man.'&typeref=',
                                               PREPAREFILL     => sub
                                                                  {
                                                                    $_[0]->{link_prods}=$_[0]->{link_tmp}.$_[0]->{typeref_id};
                                                                  },
                                               CB_SQL          => 'SELECT typeref_id, typeref_name, typeref_pic from v_man_prod_type_ref_list'.
                                                                  ' where typeref_subcat_id=? and typeref_man_id=? limit 5',
                                               DivTemplate     => $Tmanscat->{SUB_DIV},
                                               Template        => $Tmanscat->{SUB_BODY}
                                            }]
                      }]
   });
};
#---------------------------------------------------------
1;
