  $Tmantref = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/mantref');
  #-----------------------
sub ManTRL_afterrec($){
  my $a=$_[0];
  my %PRM=%$a;

  if ((($PRM{corrow}+1) % 2)==0)
  {
    $PRM{RETTEMPFLD}.=$Tmantref->{LN};
  }else{
    $PRM{RETTEMPFLD}.=$Tmantref->{LN2};
  };
  return \%PRM;
};
  #-----------------------
sub ManTRL_beforerec($){
  my $a=$_[0];
  my %PRM=%$a;
  if ((($PRM{corrow}+1) % 2)==0)
  {
  }else{
    $PRM{RETTEMPFLD}.=$Tmantref->{BEFORE};
  };
  return \%PRM;
};
  #-----------------------
sub ManTRL_after($){
  my $a=$_[0];
  my %PRM=%$a;
  if ((($PRM{corrow}+1) % 2)==0)
  {
  }else{
     $PRM{RETTEMPFLD}.=$Tmantref->{AFTER};
  };
    return \%PRM;
};
  #-----------------------
sub ManTypeRefList($) #catid
{
   my $man    = $REQ{man}   +0;
   my $subcat = $REQ{subcat}+0;
  #-----------------------
  return DBTEMPLATEFILL(
     {
       Template      => $Tmantref->{BODY},
       pic_path      => $CONF{pic_path_80},
       DivTemplate   => '',
       subcat_id     => $subcat,
       man_id        => $man,
       OneRowSelects => [{
                           OR_SQL          => 'select subcat_name,subcat_pic from v_prod_type_subcat_detail where subcat_id=?',
                           OR_SQLARGSARDER => 'subcat_id',
                           OR_FLDPREF      => '',
                           OR_throwArgs    => 'subcat_id',
                           OR_throwArgsUp  => 'subcat_name,subcat_pic'
                         },
                         {
                           OR_SQL          => 'select man_name,man_pic from v_man_detail where man_id=?',
                           OR_SQLARGSARDER => 'man_id',
                           OR_FLDPREF      => '',
                           OR_throwArgs    => 'man_id',
                           OR_throwArgsUp  => 'man_name,man_pic'
                        }],
       SubTemplDB    => [{
                           RetStorage      => 'body',
                           Template        => $Tmantref->{ITEM},
                           link_tmp        => '?gen=prods&man='.$man.'&typeref=',
                           PREPAREFILL     => sub
                                              {
                                                $_[0]->{link_prods}=$_[0]->{link_tmp}.$_[0]->{typeref_id};
                                                $_[0]->{link_typeref_pic}=$CONF{pic_path_80}.$_[0]->{typeref_pic};
                                              },

                           CB_SQL          => 'SELECT typeref_id, typeref_name, typeref_pic from v_man_prod_type_ref_list where typeref_subcat_id=? and typeref_man_id=?',
                           CB_SQLARGSARDER => 'subcat_id,man_id',
                           throwArgs       => 'subcat_id,man_id',
                           FLDPREF         => '',
                           CB_WORKBEF      => \&ManTRL_beforerec,
                           CB_WORKAFT      => \&ManTRL_afterrec,
                           CB_AFTER        => \&ManTRL_after
                        }]
     });
};
#---------------------------------------------------------
1;
