$Tpdetail = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/pdetail');
#=====================================================
#sub pdetail_d24($)
#{
# $_[0]->{checked24}=($_[0]->{prod_24}==1)?' checked':'';
#};
#=====================================================
sub ProdDetail($) #catid
{
  my $prod=$REQ{prod}+0;
  #-----------------------
   return DBTEMPLATEFILL(
   {
     Template               => (isOper() ? $Tpdetail->{BODY_ADMIN} :$Tpdetail->{BODY_USER}),
     hiddens                => gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_prod,UpPicCI:5,UpPicPI:prod_pic,gen:pdetail,prod'),
     hiddens_prm            => gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_prod_prm,gen:pdetail,prod'),
     hiddens_pic            => gen_hiddens('MAX_FILE_SIZE:1000000,action:add_pic,gen:pdetail,prod,UpPicCI:5,UpPicPI:pic_pic'),
     DivTemplate            => '',
     pic_path               => $CONF{pic_path_80},
     link_images            => '?gen=pimage&prod='.$prod,
     link_pic_del           => '?'.getGENUrl().'&action=del_pic&pic_id=',
     put_price              => put_prices(),
     link_bucket            => gen_bucket_link('prod_id'),
     PREPAREFILL            => [#\&pdetail_d24,
                                sub
                                {
                                  $REQ{typeref}=$_[0]->{prod_type_ref};
                                  $REQ{man}=$_[0]->{prod_man};
                                  $REQ{grp}=$_[0]->{prod_type_grp};
                                  $_[0]->{link_list}= '?gen=prods&'.gen_url('typeref,man,grp').'&prod='.$prod;
                                }
                               ],
     SubTemplFill           => [ select_gen('prod_man',-1,'place_man','','SELECT man.id as sql_id,man.name as sql_name from t_man man', 0,'prod_man','VALUE',''),
                                put_prices(),
                                isOper()?'':gen_bucket_link('prod_id','pdetail')
                              ],
     OneRowSelects          => [{
                                  OR_SQLARGSARDER  => 'prod_id',
                                  OR_FLDPREF       => '',
                                  prod_id          => $prod,
                                  OR_throwArgsUp   => 'prod_name,man_pic,prod_pricep,prod_pricev,prod_man,prod_id,prod_24,prod_type_grp,prod_type_ref',
                                  OR_SQL           => 'select prod_id, prod_name, prod_pricev, prod_pricep, prod_man, prod_24, prod_type_grp,prod_type_ref,'.
                                                      ' man_pic from v_products_detail where prod_id=?'
                               },
                               {
                                  OR_SQLARGSARDER  => 'prod_id',
                                  OR_FLDPREF       => '',
                                  prod_id          => $prod,
                                  OR_throwArgsUp   => 'prod_pic',
                                  OR_SQL           => 'select pic_fname as prod_pic from v_prod_image where prod_id=? limit 1'
                               },
                               ],
                                  PREPAREFILL     => sub{
                                                   $_[0]->{pic_large} =$CONF{pic_path_600}.$_[0]->{prod_pic};
                                                   $_[0]->{pic_medium}=$CONF{pic_path_80}.$_[0]->{prod_pic};
                                                     },

     SubTemplDB             => [{
                                  CB_SQLARGSARDER  => '',
                                  FLDPREF          => '',
                                  throwArgs        => '',
                                  DivTemplate      => $Tpdetail->{ATTR_DIV},
                                  Template         => $Tpdetail->{ATTR_ITEM},
                                  RetStorage       => 'ATTRS',
                                  CB_SQL           => 'SELECT prm_name,prm_val FROM v_prod_main_params where prod_id='.$prod.' limit 10',
                                },
                                {
                                  RetStorage      => 'IMAGES',
                                  prod_id         => $prod,
                                  CB_SQL          => 'select pic_fname,pic_id from v_prod_image where prod_id='.$prod,
                                  DivTemplate     => $Tpdetail->{IMAGE_DIV},
                                  Template        => isOper() ?$Tpdetail->{IMAGE_ITEM_ADM} :$Tpdetail->{IMAGE_ITEM},
                                  link_pic_delB   => '?'.getGENUrl().'&action=del_pic&pic_id=',
                                  PREPAREFILL     => sub{
                                                   $_[0]->{pic_large} =$CONF{pic_path_600}.$_[0]->{pic_fname};
                                                   $_[0]->{pic_medium}=$CONF{pic_path_80}.$_[0]->{pic_fname};
                                                   $_[0]->{link_pic_del}=$_[0]->{link_pic_delB}.$_[0]->{pic_id};
                                                     }
                                },
                                {
                                  pic_path         => $CONF{pic_path_80},
                                  RetStorage       => 'FULLATTRS',
                                  Template         => isOper() ? $Tpdetail->{PC_ADMIN} : $Tpdetail->{PC_USER},
                                  hiddens          => gen_hiddens(',action:upd_prod_prm,gen:pdetail,prod'),
                                  CB_SQL           => isOper() ? 'SELECT prmcat_id, prmcat_name, prmcat_pic from v_prm_cat_prod_admin where prod_id='.$prod
                                                               : 'SELECT prmcat_id, prmcat_name, prmcat_pic FROM v_prm_cat_prod_user where prod_id='.$prod,
                                  SubTemplDB       => [{
                                                         DivTemplate     => '',
                                                         FLDPREF         => '',
                                                         pic_path        => $CONF{pic_path_80},
                                                         RetStorage      => 'params',
                                                         throwArgs       => 'prmcat_id',
                                                         CB_SQLARGSARDER => 'prmcat_id',
                                                         PREPAREFILL     => \&roword,
                                                         Template        => isOper() ? $Tpdetail->{PRMS_ADMIN} :$Tpdetail->{PRMS_USER},
                                                         CB_SQL          => isOper() ? 'SELECT prm_id,prm_name,prm_pic,prm_val from v_prod_params where prm_cat_id=? and prod_id='.$prod
                                                                                     : 'SELECT prm_id,prm_name,prm_pic,prm_val from v_prod_params where prm_filled is not null and prm_cat_id=? and prod_id='.$prod
                                                      }]
                                }]
     });
};
#=====================================================
sub put_prices
{
  return
  {
    Template     => ((isOper())&&($REQ{gen} eq 'pdetail')) ? $Tpdetail->{PRICE_ADM} : $Tpdetail->{PRICE},
    throwArgs    => 'prod_id,prod_24,prod_pricev,prod_pricep',
    RetStorage   => 'PRICE',
    PREPAREFILL  => sub
                    {
                       if(chk_prodofday($_[0]->{prod_id}))
                       {
                         $_[0]->{pod_link_cap}=$Tpdetail->{pod_cap_del};
                         $_[0]->{pod_link}='?gen=pdetail&action=del_pod&prodofday='.$_[0]->{prod_id}.'&prod='.$_[0]->{prod_id};
                       }
                       else
                       {
                         $_[0]->{pod_link_cap}=$Tpdetail->{pod_cap_add};
                         $_[0]->{pod_link}='?gen=pdetail&action=add_pod&prodofday='.$_[0]->{prod_id}.'&prod='.$_[0]->{prod_id};
                       };
                    }
  };
};
#---------------------------------------------------------
1;
