$Tpimage = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/pimage');
#=====================================================
sub ProdImagePrep($)
{
  $_[0]->{NUM}++;
  if ($_[0]->{CORPIC} == $_[0]->{pic_id}){
    $_[0]->{Template} = $Tpimage->{LIST_ACTIVE};
  }else{
    $_[0]->{Template} = $Tpimage->{LIST_ITEM};
    $_[0]->{lick_pic} = $_[0]->{link_tmp}.$_[0]->{pic_id};
  };
};
#=====================================================
sub ProdImage($) #catid
{
  my $prod   = $REQ{prod}  +0;
  my $pic_id = $REQ{pic_id}+0;
  #-----------------------  CHECKING PICTURE(S)
  if(DBSingleValue('ProdImage1','select count(1) from t_prod_image where prod_id=?',$prod)>0){
    if(DBSingleValue('ProdImage2','select count(1) from t_prod_image where prod_id=? and pic_id=?',$prod,$pic_id)<1){
      $pic_id=DBSingleValue('ProdImage2','select pic_id from t_prod_image where prod_id=? limit 1',$prod);
    };
  }else{
    $pic_id=0;
  };
  #----------------------- BASE FORM WITH PROD DESC
  return DBTEMPLATEFILL(
  {
    pic_path      => $CONF{pic_path_600},
    Template      => $Tpimage->{BODY},
    IMAGE_PLACE   => $Tpimage->{EMPTY},
    link_desc     => '?gen=pdetail&'.gen_url('typeref,man,grp').'&prod='.$prod,
    prod_id       => $prod,
    OneRowSelects => [{ OR_throwArgsUp  => 'prod_name,prod_pic,man_pic,prod_pricep,prod_pricev,prod_man,prod_id',
                        OR_SQL          => 'SELECT prod_id, prod_name, prod_pricev, prod_pricep, prod_man, prod_24, prod_pic, man_pic from v_products_detail where prod_id='.$prod
                     }],
    SubTemplDB    => [{ RetStorage      => 'ATTRS',
                        CB_SQL          => 'SELECT prm_name,prm_val FROM v_prod_main_params where prod_id=? limit 10',
                        DivTemplate     => $Tpimage->{ATTR_DIV},
                        Template        => $Tpimage->{ATTR_ITEM},
                        CB_SQLARGSARDER => 'prod_id',
                        throwArgs       => 'prod_id'
                     }],
    SubTemplFill  => [ ($pic_id>0)?
                       {
                         RetStorage     => 'IMAGE_PLACE',
                         Template       => (isOper())?$Tpimage->{IMAGE_PLACE_ADMIN}:$Tpimage->{IMAGE_PLACE},
                         link_del       => '?gen=pimage&action=del_pic&prod='.$prod.'&pic_id='.$pic_id,
                         pic_filepath   => $CONF{pic_path_600}.DBSingleValue('ProdImage3','SELECT pic_fname as prod_pic FROM v_pictures where pic_id=?',$pic_id),
                         SubTemplDB     => [{
                                              RetStorage      => 'LIST_PLACE',
                                              NUM             => 0,
                                              CORPIC          => $pic_id,
                                              link_tmp        => '?gen=pimage&prod='.$prod.'&pic_id=',
                                              PREPAREFILL     => \&ProdImagePrep,
                                              CB_SQL          => 'select pic_id from t_prod_image where prod_id=?',
                                              CB_SQLARGSARDER => 'prod_id',
                                              prod_id         => $prod
                                           }]
                       }:'',(isOper())?
                       {
                         RetStorage   => 'IMAGE_ADD',
                         Template     => $Tpimage->{IMAGE_ADD},
                         hiddens      => gen_hiddens('MAX_FILE_SIZE:1000000,action:add_pic,gen:pimage,prod,UpPicCI:5,UpPicPI:pic_pic')
                       }:'',
                       gen_bucket_link('prod_id','pimage'),
                       put_prices()
                     ]
  });
#   put_prices().
#   gen_bucket_link('prod_id').
};
#---------------------------------------------------------
1;
#                             OR_SQLARGSARDER  => '',
#                             OR_FLDPREF       => '',
#                             OR_throwArgs     => '',
#                             CB_SQLARGSARDER  => '',
#                             FLDPREF          => '',
#                             throwArgs        => '',
