$Tprods = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/prods');
#=====================================================
sub ProdsList($) #catid
{
    my $grp=$REQ{grp}+0;
    my $man=$REQ{man}+0;
    my $tr= $REQ{typeref}+0;
  #-----------------------
    my $sqlgrp='';
    if($man>0) {  $sqlgrp='SELECT grp.id as sql_id,grp.name as sql_name FROM t_prod_type_ref_grp grp where grp.id in(select mrg.id_type_grp from t_products mrg where mrg.id_man='.$man.')and grp.owner='.$tr;
    }else{        $sqlgrp='SELECT grp.id as sql_id,grp.name as sql_name FROM t_prod_type_ref_grp grp where grp.owner='.$tr;
    };
    my $sqlman='';
    if($grp>0){   $sqlman='SELECT man.id as sql_id,man.name as sql_name from t_man man where man.id in(select mrg.id_man from t_products mrg where mrg.id_type_grp='.$grp.')';
    }else{        $sqlman='SELECT man.id as sql_id, man.name as sql_name from t_man man where man.id in (select mrg.id_man from t_products mrg where mrg.id_type_grp in (select grp.id from t_prod_type_ref_grp grp where grp.owner='.$tr.'))';
    };
  #-----------------------


######################     put_prices().


   return DBTEMPLATEFILL(
   {
     typeref_id    => $tr,
     DivTemplate   => '',
#     pic_path      => $CONF{pic_path_180},
     Template      => (isOper())?$Tprods->{ADM_MAIN}:$Tprods->{USR_MAIN},
     link_del      => '?'.gen_url('gen,grp,man,typeref,page').'&action=del_prod&prod_id=',
     hidden_tr     => gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_typeref,UpPicCI:3,UpPicPI:typeref_pic,gen:prods,typeref,typeref_id:'.$tr),
     hidden_nav    => gen_hiddens('gen,typeref'),
     hidden_add    => gen_hiddens('MAX_FILE_SIZE:1000000,action:add_prod,UpPicCI:5,UpPicPI:prod_pic,gen:pdetail'),
     SubTemplFill  => [
                        select_gen('grp',$REQ{grp},'place_grp','��� ������',$sqlgrp,1,'','',''),
                        select_gen('prod_grp',-1  ,'place_addgrp','����� ������','SELECT grp.id as sql_id,grp.name as sql_name FROM t_prod_type_ref_grp grp where grp.owner='.$tr,0,'','',''),
                        select_gen('man',$REQ{man},'place_man','��� �������������',$sqlman,1,'','',''),
                        select_gen('prod_man',-1  ,'place_addman','����� �������������','SELECT man.id as sql_id,man.name as sql_name from t_man man',0,'','','')
                      ],
     OneRowSelects => [{
                         OR_SQL          => 'SELECT typeref_name, typeref_pic from v_prod_type_ref_detail where typeref_id=?',
                         OR_SQLARGSARDER => 'typeref_id',
                         OR_FLDPREF      => '',
                         OR_throwArgs    => 'typeref_id',
                         OR_throwArgsUp  => 'typeref_name,typeref_pic'
                      }],
     SubTemplDB    => [{
                         RetStorage   => 'body',
                         PAGING       => gen_page_prm('?'.gen_url('gen,grp,man,typeref').'&page=',10),
                         tmp_lpi      => '?gen=pimage&'.gen_url('typeref,man,grp').'&prod=',
                         DivTemplate  => $Tprods->{PR_DIV},
                         SubTemplFill => [gen_bucket_link('prod_id','prods','BUKET'),
                                          put_prices()
                                         ],
                         PREPAREFILL  => [\&kill_smb, sub
                                                      {
                                                        $_[0]->{link_prod}       = '?gen=pdetail&prod='.$_[0]->{prod_id};
                                                        $_[0]->{link_prod_pic}   = $CONF{pic_path_180}.$_[0]->{prod_pic};
                                                        $_[0]->{link_prod_images}= $_[0]->{tmp_lpi}.$_[0]->{prod_id};
                                                      }],
                         KILLSMBFLDS  => 'prod_name',
                         Template     => (isOper())?$Tprods->{PR_ITEM_ADM}:$Tprods->{PR_ITEM},
                         CB_SQL       => 'SELECT prod_id, prod_name, prod_pricev, prod_pricep, prod_man, prod_24, prod_pic, man_pic from v_products_detail '.
                                         'where '.(($grp>0)?'prod_type_grp='.$grp:'prod_type_grp in (SELECT id FROM t_prod_type_ref_grp where owner='.$tr.')').
                                         (($man>0)?' and prod_man='.$man:'').' order by prod_pricev',
                         SubTemplDB   => [{ RetStorage      => 'ATTRS',
                                            CB_SQL          => 'SELECT prm_name,prm_val FROM v_prod_main_params where prod_id=? limit 10',
                                            DivTemplate     => $Tprods->{ATTR_DIV},
                                            Template        => $Tprods->{ATTR_ITEM},
                                            CB_SQLARGSARDER => 'prod_id',
                                            throwArgs       => 'prod_id'
                                         }]
                      }],
                      PREPAREFILL  => sub
                      {
                       $_[0]->{typeref_pic_path}=$CONF{pic_path_80}.$_[0]->{typeref_pic};
                      }
   });
};
#---------------------------------------------------------
1;
