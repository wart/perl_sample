$Tsubcat = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/subcat');
#----------------------------------------------------------------------
sub SubCatList($) #catid
{
  #-----------------------
   return DBTEMPLATEFILL(
   {
       link_subcat   => '?gen=subcat&cat='.$REQ{cat}.'&action=del_subcat&subcat_id=',
       hiddens_upd   => gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_cat,UpPicCI:1,UpPicPI:cat_pic,gen:subcat,cat,cat_id:'.$REQ{cat}),
       hiddens_add   => gen_hiddens('MAX_FILE_SIZE:1000000,action:add_subcat,UpPicCI:2,UpPicPI:subcat_pic,gen:typeref,subcat_owner:'.$REQ{cat}),
       Template      => (isOper())?$Tsubcat->{MAIN_ADMIN}:$Tsubcat->{MAIN_USER},
       DivTemplate   => '',
       cat_id        => $REQ{cat},
       PREPAREFILL   => [sub
                         {
                           $_[0]->{link_cat_pic}=$CONF{pic_path_80}.$_[0]->{cat_pic};
                         }
#                        \&CatList_CL_PREPFILL;
                        ],
       OneRowSelects => [{
                           OR_SQL          => "SELECT cat.name as cat_name,cat.shortname as cat_shortname ,concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as cat_pic "."\n".
                                              "FROM t_prod_type_cat as cat left join  t_pictures pic on cat.pic_id=pic.id and pic.visible=1 where cat.id=?",
                           OR_SQLARGSARDER => 'cat_id',
                           OR_FLDPREF      => '',
                           OR_throwArgs    => 'cat_id',
                           OR_throwArgsUp  => 'cat_name,cat_shortname,cat_pic'
                        }],
       SubTemplDB    => [{
                           RetStorage      => 'body',
                           PREPAREFILL     => [sub
                                               {
                                                 $_[0]->{link_subcat_pic}=$CONF{pic_path_80}.$_[0]->{subcat_pic};
                                                 $_[0]->{link_typeref}   ='?gen=typeref&subcat='.$_[0]->{subcat_id};
                                               },\&kill_smb
                                              ],
                           DivTemplate     => $Tsubcat->{SC_DIV},
                           Template        => (isOper())?$Tsubcat->{SC_ADMIN}:$Tsubcat->{SC_USER},
                           KILLSMBFLDS     => 'subcat_name',
                           throwArgs       => 'cat_id',
                           CB_SQLARGSARDER => 'cat_id',
                           PAGING          => gen_page_prm(gen_url('gen,cat').'&page=',10),
                           CB_SQL          => "SELECT subcat.id as subcat_id,subcat.name as subcat_name ,concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as subcat_pic ".
                                              "FROM t_prod_type_subcat as subcat left join  t_pictures pic on subcat.pic_id=pic.id and pic.visible=1 where subcat.owner=?",
                           SubTemplDB      => [{
                                                 RetStorage      => 'ITEMS',
                                                 throwArgs       => 'subcat_id',
                                                 throwArgsAs     => '',
                                                 FLDPREF         => '',
                                                 CB_SQLARGSARDER => 'subcat_id',
                                                 DivTemplate     => $Tsubcat->{SUB_DIV},
                                                 Template        => $Tsubcat->{SUB_ITEM},
                                                 PREPAREFILL     => [sub
                                                                     {
                                                                       $_[0]->{link_prods}='?gen=prods&typeref='.$_[0]->{typeref_id};
                                                                     }
                                                                    ],
                                                 CB_SQL          => "SELECT typeref.id as typeref_id,typeref.name as typeref_name,concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as typeref_pic ".
                                                                    "FROM t_prod_type_ref typeref left join  t_pictures pic on typeref.pic_id=pic.id and pic.visible=1 ".
                                                                    "where typeref.owner=? limit 5"
                                              }]
                        }]
   }).PUT_SM_BYCAT();
};
#---------------------------------------------------------
1;
