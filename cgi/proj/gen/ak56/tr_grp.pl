$Ttr_grp = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/tr_grp');

#=====================================================
#sub catprm_ismain($)
#{
# $_[0]->{checked}=($_[0]->{prm_main}==1)?' checked':'';
#};
#=====================================================
sub tr_grp($) #catid
{
  #-----------------------
   my $tr =$REQ{typeref}+0;

   return DBTEMPLATEFILL(
   {
     Template      => $Ttr_grp->{BODY},
     hiddens_upd   => gen_hiddens('action:upd_grp,gen,typeref,page'),
     hiddens_add   => gen_hiddens('action:add_grp,gen,typeref,page'),
     link_del      => '?gen=tr_grp&typeref='.$tr.'&action=del_grp&grp_id=',
     OneRowSelects => [{
                         OR_SQL         => 'SELECT tr.name as typeref_name, tr.owner as typeref_owner,'.
                                           "concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as typeref_pic ".
                                           ' FROM t_prod_type_ref tr left join  t_pictures pic on tr.pic_id=pic.id and pic.visible=1 where tr.id='.$tr,
                         OR_throwArgsUp => 'typeref_name,typeref_pic,typeref_owner'
                      }],
     SubTemplDB    => [{
                         RetStorage     => 'body',
                         CB_SQL         => 'SELECT grp.id as grp_id,grp.name as grp_name FROM t_prod_type_ref_grp grp where grp.owner='.$tr,
                         Template       => $Ttr_grp->{ITEM},
                         PAGING         => gen_page_prm('?gen=tr_grp&typeref='.$tr.'&page=',20),
                         PREPAREFILL    => \&kill_smb,
                         KILLSMBFLDS    => 'grp_name'
                      }],
     PREPAREFILL   => sub{
                         $_[0]->{pic_typeref}=$CONF{pic_path_80}.$_[0]->{typeref_pic};
                      }
  });
};
#---------------------------------------------------------
1;
