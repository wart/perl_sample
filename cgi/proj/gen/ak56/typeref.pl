$Ttyperef = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/typeref');

  #-----------------------
sub TypeRefList_afterrec($){

  if ((($_[0]->{corrow}+1) % 2)==0)
  {
    $_[0]->{RETTEMPFLD}.='</tr>'."\n".'<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>'."\n";
  }else{
    $_[0]->{RETTEMPFLD}.='<td>&nbsp;</td>';
  };
  if ((($_[0]->{corrow}+1) % 2)==0)
  {

  }else{
    if(($_[0]->{corrow}+1)==$_[0]->{rows})
    {
     $_[0]->{RETTEMPFLD}.='<td>&nbsp;</td><td>&nbsp;</td></tr>'."\n";
     };
  };
};
  #-----------------------
sub TypeRefList_beforerec($){
  if ((($_[0]->{corrow}+1) % 2)==0)
  {

  }
  else
  {
    $_[0]->{RETTEMPFLD}.='<tr>';
  };
};
  #-----------------------
sub TypeRefList($) #catid
{
   my %CATLIST;

   return DBTEMPLATEFILL(
   {
      Template        => (isOper() ? $Ttyperef->{BODY_ADM} : $Ttyperef->{BODY_USR} ),
      link_del        => '?gen=typeref&subcat='.$REQ{subcat}.'&action=del_typeref&typeref_id=',
      hiddens_upd     => gen_hiddens('MAX_FILE_SIZE:1000000,action:upd_subcat,UpPicCI:2,UpPicPI:subcat_pic,gen:typeref,subcat,subcat_id:'.$REQ{subcat}),
      hiddens_add     => gen_hiddens('MAX_FILE_SIZE:1000000,action:add_typeref,UpPicCI:3,UpPicPI:typeref_pic,gen:typeref,subcat,typeref_owner:'.$REQ{subcat}),
      PREPAREFILL     => [sub
                        {
                          $_[0]->{link_subcat_pic}=$CONF{pic_path_80}.$_[0]->{subcat_pic};
                        }
                       ],
      DivTemplate     => '',
      subcat_id       => $REQ{subcat},
      OneRowSelects   => [{
                            OR_SQL          => "SELECT subcat.name as subcat_name ,concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as subcat_pic ".
                                               "FROM t_prod_type_subcat as subcat left join  t_pictures pic on subcat.pic_id=pic.id and pic.visible=1 where subcat.id=?",
                            OR_SQLARGSARDER => 'subcat_id',
                            OR_FLDPREF      => '',
                            OR_throwArgs    => 'subcat_id',
                            OR_throwArgsUp  => 'subcat_name,subcat_pic'
                         }],
      SubTemplDB      => [{
                            RetStorage      => 'body',
                            Template        => (isOper() ? $Ttyperef->{ITEM_ADM} : $Ttyperef->{ITEM_USR}),
                            DivTemplate     => '',
                            PREPAREFILL     => [sub
                                                {
                                                   $_[0]->{link_prods}='?gen=prods&typeref='.$_[0]->{typeref_id};
                                                   $_[0]->{link_typeref_pic}=$CONF{pic_path_80}.$_[0]->{typeref_pic};
                                                   $_[0]->{link_catprm}='?gen=catprm&typeref='.$_[0]->{typeref_id};
                                                   $_[0]->{link_tr_grp}='?gen=tr_grp&typeref='.$_[0]->{typeref_id};
                                                },
                                                \&kill_smb
                                               ],
                            KILLSMBFLDS     => 'typeref_name',
                            PAGING          => gen_page_prm(gen_url('gen,subcat').'&page=',10),
                            CB_SQL          => "SELECT typeref.id as typeref_id,typeref.name as typeref_name,concat(pic.id,'.',pic.extens) as typeref_pic ".
                                               "FROM t_prod_type_ref typeref left join  t_pictures pic on typeref.pic_id=pic.id and pic.visible=1 ".
                                               "where typeref.owner=?",
                            CB_SQLARGSARDER => 'subcat_id',
                            throwArgs       => 'subcat_id',
                            FLDPREF         => '',

                            CB_WORKBEF      => \&TypeRefList_beforerec,
                            CB_WORKAFT      => \&TypeRefList_afterrec
#                           CB_AFTER        => \&TypeRefList_after
                         }]
   }).PUT_SM_BYSUBCAT();
};
#---------------------------------------------------------
1;
