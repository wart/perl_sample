#--------------------------------------------
%BREADCRUMB;
#--------------------------------------------
$Tnav    = LoadBlockTemplate('./interface/'.$CONF{interface}.'/nav');
#--------------------------------------------
sub nav_array
{
  my $a=shift;
  my $sz=(scalar @$a) +0;
  my $ret='';
  if($sz>0)
  {
    $last=$sz-1;
    for(my $i=0;$i<$sz;$i++)
    {
      if($i>0)
      {
        $ret.=$Tnav->{splitter};
      };
      if($i<$last)
      {
        $ret.= fill_template($Tnav->{link},{ link => $a->[$i]{link} ,cap=>$a->[$i]{cap}});
      }
      else
      {
        $ret.= fill_template($Tnav->{active},{cap=>$a->[$i]{cap}});
      };
    };
  };
  return fill_template($Tnav->{Body},{Items =>$ret});
};
#--------------------------------------------
sub nav_main
{
  return   nav_array( [{ link => '?', cap => '�������'},
                       {  cap => $_[0] }]);
};
#--------------------------------------------
#$BREADCRUMB{edituser} = sub { return nav_user();}
#$BREADCRUMB{viewuser} = sub { return nav_user();};
#--------------------------------------------
$BREADCRUMB{pwdrecovery} = sub {return nav_main('�������������� ������');};
$BREADCRUMB{reg}         = sub {return nav_main('�����������');};
$BREADCRUMB{addtown}     = sub {return nav_main('���������� ������');};
$BREADCRUMB{adduref}     = sub {return nav_main('���������� ���� �������������');};
$BREADCRUMB{usersetup}   = sub {return nav_array( [{ cap=> '���������'}]);};
$BREADCRUMB{changepwd}   = sub {return nav_array( [{ link => '?gen=usersetup', cap => '���������'},
                                                   {  cap => '��������� ������' }]);};

#--------------------------------------------
sub gen_page_prm($$)
{

# $ARGS{t_pages};
# $ARGS{t_corpage};
# $ARGS{t_it_lnk};
# $ARGS{t_it_cap};

  return
  {

    PAGEURL     => $_[0],
    linkAll     => $_[0].'all',
    PAGESIZE    => $_[1],
    PagePrep    => \&default_prepare_page,
    PAGEPLACES  => 'PagesPlace1,PagesPlace2',
    PAGEPARAM   => 'page',
    t_main      => $Tnav->{page_containeer},
    t_main_body => 'BODY',
    t_it        => $Tnav->{page},
    t_itAll     => $Tnav->{page_all},
    t_cit       => $Tnav->{cor_page},
    t_citAll    => $Tnav->{cor_page_all},
    t_div       => '',
    t_max_pages => 10
  };
};
#--------------------------------------------
1;