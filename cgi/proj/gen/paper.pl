use mod::Note;
#--------------------------------------------
$Tpaper   = LoadBlockTemplate('./interface/'.$CONF{interface}.'/paper');

#[#menu_items#] select r.id, r.cap from t_pages r order by id [#END#]



$PAPERS_MOD= Note->new(
{
  SysMsgParamID => 'sys_message',
  ParamID    => 'paper_id',
  SQLID      => 'paper_id',
  AdminChk   => sub { return isAdmin();},
  List       => {
                  Body       =>  $Tpaper->{Body},
                  BodyAdm    =>  $Tpaper->{BodyAdm},
                  Item       =>  $Tpaper->{Item},
                  ItemAdm    =>  $Tpaper->{ItemAdm},
                  ITEMS_RET  =>  'ITEMS',
                  SQL        =>  'select id as paper_id, cap as paper_cap, text as paper_text, post_dt as paper_date from t_papers order by id desc'
                },
  Paging     => {
                  Param      => 'page',
                  Size       =>  20,
                },
  TParams    => {
                  LinkView   => 'link_view',
                  LinkEdit   => 'link_edit',
                  LinkDelete => 'link_del',
                  HiddenAdd  => 'hiddens',
                  HiddenEdt  => 'hiddens'
                },
  Gen        => {
                  ParamID    => 'gen',
                  List       => 'paper_list',
                  Edit       => 'paper_edit',
                  View       => 'paper_view'
                },
  Action     => {
                  ParamID    => 'action',
                  Delete     => 'paper_del',
                  Edit       => 'paper_edit',
                  Add        => 'paper_add'
                },
  View       => {
                  Body       => $Tpaper->{detail},
                  SQL        => 'select id as paper_id, cap as paper_cap, text as paper_text, post_dt as paper_date from t_papers where id=?'
                },
  Edit       => {
                  Body       => $Tpaper->{edit},
                  ChkSQL     => 'select count(1) from t_papers where id=?',
                  SQL        => 'select id as paper_id, cap as paper_cap, text as paper_text, post_dt as paper_date from t_papers where id=?',
                  UpdSQL     => 'update t_papers set cap=?, text=? where id=?',
                  UpdSQLArgs => 'paper_cap,paper_text,paper_id',
                  OnNotFound => {},
                  OnAccDeny  => {},
                  Checks     => {
                                  paper_cap    =>  '�� �������� ��������� ��������',
                                  paper_text   =>  '�� �������� ����� ��������'
                                },
                  OnDone     => sub{ $REQ{gen}='paper_view';$REQ{sys_message}='�������� ��������.';}
                },
  Add        => {
                  SQL        => 'insert into t_papers(cap,text,post_dt) values(?,?,sysdate())',
                  SQLArgs    => 'paper_cap,paper_text',
                  Checks     => {
                                  paper_cap     =>'�� �������� ��������� ��������',
                                  paper_text    =>'�� �������� ����� ��������'
                                },
                  OnDone     => sub
                                {
                                  ClearREQ('paper_cap,paper_text');
                                  $REQ{gen}='paper_view';
                                  $REQ{paper_id}=DBlast_insert_id();
                                  $REQ{sys_message}='�������� ���������.';
                                }
                },
  Delete     => {
                  SQL        => 'delete from t_papers where id=?',
                  SQLArgs    => 'paper_id',
                  OnAccDeny  => {},
                  OnDone     => {}
                }
});
#--------------------------------------------
#$BREADCRUMB{adduref}     = sub {return nav_main('���������� ���� �������������');};
#$BREADCRUMB{usersetup}   = sub {return nav_array( [{ cap=> '���������'}]);};
#$BREADCRUMB{changepwd}   = sub {return nav_array( [{ link => '?gen=usersetup', cap => '���������'},
#                                                   {  cap => '��������� ������' }]);};
#--------------------------------------------
$BREADCRUMB{paper_list} = sub
                          {
                            return nav_main('������');
                          };
$BREADCRUMB{paper_view} = sub
                          {
                            return nav_main(DBSingleValue('BREADCRUMB::paper_view','select cap from t_papers where id=?',$REQ{paper_id}));
                          };
$BREADCRUMB{paper_edit} = sub {return nav_array([
                                                  { link => '?', cap => '�������'},
                                                  { link => '?gen=paper_list',cap => '������'},
                                                  { cap=> DBSingleValue('BREADCRUMB::paper_view','select cap from t_papers where id=?',$REQ{paper_id})}
                                                ]);};




#--------------------------------------------
sub gen_papers_list
{
  return $PAPERS_MOD->genList();
};
#--------------------------------------------
sub gen_papers_view
{
  return   $PAPERS_MOD->genView();
};
#--------------------------------------------
sub gen_papers_edit
{
  return   $PAPERS_MOD->genEdit();
};
#--------------------------------------------
sub act_papers_add
{
  $PAPERS_MOD->doAdd();
};
#--------------------------------------------
sub act_papers_edit
{
  $PAPERS_MOD->doEdit();
};
#--------------------------------------------
sub act_papers_delete
{
  $PAPERS_MOD->doDelete();
};
#--------------------------------------------
1;
