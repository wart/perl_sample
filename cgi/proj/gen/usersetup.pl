#------------------------------------------------------------
$sql_usersetup = LoadBlockTemplate('./sql/usersetup.bsq');
$Tusersetup    = LoadBlockTemplate('./interface/'.$CONF{interface}.'/usersetup');
#------------------------------------------------------------
sub gen_usersetup
{

  if ($SESS{uid}>0)
  {
       my $F={
          SETUPFIELDS=> gen_hiddens({
                        gen=>'',
                        action=>'usersetup'
                      })
          };
          $F->{linkPwd}='?gen=changepwd';
          $F->{chpwd}=fill_template($Tusersetup->{chpwd},$F);
          return fill_template($Tusersetup->{usersetup},$F);
  };

   return 'Error';

};
#------------------------------------------------------------
1;


