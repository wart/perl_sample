
$sql_viewuser = LoadBlockTemplate('./sql/viewuser.bsq');
$Tviewuser    = LoadBlockTemplate('./interface/'.$CONF{interface}.'/viewuser');
#--------------------------------------------
sub user_present
{
   return DBCountPresent('user_present',$sql_viewuser->{'user_present'},$_[0]);
};
#--------------------------------------------
sub form_user_contacts #edit
{
   my $DTL=
   {
       Template      =>
         TemplateInsertBlock
         (
           $Tviewuser->{info_form},
           contacts,
           ($_[0]==0)?
                $Tviewuser->{contacts}
               :$Tviewuser->{contacts_edit}
         ),
       OneRowSelects =>
       [
         {
            user_id         => ($_[0]==0)?$REQ{user_id}:$SESS{uid},
            OR_SQL          => $sql_viewuser->{user_detail},
            OR_SQLARGSARDER => 'user_id',
            OR_throwArgsUp  => '*'
         }
       ],
       townplace=>fill_select($Tviewuser->{townplace},'towns',$Tviewuser->{singletown},$sql_user->{towns},$SESS{inf_town}),
       USEREDITFIELDS=>gen_hiddens({ gen      => '',
                                     action   => upd_user_info
                                         }),
       PREPAREFILL => sub
       {
           my $a = shift;
           $a->{avatar}=$CONF{avatarpath}.(($a->{avatar_ext} ne '')? $a->{user_id}.'.'.$a->{avatar_ext}:'blank.png' );
       }
   };
   #USEREDITFIELDS

   return DBTEMPLATEFILL($DTL);
};
#--------------------------------------------
sub form_viewuser_actions
{
   my $a={};
#   fill_user_action_links($a,$REQ{user_id});
   if(isUser())
   {
      if($REQ{user_id}!=$SESS{uid})
      {
         if(userPresent($REQ{user_id}))
         {
#             $a->{send_msg}=fill_template($Tviewuser->{send_msg},$a);


             return fill_template ($Tviewuser->{action_pane},$a);
          };
       };
    };
    return '';
};
#------------------------------------------------------------
sub form_update_avatar
{
   return fill_template($Tviewuser->{update_avatar},
          {
              LOADAVATFIELDS=>gen_hiddens({ gen      => '',
                                            user_id  => '',
                                            action   => upload_avatar,
                                            MAX_FILE_SIZE =>1000000
                                         })
          });
};
#------------------------------------------------------------
sub act_upload_avatar
{
   if(isUser())
   {
      my $ext=getFileExt($REQ{avatar_file});
      if ($ext ne '')
      {
         my $tmp_file='./tmp/'.$SESS{'tmp-file'}.'.'.$ext;
         if(upload_keep_as('avatar_file',$tmp_file))
         {
            my $image = Image::Magick->new;
            $image->Read($tmp_file);
            if(my_image_crop_to_aspect($image,150,300))
            {
               DBexec('act_upload_avatar',$sql_viewuser->{avatar_upload},$ext,$SESS{uid});
               my $res_fname=$SESS{uid}.'.'.$ext;
               $image->Resize( width=>150, height=>300);
               unlink($CONF{LOCALPATH}.$CONF{avatarpath}.$res_fname);
               $x = $image->Write($CONF{LOCALPATH}.$CONF{avatarpath}.$res_fname);

               my $image2 = Image::Magick->new;
               $image2->Read($tmp_file);
               my_image_crop_to_aspect($image2,50,50);
               $image2->Resize( width=>50, height=>50);
               unlink($CONF{LOCALPATH}.$CONF{avatarpath}.$CONF{smallpath}.$res_fname);
               $x = $image2->Write($CONF{LOCALPATH}.$CONF{avatarpath}.$CONF{smallpath}.$res_fname);
            };
            unlink($tmp_file);
         }
         else
         {
            $REQ{sys_message}='Ошибка загрузки 2';
         };
      }else
      {
        $REQ{sys_message}='Ошибка загрузки 1';
      };
   }else
   {
     $REQ{sys_message}='Нет прав';
   };
};
#--------------------------------------------
sub gen_viewuser
{
   my $R= form_viewuser_actions(0);
          $R.=form_user_contacts();
   return $R;
};
#------------------------------------------------------------
sub gen_edituser
{
   if(isUser())
   {
      $REQ{user_id}=$SESS{uid};
      return form_user_contacts(1).
             form_update_avatar();
   }else{
      return center_part(0);
   };
};


#------------------------------------------------------------
sub act_upd_user_info
{
   if(isUser())
   {
         DBexec('act_upd_user_info',$sql_viewuser->{upd_user_info},$REQ{usr_skype},$REQ{usr_email},$REQ{usr_phone},$REQ{usr_icq},$REQ{usr_birth_dt},$REQ{usr_town},$SESS{uid});
         $SESS{inf_town}=$REQ{usr_town};
   };
};
#------------------------------------------------------------
1;
