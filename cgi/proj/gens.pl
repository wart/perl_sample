
require "./proj/gen/admin.pl";
require "./proj/gen/nav.pl";
require "./proj/gen/viewuser.pl";
require "./proj/gen/usersetup.pl";
require "./proj/gen/paper.pl";
#---------------------------------------------------------------
%GEN;
$GEN{main}      = \&gen_main;

$GEN{changepwd}   = \&gen_changepwd;
#$GEN{pwdrecovery} = \&gen_pwdrecovery;
$GEN{usersetup}   = \&gen_usersetup;


#$GEN{reg}         =\&gen_reg;
#$GEN{viewuser}    =\&gen_viewuser;
#$GEN{edituser}    =\&gen_edituser;


#$GEN{addtown}=\&gen_addtown;
#$GEN{adduref}=\&gen_adduref;

$GEN{paper_list} =\&gen_papers_list;
$GEN{paper_view} =\&gen_papers_view;
$GEN{paper_edit} =\&gen_papers_edit;

#---------------------------------------------------------------
%GENNAV;$GENNAVready='';
        $GENNAVhiddensready='';

$GENNAV{changepwd}   = '';
$GENNAV{usersetup}   = '';
$GENNAV{paper_list}  = '';
$GENNAV{paper_view}  = 'paper_id';
$GENNAV{paper_edit}  = 'paper_id';

#---------------------------------------------------------------
sub getGENUrl()
{
  if($GENNAVready ne '')
  {
    return $GENNAVready;
  };
  my $g=$GENNAV{$REQ{gen}};

  my $ret='gen='.$REQ{gen};

  my $r2=gen_url($g);
  if ($r2 ne '')
  {
   $ret.='&'.$r2;
  };
  $GENNAVready=$ret;
  return $ret;
};
#---------------------------------------------------------------
sub putNAVREQHiddens
{
  if($GENNAVhiddensready ne '')
  {
    return $GENNAVhiddensready;
  };
  my $g=$GENNAV{$REQ{gen}};
  my $ret=gen_single_hidden('gen',$REQ{gen}). gen_hiddens($g);

  $GENNAVhiddensready=$ret;
  return $ret;
};
#---------------------------------------------------------------
sub put_mainpage
{
  my $mt = LoadBlockTemplate('./interface/'.$CONF{interface}.'/main');
  my %P;
  if($REQ{sys_message} ne '')
  {
    $P{sys_msg}=$REQ{sys_message};
    $P{alert_place}=fill_template($mt->{alert_place},\%P);
  };
  $P{townplace}=fill_select(
  TemplateInsertBlock($mt->{townplace},TOWNNAVFIELDS,putREQHiddens( [action,select_town] ))
  ,'towns',$mt->{singletown},$sql_user->{towns},$SESS{town_id}); # mtemp rets stemp sql curval

  out_print(fill_template($mt->{main},\%P));
};
#---------------------------------------------------------------
sub prepGenName
{
 return $REQ{'gen'}=($_[0] eq '')? $REQ{'gen'}:(($_[0] eq '0')?'':$_[0])
};
#---------------------------------------------------------------
sub Breadcrumb
{
   my $r=prepGenName();
   if (ref($BREADCRUMB{$r}) eq 'CODE')
   {
       return $BREADCRUMB{$r}->();
   };

   if(ref($BREADCRUMB{main}) eq 'CODE')
   {
      return $BREADCRUMB{main}->();
   };
   return 'Breadcrumb not defined';
};
#---------------------------------------------------------------
sub center_part
{
   my $r=prepGenName();
   if ( ref($GEN{$r}) eq 'CODE')
   {
       return $GEN{$r}->();
   };
   if (ref($GEN{main})eq 'CODE')
   {
      return $GEN{main}->();
   };
   return 'Generator not defined';
};

#---------------------------------------------------------------

sub gen_main
{
  return 'nosection';#form_LastNews().form_events();
};
#---------------------------------------------------------------
1;
