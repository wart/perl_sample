$Tmenu = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/menu');
#-----------------------------------------------------------------------
sub gen_MENU
{
   return DBTEMPLATEFILL(
   {
     Template   => $Tmenu->{BODY},
     SubTemplDB =>
     [{
        RetStorage        => 'MITEMSLINE',
        Template          => $Tmenu->{ITEM_L},
        CB_SQL            => 'SELECT id as cat_id,name as cat_name FROM t_prod_type_cat order by cat_name',
      },{
        RetStorage        => 'MITEMS',
        Template          => $Tmenu->{ITEM_M},
        CB_SQL            => 'SELECT c.id as cat_id,c.name as cat_name,m.cat_mode FROM t_prod_type_cat as c'.
                             ' join v_menu_mode m on c.id=m.cat_id',
        SubTemplFill      =>
        [{
           throwArgs      => 'cat_id,cat_name,cat_mode,corrow',
           CaseArg        => 'cat_mode',
           RetStorage     => 'MSITEMS',
           CaseVariant    =>
           {
              1             =>
              {
                throwArgs   => 'cat_id,cat_name,cat_mode,corrow',
                Template    => $Tmenu->{BODY_TR},
                PREPAREFILL => sub
                               {
                                 $_[0]->{link_typeref}='?gen=typeref&subcat='.$_[0]->{subcat_id};
                               },
                SubTemplDB=>[{
                               Template        => $Tmenu->{ITEM_TR},
                               RetStorage      => 'ITEM_TR',
                               throwArgs       => 'cat_id',
                               CB_SQLARGSARDER => 'cat_id',
                               CB_SQL          => 'SELECT id as typeref_id,name as typeref_name FROM t_prod_type_ref '.
                                                  ' where owner in (select id from t_prod_type_subcat where owner=?)',
                               PREPAREFILL     => sub
                                                  {
                                                    $_[0]->{link_prods}='?gen=prods&typeref='.$_[0]->{typeref_id};
                                                  }
                            }],
              },
              0        =>
              {
                throwArgs => 'cat_id,cat_name,cat_mode,corrow',
                Template  => $Tmenu->{BODY_SC},
                SubTemplDB=>[{
                               Template        => $Tmenu->{ITEM_SC},
                               RetStorage      => 'ITEM_SC',
                               throwArgs       => 'cat_id',
                               CB_SQLARGSARDER => 'cat_id',
                               CB_SQL          => 'SELECT id as subcat_id,name as subcat_name FROM t_prod_type_subcat where owner=?',
                               PREPAREFILL     => sub
                                                  {
                                                     $_[0]->{link_typeref}='?gen=typeref&subcat='.$_[0]->{subcat_id};
                                                  },
                               OneRowSelects   => [{
                                                     OR_SQL          => 'SELECT count(1) as subcat_items FROM t_prod_type_ref where owner=?',
                                                     OR_SQLARGSARDER => 'subcat_id',
                                                     OR_throwArgs    => 'subcat_id',
                                                     OR_throwArgsUp  => 'subcat_items'
                                                  }],
                               SubTemplDB      => [{
                                                     RetStorage      => 'ITEM_SCSUB',
                                                     Template        => $Tmenu->{ITEM_SCSUB},
                                                     TemplateAll     => $Tmenu->{ITEM_SCALL},
                                                     throwArgs       => 'subcat_id,subcat_items',
                                                     CB_SQL          => 'SELECT id as typeref_id,name as typeref_name FROM t_prod_type_ref where owner=? limit 6',
                                                     CB_SQLARGSARDER => 'subcat_id',
                                                     PREPAREFILL     => sub
                                                                        {
                                                                          $_[0]->{link_prods}='?gen=prods&typeref='.
                                                                          $_[0]->{typeref_id};
                                                                        },
                                                     CB_AFTER        => sub
                                                                        {
                                                                          if ($_[0]->{subcat_items}>$_[0]->{corrow})
                                                                          {
                                                                             $_[0]->{link_all}='?gen=typeref&subcat='.$_[0]->{subcat_id};
                                                                             $_[0]->{RETTEMPFLD}.=fill_template($_[0]->{TemplateAll},$_[0]);
                                                                          };
                                                                        }
                                                  }]
                            }]
              }
           }
        }]

     }]
   });
};
#-----------------------------------------------------------------------
1;