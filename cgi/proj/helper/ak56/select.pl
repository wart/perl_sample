#=====================================================
sub kill_smb($)
{

  foreach my $r (split(',',$_[0]->{KILLSMBFLDS}))
  {

    my @e=split(':',$r);

    if (scalar @e ==1)
    {
      $_[0]->{$e[0]}=~ s/\"/\&\#34\;/g;
      $_[0]->{$e[0]}=~ s/\'/ /g;
    }else{
      $_[0]->{$e[1]}=$_[0]->{$e[0]};
      $_[0]->{$e[1]}=~ s/\"/\&\#34\;/g;
      $_[0]->{$e[1]}=~ s/\'/ /g;
    };
  };
};

#=====================================================
sub select_gen_br($)
{
  $_[0]->{SELECTED}= ($_[0]->{VALUE}==$_[0]->{sql_id})?' selected':'';
};
#=====================================================
sub roword($)
{
  $_[0]->{roword}=($_[0]->{roword}==0)?1:0;
};
#=====================================================
sub select_gen($$$$$$$$$) #name val rets defname sql
{
   my ($name,$value,$retstorage,$defname,$sql,$subm,$th,$thas,$sord)=@_;
   $value+=0;

    my %K;
    $K{RetStorage}=$retstorage;
    $K{Template}='<select name="'.$name.'"';


    $K{throwArgs}=$th;
    $K{throwArgsAs}=$thas;


    if($subm!=0)
    {
      $K{Template}.=' onChange="this.form.submit();"';
    };
    $K{Template}.=' style="width:100%;">'."\n";
    if($defname ne '')
    {
      $K{Template}.='<option value="0">'.$defname.'</option>';
    };
    $K{Template}.='{%PRM:SELITEMS%}'.
                 '</select>';
#-------------------------
    my %L;
    $L{PREPAREFILL}=\&select_gen_br;
    $L{RetStorage}='SELITEMS';
    $L{VALUE}=$value;
    $L{FLDPREF}='';
    $L{throwArgs}=($thas eq '')?$th:$thas;
    $L{throwArgsAs}=$thas;
    $L{Template}='<option value="{%PRM:sql_id%}"{%PRM:SELECTED%}>{%PRM:sql_name%}</option>'."\n";
    $L{CB_SQL}=$sql;
    $L{CB_SQLARGSARDER}=$sord;
    $L{DivTemplate}='';
#-------------------------
     #if($sql ne '')
     {$K{SubTemplDB}=[\%L];};
#-------------------------
    return \%K;
};
#-------------------------
1;