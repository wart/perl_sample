$Tside = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/side');
#-------------------------
sub SidePaneEmpty($)
{
 my $args=$_[0];
 $args->{RETTEMPFLD}.=$args->{EMPTY1}.$args->{KRITERII}.$args->{EMPTY2};
};
#-------------------------
sub SidePane#($$$$$$)  # side (l\r)
{
  my ($side,$itmlnk,$alllnk,$allcap,$maincap,$sql,$emp1,$emp2,$itemhtml,$extPrep)=@_;
  #-----------------------
  return DBTEMPLATEFILL(
  {
     Template      => $Tside->{BODY},
     DivTemplate   => '',
     SubTemplFill  => [ ($maincap ne '')?
                        {
                           Template    => $Tside->{TITLE},
                           RetStorage  => 'TITLE',
                           MAINCAP     => $maincap,
                           SIDE        => $side,
                           link_all    => ($alllnk eq '')? '#':$alllnk
                        }:{},
                        ($allcap ne '')?
                        {
                           Template    => $Tside->{FOOT},
                           RetStorage  => 'FOOT',
                           SIDE        => $side,
                           link_all    => $alllnk,
                           link_allcap => $allcap
                        }:{},
                      ],
     SubTemplDB    => [{
                         Template      => ($itemhtml eq '')?$Tside->{ITEM}:$itemhtml,
                         DivTemplate   => $Tside->{ITEM_DIV},
                         RetStorage    => 'indata',
                         CB_SQL        => $sql,
                         CB_EMPTY      => \&SidePaneEmpty,
                         FLDPREF       => '',
                         itmlnk        => $itmlnk,
                         EMPTY1        => ($emp1 ne '')?$emp1:$Tside->{EMPTY1},
                         EMPTY2        => ($emp2 ne '')?$emp2:$Tside->{EMPTY2},
                         PREPAREFILL   => [sub
                                          {
                                            $_[0]->{link_item}=$_[0]->{itmlnk}.$_[0]->{sql_id};
                                            $_[0]->{link_cap}=$_[0]->{sql_name};
                                          },
                                          $extPrep]
                      }]
  });
};
#---------------------------------------------------------
sub sideCatlist($) #side
{
  return SidePane($_[0],'?gen=subcat&cat=','?gen=cat','��� ��������','��������','SELECT cat.id as sql_id,shortname as sql_name from t_prod_type_cat cat limit 5');
};
#=========================================================
sub sideSubCatlistA($$) #side
{
 my $v=DBSingleValue('sideSubCatlistA','select owner from t_prod_type_subcat where id=?',$_[1]);

 return SidePane($_[0],'?gen=typeref&subcat=','?gen=subcat&cat='.$v,'��� �������','�������','SELECT cat.id as sql_id,name as sql_name from t_prod_type_subcat cat where cat.owner='.$v.' limit 5');
};
#=========================================================
sub sideSubCatlistB($$) #side
{
 return sideSubCatlistA($_[0],DBSingleValue('sideSubCatlistB','select owner from t_prod_type_ref where id=?',$_[1]));
};
#=========================================================
sub TyperefCatlistA($$) #side
{
 my $v=DBSingleValue('sideSubCatlistA','select owner from t_prod_type_ref where id=?',$_[1]);
return SidePane($_[0],'?gen=prods&typeref=','?gen=typeref&subcat='.$v,'��� ���� �������','���� �������','SELECT cat.id as sql_id,name as sql_name from t_prod_type_ref cat where cat.owner='.$v.' limit 5');
};
#=========================================================
sub sideManlist($) #side
{
  return '';# current magaz only one manufaturer
  return SidePane($_[0],'?gen=manscat&man=','?gen=man','��� �������������','�������������','SELECT man.id as sql_id,name as sql_name from t_man man limit 5');
};
#=========================================================
sub sideGRPList($)
{
  return '' ;# ���� ����� ������ �������������

  my $man=$REQ{man}+0;
  my $tr =$REQ{typeref};
  my $sql='';
  if($man>0)
  {
    $sql='SELECT grp.id as sql_id,grp.name as sql_name FROM t_prod_type_ref_grp grp where grp.id in (select mrg.id_type_grp from t_products mrg where mrg.id_man='.$man.') and grp.owner='.$tr;
  }else{
    $sql='SELECT grp.id as sql_id,grp.name as sql_name FROM t_prod_type_ref_grp grp where grp.owner='.$tr.' limit 8';
  };
  return SidePane($_[0],'?gen=prods&'.gen_url('typeref,man,filter').'&grp=','','','��� ���� �������',$sql.' ');
};
#=========================================================
sub sideManProdlist($) #side
{

 return '' ;# ���� ����� ������ �������������


  my $tr =$REQ{typeref}+0;
  my $grp =$REQ{grp}+0;
  if ($grp>0)
  {
    $sql='SELECT man.id as sql_id,man.name as sql_name from  t_man man where man.id in (select mrg.id_man from t_products mrg where mrg.id_type_grp='.$grp.')';
  }else{
    $sql='SELECT man.id as sql_id,man.name as sql_name from t_prod_type_ref_grp grp, t_products mrg, t_man man where grp.owner='.$tr.' and mrg.id_type_grp=grp.id and mrg.id_man=man.id group by sql_id,sql_name';
  };
  return SidePane($_[0],'?gen=prods&'.gen_url('typeref,grp,filter').'&man=','?gen=man','��� �������������','�������������',$sql.' ');
};
#=========================================================
sub Random_prod($)
{
my $side=$_[0];

  my ($ok,@r)=DBexec('Random_prod1','select min(id),max(id) from t_products');
  $ok=0;
  my $min=$r[0]+0;
  my $max=$r[1]+0;
  my $id=0;
  if($max==$min){ return '';};

  while($ok==0)
  {
    $id= int((rand()*($max-$min))+$min);
    my ($o,@r)=DBexec('Random_prod2','select count(1) from t_products where id=?',$id);
    if ($r[0]>0)
    { $ok=1;};
  };

  return DBTEMPLATEFILL(
  {
    Template      => $Tside->{RANDOM_PROD},
    prod_id       => $id,
    OneRowSelects => [{
                        OR_SQLARGSARDER  => 'prod_id',
                        OR_FLDPREF       => '',
                        prod_id          => $id,
                        OR_throwArgsUp   => 'prod_name,prod_pic,prod_pricep,prod_pricev',
                        OR_SQL           => 'SELECT prod_pricev, prod_pricep, prod_name, prod_pic from v_products_detail where prod_id=?',
                     }],
    PREPAREFILL   => sub
                     {
                       $_[0]->{link_detail}='?gen=pdetail&prod='.$_[0]->{prod_id};
                       $_[0]->{link_prod_pic}=$CONF{pic_path_80}.$_[0]->{prod_pic};
                     }
  });
   return $out;
};
#=========================================================
sub getBasket()
{
  if($USRPRM{isOper}==1) {return '';};
  my $bid=$REQ{basket_id}+0;
  if ($bid==0) {return '';};
  return SidePane('RIGHT','?gen=pdetail&prod=','?gen=bucket','������� ��������','��� �������',
                          'SELECT prod_id as sql_id,prod_name as sql_name,prod_pricev,prod_pricep,prod_pic from'.
                          ' v_basket_detail where basket_id='.$bid.' limit 6','<center>�����','');
};
#=========================================================
sub Day_prod()
{
   return   (DBSingleValue('Day_prod','SELECT count(1) from v_products_of_day')>0)
   ?
SidePane('RIGHT','','','', '����� ���',
          'SELECT prod_id as sql_id, prod_name as sql_name,prod_pricev,prod_pricep,prod_pic from v_products_of_day',
          '','',
          isOper() ? $Tside->{PRICE_POD_ADM}:$Tside->{PRICE_POD},
          sub
          {
            my $a=shift;



            $a->{link_del}='?'.gen_url('gen,prod,page,cat,typeref,subcat').'&action=del_pod&prodofday='.$a->{sql_id};
            $a->{link_view}='?gen=pdetail&prod='.$a->{sql_id};
            $a->{link_picture} = $CONF{pic_path_80}.$a->{prod_pic};
          }
   ):'';
};
#=========================================================




#=========================================================
%SIDERIGHT;
%SIDELEFT;

$SIDERIGHT{cat}     = sub {return Random_prod('RIGHT');};
$SIDELEFT{cat}      = sub {return sideManlist('LEFT');};

$SIDERIGHT{subcat}  = $SIDERIGHT{cat};
$SIDELEFT{subcat}   = sub {return sideCatlist('LEFT');};

$SIDERIGHT{typeref} = $SIDERIGHT{cat};
$SIDELEFT{typeref}  = sub {return sideSubCatlistA('LEFT',$REQ{subcat}).sideCatlist('LEFT');};

$SIDERIGHT{man}     = $SIDERIGHT{cat};
$SIDELEFT{man}      = $SIDELEFT{subcat};



# ************************** current magaz only one manufaturer
#$SIDERIGHT{manscat} = $SIDERIGHT{cat};
#$SIDELEFT{manscat}  = sub{return sideManlist('LEFT').sideCatlist('LEFT');};

#$SIDERIGHT{mantref} = $SIDERIGHT{cat};
#$SIDELEFT{mantref}  = $SIDELEFT{manscat};
# *******************************************************************

$SIDERIGHT{prods}   = sub{return sideGRPList('RIGHT').sideManProdlist('RIGHT');};
$SIDELEFT{prods}    = sub{return TyperefCatlistA('LEFT',$REQ{typeref}).sideSubCatlistB('LEFT',$REQ{typeref}).sideCatlist('LEFT');};

$SIDERIGHT{pdetail} = $SIDERIGHT{prods};
$SIDELEFT{pdetail}  = $SIDELEFT{prods};

$SIDERIGHT{pimage}  = $SIDERIGHT{pdetail};
$SIDELEFT{pimage}   = $SIDELEFT{pdetail};


$SIDERIGHT{catprm}  = sub{return 'RELEASE THIS';};
$SIDELEFT{catprm}   = sub{return 'RELEASE THIS';};

$SIDERIGHT{tr_grp}  = sub{return 'RELEASE THIS';};
$SIDELEFT{tr_grp}   = sub{return 'RELEASE THIS';};

$SIDERIGHT{bucket}  = $SIDERIGHT{cat};
$SIDELEFT{bucket}   = {};

$SIDERIGHT{search}  = $SIDERIGHT{cat};
$SIDELEFT{search}   = {};

#=========================================================
sub left_part  {
   my $ret='';

   if (ref($SIDELEFT{$REQ{'gen'}}) eq 'CODE')
   {
       $ret= $SIDELEFT{$REQ{'gen'}}->();
   }else
   {
       $ret= $SIDELEFT{cat}->();
   };
   return $ret;
};
#=========================================================
sub right_part {
   my $ret='';
   if (ref($SIDERIGHT{$REQ{'gen'}}) eq 'CODE')
   {
       $ret=$SIDERIGHT{$REQ{'gen'}}->();
   }else
   {
     $ret=$SIDERIGHT{cat}->();
   };

   return $ret.getBasket().Day_prod();
};
#=========================================================
sub all_part()
{
 return right_part().left_part();
};
#=========================================================

1;
