#$Tman = LoadBlockTemplate('./interface/'.$CONF{interface}.'/ak56/man');

sub SMAN_AREC($){
  my $a=$_[0];
  my %PRM=%$a;
  if ((($PRM{corrow}+1) % $PRM{ITEMPERROW})==0)
  {
    $PRM{RETTEMPFLD}.=$Tman->{SBM_AR};
  }else{
#    $PRM{RETTEMPFLD}.='<td>&nbsp;</td>';
  };
  return \%PRM;
};
  #-----------------------
sub SMAN_BREC($){
  my $a=$_[0];
  my %PRM=%$a;
  if ((($PRM{corrow}+1) % $PRM{ITEMPERROW})==1)
  {
    $PRM{RETTEMPFLD}.=$Tman->{SBM_AR};
  };
  return \%PRM;
};
  #-----------------------
sub SMAN_AFTER($){
  my $a=$_[0];
  my %PRM=%$a;
  my $c=($PRM{corrow}+1) % $PRM{ITEMPERROW};

  if ($c!=0)
  {
    $c=$PRM{ITEMPERROW}-$c;
    while($c>0)
    {
      $PRM{RETTEMPFLD}.=$Tman->{SBM_AFT};
      $c--;
    };
  };
    return \%PRM;
};

sub SMAN_EMPTY()
{
  my $a=$_[0];
  my %PRM=%$a;
$PRM{RETTEMPFLD}.=$Tman->{SBM_EMPTY};
   return \%PRM;
};

  #-----------------------
sub SubMans($$$$) #catid
{
  my ($ref,$sql,$prow,$id)=@_;
  #-----------------------
   return DBTEMPLATEFILL(
   {
     Template    => $Tman->{SBM_BODY},
#     DivTemplate => '',
     SubTemplDB  => [{
                       RetStorage      => 'body',
                       Template        => $Tman->{SBM_ITEM},
                       CB_SQL          => $sql,
                       FLDPREF         => '',
                       ITEMPERROW      => $prow,
                       CB_SQLARGSARDER => 'filter_id',
                       filter_id       => $id,
                       tmp             => $ref,
                       CB_WORKBEF      => \&SMAN_BREC,
                       CB_WORKAFT      => \&SMAN_AREC,
                       CB_AFTER        => \&SMAN_AFTER,
                       CB_EMPTY        => \&SMAN_EMPTY,
                       PREPAREFILL     => sub
                                          {
                                            $_[0]->{link} = $_[0]->{tmp}.$_[0]->{man_id};
                                          }
                    }]
   });
};
#---------------------------------------------------------
sub QQ1($){return  'SELECT mrg.id_man FROM t_products mrg where mrg.id_type_grp '.$_[0];};
sub QQ2($){return  'SELECT id FROM t_prod_type_ref_grp trg where trg.owner '.$_[0];};
sub QQ3($){return  'SELECT typeref.id FROM t_prod_type_ref typeref where typeref.owner '.$_[0];};
sub QQ4($){return  'SELECT id FROM t_prod_type_subcat subcat where owner '.$_[0];};

sub QQ5($){return  "SELECT man.id as man_id,man.name as man_name ,concat(IFNULL(pic.id,'0'),'.',IFNULL(pic.extens,'png')) as man_pic ".
                   ' FROM t_man as man left join t_pictures pic on man.pic_id=pic.id and pic.visible=1 where man.id in ('.$_[0].')';};


sub SM_BYCAT()    { return QQ5(QQ1('in('.QQ2('in('.QQ3('in('.QQ4('=?').')').')').')'));};
sub SM_BYSUBCAT() { return QQ5(QQ1('in('.QQ2('in('.          QQ3('=?')     .')').')'));};
sub SM_BYTYPEREF(){ return QQ5(QQ1('in('.                    QQ2('=?')          .')'));};
sub SM_BYREFGRP() { return QQ5(                              QQ1('=?')               );};


sub PUT_SM_BYCAT    { return SubMans('?gen=manscat&man=',   SM_BYCAT(),5,$REQ{cat});};
sub PUT_SM_BYSUBCAT { return SubMans('?gen=mantref&subcat='.$REQ{subcat}.'&man=',SM_BYSUBCAT(),5,$REQ{subcat});};

#print SM_BYCAT();
 #by cat
 #by subcat
#sidebar
 #by type ref
 #by type ref grp

1;





