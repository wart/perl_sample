


  $CONF{LOCALPATH}='../htdocs';
  $CONF{avatarpath}='/avatars/';
  $CONF{smallpath}='small/';
  $CONF{mailsender}='myFeast.ru';
  $CONF{servername}='localhost';

$sql_tool =LoadBlockTemplate('./sql/tool.bsq');

#---------------------------------------------------------------
sub get_user_info
{
   DBFetchHashQ('get_user_info',$sql_tool->{user_info},[$SESS{'uid'}],\%SESS,'','');
#   print get_user_info."\n";
};
#---------------------------------------------------------------
sub gen_single_hidden($$){ return '<input type=hidden name='.$_[0].' value="'.$_[1].'">'."\n";};
#---------------------------------------------------------------
sub gen_hiddens($)
{
  my $ret='';
  my $arg= shift;
  if (ref($arg) eq 'HASH')
  {
  #  $ret.='HASH';
    foreach my $k (keys %$arg)
    {
      if ($arg->{$k} ne '')
      {
        $ret.=gen_single_hidden($k,$arg->{$k});
      }
      else
      {
        $ret.=gen_single_hidden($k,$REQ{$k});
      };
    };
  }
  else
  {
  # $ret.='STR:'.$arg;
    my @r=split(',',$arg);
    foreach my $t (@r)
    {
   #   $ret.='-'.$t;
      my @e=split(':',$t);
      my $se=@e;
      if ($se>1)
      {
        $ret.=gen_single_hidden($e[0],$e[1]);
      }else{
        $ret.=gen_single_hidden($e[0],$REQ{$e[0]});
      };
    };
  };
  return $ret;
};
#---------------------------------------------------------------
sub notIn # name arr ref
{
  foreach my $r (@{$_[1]})
  {
    if ($_[0] eq $r)
    {
      return 0;
    };
  };

  foreach my $r (@prm_cookie)
  {
    if ($_[0] eq $r)
    {
      return 0;
    };
  };

  return 1;
};
#---------------------------------------------------------------
sub putREQHiddens
{
  my $ret='';
  foreach my $r (keys %REQ)
  {
    if(notIn($r,$_[0]))
    {
        $ret.=gen_single_hidden($r,$REQ{$r});
    };
  };
  return $ret;
};
#---------------------------------------------------------------
sub GridView_LineBegin
{
  my $a = shift;
  if ((($a->{corrow}+1)% $a->{ItemPerLine})==1)
  {
    $a->{RETTEMPFLD}.=$a->{LineBegin};
  };
};
#---------------------------------------------------------------
sub GridView_LineEnd
{
  my $a = shift;
  if ((($a->{corrow}+1)% $a->{ItemPerLine})==0)
  {
    $a->{RETTEMPFLD}.=$a->{LineEnd};
  };
};
#---------------------------------------------------------------
sub GridView_LineFinish
{
  my $a = shift;
  my $c=$a->{corrow} % $a->{ItemPerLine};
  if ($c>0)
  {
    for (my $i=$c;$i<$a->{ItemPerLine};$i++)
    {
      $a->{RETTEMPFLD}.=$a->{LineEmptyItem};
    };
    $a->{RETTEMPFLD}.=$a->{LineEnd};
  };
};
#---------------------------------------------------------------
sub town_present
{
    return DBCountPresent('town_present',$sql_tool->{'town_present'},$_[0]);
};
#---------------------------------------------------------------
sub fill_select # mtemp rets stemp sql curval
{
    my ($mtemp,$rets,$stemp,$sql,$curval)=@_;
    my $F=
    {
        Template   =>$mtemp,
        SubTemplDB =>
        [{
                RetStorage      => $rets,
                Template        => $stemp,
                CB_SQL          => $sql,
                CURVAL          => $curval,
                CB_WORKBEF      => sub
                                   {
                                     my $a = shift;
                                     $a->{current}=($a->{CURVAL}==$a->{id})?'selected':'';
                                   }
        }]
    };
    return DBTEMPLATEFILL($F);
};
#---------------------------------------------------------------
1;
