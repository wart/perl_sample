
$sql_sess=LoadBlockTemplate('./sql/sess.bsq');
$CONF{default_town}=1;
%ACTS;
$ACTS{abcdef_123456}=\&RDE;

#-------------------------------------------------------------------------------
sub sess_check
{
          DBexec('sess_check1',$sql_sess->{'kill_old_sess'});
          DBexec('sess_check2',$sql_sess->{'fresh_sess_time'},$REQ{'nbsess'},$SESS{'ip'});
  ($c,@w)=DBexec('sess_check3',$sql_sess->{'get_sess_user'},$REQ{'nbsess'},$SESS{'ip'});
    return ($c==1)?$w[0]:0;
};
#-------------------------------------------------------------------------------
sub logonB # login pwd
{
  ($c,@w)=DBexec('logon',$sql_sess->{'logon'},$_[0],$_[1]);
  return ($c==1)?$w[0]:0;
};
#-------------------------------------------------------------------------------
sub logon
{
        $REQ{'nbsess'}='guest';
        if ($REQ{'site_login'} eq ""){  return -1;};
        if ($REQ{'site_pwd'}   eq ""){  return -2;};
        $SESS{'uid'}=logonB($REQ{'site_login'},$REQ{'site_pwd'});
        if ($SESS{'uid'}>0)
        {
           my $sid=substr(int(rand()*1000000000000),0,15);
            add_prm_sess('nbsess',$sid);
            add_prm_req ('nbsess',$SESS{'nbsess'});
            add_cookie  ('nbsess',$REQ{'nbsess'});
            DBexec('authentification1',$sql_sess->{'sess_add'},$SESS{'nbsess'},$SESS{'uid'},$SESS{'ip'});
            DBexec('fresh_account_time',$sql_sess->{'fresh_account_time'},$SESS{uid});
        }else{
            return -3;
        };
        return 1;
};
#-------------------------------------------------------------------------------
sub logout
{
    $SESS{'uid'}=0;
    $REQ{'nbsess'}='guest';
    DBexec('authentification2',$sql_sess->{'logout'},$SESS{'nbsess'},$SESS{'ip'});
};
#-------------------------------------------------------------------------------
sub sess_alive
{
        if ($REQ{'nbsess'} ne 'guest')
        {
            $SESS{'uid'}=sess_check();
            if ($SESS{'uid'}>0)
            {
                $SESS{'nbsess'}=$REQ{'nbsess'};
                DBexec('fresh_account_time',$sql_sess->{'fresh_account_time'},$SESS{uid});
            }else{
                $REQ{'nbsess'}='guest';
            };
        }else{$SESS{'uid'}=0;};
};
#-------------------------------------------------------------------------------
sub sess_town
{
    $REQ{select_town} +=0;# if($REQ{select_town} <0){$REQ{select_town} =$CONF{default_town};};
    $REQ{coockie_town}+=0; if($REQ{coockie_town}<0){$REQ{coockie_town}=$CONF{default_town};};

    if(town_present($REQ{select_town}))
    {
        add_cookie('coockie_town',$REQ{select_town});
        $SESS{'town_id'}=$REQ{select_town};
    }else
    {
      if(town_present($REQ{coockie_town}))
      {
         $SESS{'town_id'}=$REQ{coockie_town};
      }
      else
      {
        $SESS{'town_id'}= $CONF{default_town};
      };
    };
};
#-------------------------------------------------------------------------------
sub authentification
{
   if ($SESS{'ip'} ne '')
   {

    my $EM ={-1=>'�� ������ �����',
             -2=>'�� ������ ������',
             -3=>'�� ������ ����� ��� ������'};

    if ($REQ{'site_trylogon'}==1)
    {
         my $r=logon(); $REQ{autherror}.=$EM->{$r};
         if ($r>0)
         {
             get_user_info();
            add_cookie('coockie_town',0);
         }else
         {
             sess_town();
         };
         return $r;
    }
    else
    {
         sess_alive();
         get_user_info();
    };
    if($REQ{'site_auth_out'}==1) {       logout();    };
    sess_town();
    return 1;
  };
  return 0;
};
#-------------------------------------------------------------------------------
1;
