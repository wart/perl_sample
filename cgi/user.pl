

$sql_user = LoadBlockTemplate('./sql/user.bsq');
$Tuser    = LoadBlockTemplate('./interface/'.$CONF{interface}.'/userpane');
$CONF{admin_role}=1;
#--------------------------------------------------------------
sub isUser
{
  return $SESS{uid}>0;
};
#--------------------------------------------------------------
sub isAdmin
{
  my $w=DBSingleValue('isAdmin',$sql_user->{'user_role'},$SESS{uid});
  return $w==$CONF{admin_role};
};

#--------------------------------------------------------------
sub gen_user_pane
{
  my $UP;
  if($SESS{uid}>0)
  {
    $UP=
    {
      Template         => $Tuser->{userpane},
      lnk_setup        => '?gen=usersetup',
      lnk_exit         => '?'.getGENUrl().'&site_auth_out=1',
    };

    if(!(($REQ{gen} eq 'viewuser')&&($REQ{user_id}==$SESS{uid})))
    {
      $UP->{lnk_mypage}      = '?gen=viewuser&user_id='.$SESS{uid};
      $UP->{mypage}          = $Tuser->{mypage};
    }else
    {
      $UP->{lnk_edit_mypage} = '?gen=edituser';
      $UP->{mypage}          = $Tuser->{edit_mypage};
    };
      if (isAdmin())
      {
        $UP->{lnk_addtown}='?gen=addtown';
        $UP->{lnk_adduref}='?gen=adduref';
        $UP->{admin}=$Tuser->{admin};
      };

  }else
  {
    $UP = {
            Template     => $Tuser->{authform},
            lnk_pwdrecov => '?gen=pwdrecovery',
            lnk_reg      => '?gen=reg',
            AUTHFIELDS   => gen_hiddens({site_trylogon=>1}).putNAVREQHiddens()
          };
  };
  return '----------'.DBTEMPLATEFILL($UP).'----------';
};

#--------------------------------------------------------------
sub gen_changepwd
{
  if(isUser())
  {
   return fill_template($Tuser->{changepwd},
                {
      CHPWDFIELDS=> gen_hiddens({gen=>'changepwd',action=>'changepwd'})
   });
  }else
  {
    return center_part(0);
  };
};

#--------------------------------------------------------------
sub act_changepwd
{
  if(isUser())
  {
    if    ($REQ{chpwd_op} ne '')
    {
      if  ($REQ{chpwd_np} ne '')
      {
        if($REQ{chpwd_np2} eq $REQ{chpwd_np})
        {
           if(DBCountPresent('act_changepwd',$sql_user->{check_pwd},$REQ{chpwd_op},$SESS{uid}))
           {
              DBexec('act_changepwd2',$sql_user->{set_pwd},$REQ{chpwd_np},$SESS{uid});
              $REQ{gen}='';
              $REQ{sys_message}='������ �������';
           }
           else
           {
             $REQ{sys_message}='�� ������ ������ ������';
           };
        }else
        {
             $REQ{sys_message}='�� ��������� ����� ������ ������';
        };
      }else
      {
             $REQ{sys_message}='�� ������ ����� ������';
      };
    }else
    {
             $REQ{sys_message}='�� ������ ������ ������';
    };
  }else
  {
    $REQ{gen}='';
  };
};
#--------------------------------------------------------------
sub gen_pwdrecovery
{
   return fill_template($Tuser->{pwdrecovery},
   {
      PWDRECOVFIELDS=> gen_hiddens({gen=>'pwdrecovery',action=>'pwdrecovery'})
   });
};
#--------------------------------------------------------------
sub act_pwdrecovery
{
    if(!(isUser()))
    {
        if($REQ{recov_email} ne '')
        {
            if ($REQ{reg_login} =~ /^(\w\-\_\.)+\@((\w\-\_)+\.)+[a-zA-Z]{2,}$/)
            {
                my $uid=DBSingleValue('act_pwdrecovery1',$sql_user->{uid_by_login},$REQ{recov_email});
                if($uid>0)
                {
                    my $newpwd=substr(int(rand()*100000000000000),0,5);
                    DBexec('act_pwdrecovery2',$sql_user->{set_pwd},$newpwd,$uid);
                    my $msg = MIME::Lite->new
                    (
                        From    => $CONF{mailsender},
                        To      =>  $REQ{reg_login},
                        Subject => 'pawssword recovery',
                        Data    => '��� ������:'.$newpwd,
                       Type    => 'text/html; charset=windows-1251'
                    );
                    $msg->send;
                    $REQ{sys_message}='������ ������ �� ��������� e-mail';
                }else{   $REQ{sys_message}='������ e-mail �� ��������������� � �������.';};
    }else{      $REQ{sys_message}='�� ���������� e-mail';};
    }else{      $REQ{sys_message}='�� ������ e-mail';};
  };
};
#--------------------------------------------------------------
sub gen_reg
{
   return fill_template($Tuser->{regform},{
          REGFIELDS=> gen_hiddens({
                      gen=>'',
                      action=>'reg'
                      }),
          roleplace=>fill_select($Tuser->{roleplace},'roles',$Tuser->{singlerole},$sql_user->{roles},$REQ{reg_role}),
          townplace=>fill_select($Tuser->{townplace},'towns',$Tuser->{singletown},$sql_user->{towns},$REQ{reg_town})
          });
};
#--------------------------------------------------------------
sub delete_old_requests
{
  DBexec('delete_old_requests1',$sql_user->{del_old_reqs});
  DBexec('delete_old_requests2',$sql_user->{del_old_req_users});
};
#--------------------------------------------------------------
sub act_reg
{
  if(!(isUser()))
  {
#    if ($REQ{reg_login} =~ /^(\w\-\_\.)+\@((\w\-\_)+\.)+[a-zA-Z]{2,}$/){
    if ($REQ{reg_login} =~ /^[\w.-]+@[\w.-]+.\w{2,4}$/){


    if(! DBCountPresent('act_reg1',$sql_user->{login_present},$REQ{reg_login})){
     if(length($REQ{reg_pwd})>4){
      if($REQ{reg_pwd} eq $REQ{reg_pwd2}){
       if(DBCountPresent('act_reg2',$sql_user->{role_present},$REQ{reg_role})){
        if(town_present($REQ{reg_town})){
         if($REQ{reg_fio} ne ''){
          if($REQ{reg_nick} ne ''){
           if(DBexec('act_reg4',$sql_user->{reg},$REQ{reg_role},$REQ{reg_town},$REQ{reg_bd},$REQ{reg_login},$REQ{reg_pwd},$REQ{reg_fio},$REQ{reg_nick}))
           {
             my $uid=DBlast_insert_id();
             my $rid=substr(int(rand()*100000000000000),0,15);
             DBexec('act_reg5',$sql_user->{add_req},$rid,$uid);
             delete_old_requests();
             my $url= 'http://'.$CONF{servername}.'/cgi-bin/enter.cgi?action=acc_activate&request_id='.$rid;

             my $msg = MIME::Lite->new
             (
                From    =>  $CONF{mailsender},
                To      =>  $REQ{reg_login},
                Subject => 'Account activation',
                Data    => '��� ��������� �������� ��������� �� ������ '.$url,
                Type    => 'text/html; charset=windows-1251'
             );

             $msg->send;
             $REQ{gen}=
             $REQ{reg_role}=
             $REQ{reg_town}=
             $REQ{reg_bd}=
             $REQ{reg_login}=
             $REQ{reg_pwd}=
             $REQ{reg_fio}=
             $REQ{reg_nick}='';


            $REQ{sys_message}='����������� ����������� ������ �������.<br>'.
                              '����� ��������� ����� �� ��������� ���� e-mail ������ ������ � ������ ��� ��������� ��������.';
           }else{$REQ{sys_message}='������';};
          } else{$REQ{sys_message}='�� ������ ���';};
         }  else{$REQ{sys_message}='�� ������� ���';};
        }   else{$REQ{sys_message}='�� ����� ������ �����';};
       }    else{$REQ{sys_message}='�� ������ ������� ����';};
      }     else{$REQ{sys_message}='������ �� ���������';};
     }      else{$REQ{sys_message}='������� �������� ������';};
    }       else{$REQ{sys_message}='����� ����� ��� ����������';};
    }       else{$REQ{sys_message}='�� ���������� email ������';};
  }         else{$REQ{sys_message}='��������� ����������� �� ���������';};
};
#--------------------------------------------------------------
sub act_acc_activate
{
  delete_old_requests();
  if(!(isUser()))
  {
   $rid=$REQ{request_id}+0;
    my $uid= DBSingleValue('act_acc_activate',$sql_user->{req_acc_uid},$rid);

    if($uid>0)
    {
      DBexec('act_acc_activate1',$sql_user->{acc_activate},$uid);
      DBexec('act_acc_activate2',$sql_user->{del_activated},$rid,$uid);
      $REQ{sys_message}.='��������� ������ �������. ������ ������ �������������� � ��������� ������.';
    }else{$REQ{sys_message}.='�� ������ ��� ���������';};
  };
};
#--------------------------------------------------------------
sub isMyPage
{
  return $SESS{uid}==$_[0];
};
#--------------------------------------------------------------
sub userPresent
{
  return DBCountPresent('userPresent',sql_user->{user_present},$_[0]);
};
#--------------------------------------------------------------
1;

