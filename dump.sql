CREATE DATABASE  IF NOT EXISTS `lights` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lights`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: lights
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_prodofday`
--

DROP TABLE IF EXISTS `t_prodofday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prodofday` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prodofday`
--

LOCK TABLES `t_prodofday` WRITE;
/*!40000 ALTER TABLE `t_prodofday` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_prodofday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_status`
--

DROP TABLE IF EXISTS `t_user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cap` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Статусы аккаунтов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_status`
--

LOCK TABLES `t_user_status` WRITE;
/*!40000 ALTER TABLE `t_user_status` DISABLE KEYS */;
INSERT INTO `t_user_status` VALUES (1,'active'),(2,'wait activation');
/*!40000 ALTER TABLE `t_user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_gmt`
--

DROP TABLE IF EXISTS `t_gmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_gmt` (
  `id` varchar(6) NOT NULL,
  `cap` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_gmt`
--

LOCK TABLES `t_gmt` WRITE;
/*!40000 ALTER TABLE `t_gmt` DISABLE KEYS */;
INSERT INTO `t_gmt` VALUES ('+00:00','(GMT) Дублин,Лондон, Лиссабон, Эдинбург, Касабланка'),('+01:00','(+01:00) Амсердам,Берлин,Берн,Вена,Рим,Братеслава,Будапешт'),('+02:00','(+02:00) Бухарест,Вильнюс, Киев, Рига,София,Иерусалим,Каир,Хараре'),('+03:00','(+03:00) Багдад, Кувейт, Эр-Рияд, Москва,С.Петербург,Найроби'),('+03:30','(+03:30) Тегеран'),('+04:00','(+04:00) Абу-Даби, Мускат, Баку, Ереван, Тбилиси'),('+04:30','(+04:30) Кабул'),('+05:00','(+05:00) Екатербург, Оренбург,Ташкент, Карачи,Исламабад'),('+05:30','(+05:30) Бомбей, Калькутта, Мадрас, Нью-Дели'),('+05:45','(+05:45) Катманд'),('+06:00','(+06:00) Астана, Дхака, Омск, Новосибирск,Алма-Ата'),('+06:30','(+06:30) Рангун'),('+07:00','(\n+07:00) Бангкок, Джакарта, Ханой, Красноярск'),('+08:00','(+08:00) Гонконг,Пекин,Петр, Урумчи,Иркутск,Улан-Батор'),('+09:00','(+09:00) Осака,Саппоро, Токио, Якутск,Сеул,'),('+09:30','(+09:30) Аделаида, Дарвин'),('+10:00','(+10:00) Брисбейн, Владивосток,Гуам,Мельбурн,Сидней,Хобарт'),('+11:00','(+11:00) Магадан, Сахалин, Соломоновы о-ва'),('+12:00','(+12:00) Камчатка,Фиджи, Маршалловы о-ва,Окленд,Веллингтон'),('+13:00','(+13:00) Нуку-алофа'),('-01:00','(-01:00) о-ва Зеленого мыса, Азорские о-ва'),('-02:00','(-02:00) Среднеатлантическое время'),('-03:00','(-03:00) Бразилия, Буэнос-Айрес, Джорджтаун, Гренландия'),('-03:30','(-03:30) Ньюфаундленд'),('-04:00','(-04:00) Атлантичес'),('-05:00','(-05:00) Богота, Лима, Кито, Индиана'),('-06:00','(-06:00) Гвадалахара, Мехико, Монтеррей,Саскачеван'),('-07:00','(-07:00) Аризона, Ла Пас, Мазатлан, Чихуахуа'),('-08:00','(-08:00) Тихоокеанское время, Тихуана'),('-09:00','(-09:00) Аляска'),('-10:00','(-10:00) Гавайи'),('-11:00','(-11:00) о. Мидуэй, Самоа'),('-12:00','(-12:00) Меридиан смены дат (запад)');
/*!40000 ALTER TABLE `t_gmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_papers`
--

DROP TABLE IF EXISTS `t_papers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_papers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_dt` datetime NOT NULL,
  `cap` varchar(128) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='`id` int(11) NOT NULL auto_increment,\n  `post_dt` datetime N';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_papers`
--

LOCK TABLES `t_papers` WRITE;
/*!40000 ALTER TABLE `t_papers` DISABLE KEYS */;
INSERT INTO `t_papers` VALUES (1,'2012-12-12 00:00:00','Контакты','<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; padding: 0px;\">Офис и склад.</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; padding: 0px;\">Адрес &mdash; 123298, РФ, г. Москва, ул. Маршала&nbsp;<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Бирюзова</span>,&nbsp;дом 1, корпус 3.&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; padding: 0px;\"><span style=\"color: #ff6600;\">На проходной выписать пропуск в компанию ООО МАГАЗИН СВЕТ при себе иметь паспорт или водительское удостоверение.</span></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\">Телефон (многоканальный) :&nbsp;<span class=\"skype_pnh_container\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: none !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; text-align: left !important; top: auto !important; white-space: nowrap !important; word-spacing: normal !important; z-index: 0 !important; color: #49535a !important; font-family: Tahoma, Arial, Helvetica, sans-serif !important; font-size: 11px !important; font-weight: bold !important; height: 14px !important; line-height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" dir=\"ltr\">&nbsp;<span class=\"skype_pnh_highlighting_inactive_common\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: none !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Позвонить через Skype на следующий номер (Россия): +74955404079\" dir=\"ltr\"><span class=\"skype_pnh_left_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 6px !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Действия Skype\">&nbsp;&nbsp;</span><span class=\"skype_pnh_dropart_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 27px !important; background-position: -11px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Действия Skype\"><span class=\"skype_pnh_dropart_flag_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/flags.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 18px !important; background-position: 1px 1px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;</span><span class=\"skype_pnh_textarea_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: -125px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\"><span class=\"skype_pnh_text_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; padding-left: 5px !important; padding-top: 0px !important; padding-right: 0px !important; padding-bottom: 0px !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: -125px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; margin: 0px !important;\">(495) 540-40-79</span></span><span class=\"skype_pnh_right_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 15px !important; background-position: -62px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>&nbsp;</span><strong><br /> </strong></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\">Внутренние номера сотрудников:</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Серебрякова</span>&nbsp;Ольга - доб.101</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Епишина</span>&nbsp;Татьяна &nbsp; &nbsp; -доб.102</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Ильясова</span>&nbsp;Наталья &nbsp; &nbsp; -доб.103</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Прокопович</span>&nbsp;Сергей -доб.104</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\">По России (бесплатно) &mdash; 8 (800) 555-14-21.</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">EMAIL</span>: info@<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">svetilniki</span>-online.<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">ru</span></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\">Время работы:</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; color: #1a1a1a; padding: 0px;\">&nbsp;Понедельник -&nbsp;Пятница:&nbsp;<span style=\"color: #00ff00;\">с 09:00 - 18:00</span>,</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; font-family: verdana, arial, sans-serif; line-height: 16px; padding: 0px;\"><span style=\"color: #1a1a1a;\">&nbsp;Суббота и Воскресенье:&nbsp;</span><span style=\"color: #ff0000;\">Выходной</span><span style=\"color: #1a1a1a;\">&nbsp;&nbsp;</span><span style=\"color: #1a1a1a;\">&nbsp;</span></p>'),(2,'2012-12-20 15:14:44','Доставка','<div class=\"content\">\r\n<p><strong>ДОСТАВКА по МОСКВЕ:</strong></p>\r\n<p>Минимальная сумма заказа для курьерской доставки 3000 рублей.&nbsp;</p>\r\n<p>Доставка в пределах МКАД осуществляется курьером, услуга стоит 300 руб. Если сумма вашего заказа более 50000 руб, доставка осуществляется БЕСПЛАТНО. Заказы доставляются по предварительному согласованию с менеджером нашего магазина.</p>\r\n<p>Возможность&nbsp;доставки за пределы МКАД рассматривается индивидуально в каждом конкретном случае и зависит от дальности расположения и удобства проезда. Стоимость 300 + 30 рублей за километр от МКАД.</p>\r\n<p>Уважаемые клиенты, просьба не задерживать курьера более 20 минут.</p>\r\n<p>ПОДЪЕМ&nbsp;НА&nbsp;ЭТАЖ: при объемных заказах оплачивается дополнительно, стоимость услуги 200 руб.</p>\r\n<p><strong>Обращаем Ваше внимание&nbsp;</strong>на то, что установка и подключение техники сотрудниками компании, производящей доставку, не осуществляется!<br /> <br /> <strong>ДОСТАВКА товаров по РОССИИ:</strong></p>\r\n<p>Отправка товара по России осуществляется после 100% оплаты.</p>\r\n<p>Отправка через транспортные компании осуществляется &nbsp;на основании доверенности:</p>\r\n<p><a href=\"http://www.svetilniki-online.ru/netcat_files/File/Doverennost_Ur_L.doc\">Для юридических лиц скачать.</a></p>\r\n<p><a href=\"http://www.svetilniki-online.ru/netcat_files/File/Doverennost_fiz_L.doc\">Для физического лица скачать.</a><br /> <br /> Мы активно работаем с транспортными компаниями.</p>\r\n<p><span><strong>Выбрать оптимального перевозчика товаров по России, очень просто,&nbsp;используйте калькулятор на сайте перевозчика!&nbsp;</strong></span></p>\r\n<p>Для расчета стоимости Вам потребуется ввести пункт назначения, вес и объем груза.</p>\r\n<p>Сайт компании:&nbsp;<a href=\"http://www.ponyexpress.ru/tariff.php\">PONY EXPRESS</a>&nbsp; возможна экспресс доставка ко времени.</p>\r\n<p>Сайт компании: <a href=\"http://www.spasibo.ru/shipping_calculator/normal/\">СПАСИБО.РУ</a></p>\r\n<p>Сайт компании:&nbsp;<a href=\"http://www.ae5000.ru/rates/calculate/\">АВТОТРЕЙДИНГ</a></p>\r\n<p>Сайт компании:&nbsp;<a href=\"http://www.pecom.ru/ru/calc/\">ПЭК</a>&nbsp;</p>\r\n<p>Сайт компании:&nbsp;<a href=\"http://www.dellin.ru/\">ДЕЛОВЫЕ ЛИНИИ</a></p>\r\n<p>На сайте компании перевозчика можно рассчитать тариф &nbsp;доставки груза от нашего склада до Вашего города или склада.</p>\r\n<p>Так же можно оформить заявку на забор груза из нашего офиса самостоятельно.</p>\r\n<p>Вес и объем груза Вы сможете узнать только после оплаты и консолидации груза на нашем центральном складе.&nbsp;<br /> <br /> Доставка от нашего склада до транспортной компании для Вас БЕСПЛАТНАЯ.<br /> <br /> Срочный забор и отправка груза через транспортную компанию оплачивается покупателем по тарифам транспортной компании.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>САМОВЫВОЗ:</strong></p>\r\n<p>Заказы, осуществляемые самовывозом из нашего офиса, не имеют никаких ограничений по сумме.&nbsp;&nbsp;При этом товар, забираемый самовывозом из нашего офиса, требует 100% предоплаты.</p>\r\n<p>Вы можете забрать заказанный товар из нашего офиса по адресу: г.Москва, Ул.Маршала Бирюзова д.1 корп.3 &nbsp;Часы работы с 10 до 18 часов, суббота/воскресение - выходной.</p>\r\n</div>'),(3,'2012-12-20 15:35:16','О Магазине','<p><span>Светотехническая торговая компания&nbsp; ООО \"МАГАЗИН СВЕТ\" представляет Вам свой новый проект интернет магазина СВЕТИЛЬНИКИ ОНЛАЙН </span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>С помощью нашего сайта Вы сможете легко и просто подобрать светильники, люстры, бра, торшеры для квартиры или дома.</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>&nbsp;На сайте представлен большой ассортимент светильников и люстр, что поможет Вам без труда решить любую задачу по освещению. </span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>&nbsp;Наша основная задача была создать удобный систематизированный каталог продукции, мы надеемся, что это поможет Вам в выборе светильников и люстр.</span></p>\r\n<p><span>&nbsp;</span></p>\r\n<p><span>Наш многолетний опыт продаж светотехнической продукции позволил создать&nbsp; нечто большее, чем сайт - это универсальный инструмент по подбору осветительного оборудования.&nbsp;</span></p>\r\n<p>&nbsp;</p>\r\n<p>Мы всегда рады видеть Вас в числе наших клиентов, выбрать светильники, люстры, бра, торшеры, можно у нас в офисе:</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>С уважением OOO \"МАГАЗИН СВЕТ\"</p>\r\n<p>&nbsp;</p>'),(4,'2012-12-21 12:43:35','Как купить?','<h3>Уважаемые Дамы и Господа! Вы сможете приобрести товары, выбрав один из способов.</h3>\r\n<div class=\"fck_center\"><span class=\"fck_orange\"><strong><big>Офисы продаж:&nbsp;</big></strong></span><strong> <span class=\"fck_red\">МОСКВА</span>&nbsp;</strong></div>\r\n<p>&nbsp;Офис и склад.</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px;\">Адрес &mdash; 123298, РФ, г. Москва, ул. Маршала&nbsp;<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Бирюзова</span>,&nbsp;дом 1, корпус 3.&nbsp;</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px;\"><span style=\"color: #ff6600;\">На проходной выписать пропуск в компанию ООО МАГАЗИН СВЕТ при себе иметь паспорт или водительское удостоверение.</span></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">Телефон (многоканальный) :&nbsp;<span class=\"skype_pnh_container\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: none !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; text-align: left !important; top: auto !important; white-space: nowrap !important; word-spacing: normal !important; z-index: 0 !important; color: #49535a !important; font-family: Tahoma, Arial, Helvetica, sans-serif !important; font-size: 11px !important; font-weight: bold !important; height: 14px !important; line-height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" dir=\"ltr\">&nbsp;<span class=\"skype_pnh_highlighting_inactive_common\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: none !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Позвонить через Skype на следующий номер (Россия): +74955404079\" dir=\"ltr\"><span class=\"skype_pnh_left_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 6px !important; background-position: 0px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Действия Skype\">&nbsp;&nbsp;</span><span class=\"skype_pnh_dropart_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 27px !important; background-position: -11px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\" title=\"Действия Skype\"><span class=\"skype_pnh_dropart_flag_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/flags.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 18px !important; background-position: 1px 1px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;</span><span class=\"skype_pnh_textarea_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: -125px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\"><span class=\"skype_pnh_text_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; padding-left: 5px !important; padding-top: 0px !important; padding-right: 0px !important; padding-bottom: 0px !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: auto !important; background-position: -125px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; margin: 0px !important;\">(495) 540-40-79</span></span><span class=\"skype_pnh_right_span\" style=\"background-attachment: scroll !important; background-color: transparent !important; background-image: url(\'chrome-extension:/lifbcibllhkdhoafpjfnlhfpfgnpldfl/numbers_common_inactive_icon_set.gif\') !important; border-color: #000000 !important; border-image: initial !important; border-collapse: separate !important; bottom: auto !important; clear: none !important; clip: auto !important; cursor: pointer !important; direction: ltr !important; display: inline !important; float: none !important; left: auto !important; letter-spacing: 0px !important; list-style-image: none !important; list-style-position: outside !important; list-style-type: disc !important; overflow-x: hidden !important; overflow-y: hidden !important; page-break-after: auto !important; page-break-before: auto !important; page-break-inside: auto !important; position: static !important; right: auto !important; table-layout: auto !important; top: auto !important; word-spacing: normal !important; z-index: 0 !important; height: 14px !important; vertical-align: baseline !important; width: 15px !important; background-position: -62px 0px !important; background-repeat: no-repeat no-repeat !important; border-width: 0px !important; border-style: none !important; padding: 0px !important; margin: 0px !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>&nbsp;</span><strong><br /> </strong></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">Внутренние номера сотрудников:</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Серебрякова</span>&nbsp;Ольга - доб.101</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Епишина</span>&nbsp;Татьяна &nbsp; &nbsp; -доб.102</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Ильясова</span>&nbsp;Наталья &nbsp; &nbsp; -доб.103</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Прокопович</span>&nbsp;Сергей -доб.104</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">По России (бесплатно) &mdash; 8 (800) 555-14-21.</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\"><span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">EMAIL</span>:&nbsp;<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">info</span>@<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">svetilniki</span>-online.<span class=\"J-JK9eJ-PJVNOc\" style=\"background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">ru</span></p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">Время работы:</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">&nbsp;Понедельник - Пятница:&nbsp;<span style=\"color: #00ff00;\">с 09:00 - 18:00</span>,</p>\r\n<p style=\"margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; color: #1a1a1a; padding: 0px;\">&nbsp;Суббота и Воскресенье:&nbsp;<span style=\"color: #ff0000;\">Выходной</span>&nbsp;&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><span class=\"fck_orange\"><strong>ЗАКАЗ ТОВАРОВ:</strong></span></p>\r\n<p><span style=\"color: #00ff00;\">Через корзину сайта:</span> Положите товар в корзину, заполните форму и отправьте заявку.</p>\r\n<p><span style=\"color: #00ff00;\">Через сообщение электронной почты: </span>Пришлите в произвольной форме заявку на адрес: info@svetilniki-online.ru<br /> <br /> <span style=\"color: #00ff00;\">Позвоните по телефону:</span> &nbsp;Вы можете сделать заказ или сформировать заявку по многоканальному бесплатному номеру 8(800) 555-14- 21<br /> &nbsp;<br /> Если Вы сделали у нас заказ через сайт, то в течение рабочего дня с Вами свяжется менеджер, чтобы обсудить наиболее удобное для Вас время доставки товара и уточнить адрес.<br /> &nbsp;<br /> В случае если Вы сделали заказ в выходной или праздничный день, мы свяжемся с Вами в первый рабочий день.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">&nbsp;</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\"><strong>ДОСТАВКА по МОСКВЕ:</strong></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Минимальная сумма заказа для курьерской доставки 3000 рублей.&nbsp;</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Доставка в пределах МКАД осуществляется курьером, услуга стоит 300 руб. Если сумма вашего заказа более 50000 руб, доставка осуществляется БЕСПЛАТНО. Заказы доставляются по предварительному согласованию с менеджером нашего магазина. Возможность</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">доставки за пределы МКАД рассматривается индивидуально в каждом конкретном случае и зависит от дальности расположения и удобства проезда. Стоимость 300 + 30 рублей за километр от МКАД. Уважаемые клиенты, просьба не задерживать курьера более 20 минут.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">ПОДЪЕМ&nbsp;НА&nbsp;ЭТАЖ: при объемных заказах оплачивается дополнительно, стоимость услуги 200 руб.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\"><strong>Обращаем Ваше внимание&nbsp;</strong>на то, что установка и подключение техники сотрудниками компании, производящей доставку, не осуществляется!<br /> <br /> <strong>ДОСТАВКА товаров по РОССИИ:</strong></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Отправка товара по России осуществляется после 100% оплаты.<br /> <br /> Мы активно работаем с транспортными компаниями.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Сайт компании: <a href=\"http://www.ponyexpress.ru/tariff.php\">PONY EXPRESS</a></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Сайт компании: <a href=\"http://www.spasibo.ru/shipping_calculator/normal/\">СПАСИБО.РУ</a></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Сайт компании:&nbsp;<a style=\"color: #708c28; outline: none;\" href=\"http://www.ae5000.ru/\">АВТОТРЕЙДИНГ</a></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Сайт компании:&nbsp;<a style=\"color: #708c28; outline: none;\" href=\"http://www.pecom.ru/\">ПЭК</a>&nbsp;</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Сайт компании:&nbsp;<a style=\"color: #708c28; outline: none;\" href=\"http://www.dellin.ru/\">ДЕЛОВЫЕ ЛИНИИ</a></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">На сайте компании перевозчика можно рассчитать тариф &nbsp;доставки груза от нашего склада до Вашего города или склада.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Так же можно оформить заявку на забор груза из нашего офиса самостоятельно.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Вес и объем груза Вы сможете узнать только после оплаты и консолидации груза на нашем центральном складе.&nbsp;<br /> <br /> Доставка от нашего склада до транспортной компании для Вас БЕСПЛАТНАЯ.<br /> <br /> Срочный забор и отправка груза через транспортную компанию оплачивается покупателем по тарифам транспортной компании.&nbsp;</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">&nbsp;</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\"><strong>САМОВЫВОЗ:</strong></p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Заказы, осуществляемые самовывозом из нашего офиса, не имеют никаких ограничений по сумме.&nbsp;&nbsp;При этом товар, забираемый самовывозом из нашего офиса, требует 100% предоплаты.</p>\r\n<p style=\"margin: 0px 0px 1em; padding: 0px; font-family: verdana, arial, sans-serif; line-height: 16px;\">Вы можете забрать заказанный товар из нашего офиса по адресу: г.Москва, Ул.Маршала Бирюзова д.1 корп.3 &nbsp;Часы работы с 10 до 18 часов, суббота/воскресение - выходной.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>ПРИЁМ И ВОЗВРАТ ТОВАРА:</strong></p>\r\n<p>Покупатель вправе отказаться от товара в любое время до его передачи, а после его передачи в течение 7 (семи) дней.</p>\r\n<p>Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного товара. При отказе потребителя от товара Продавец обязан возвратить Покупателю</p>\r\n<p>денежную сумму, уплаченную последнем по договору, за исключением расходов продавца на доставку от Покупателя возвращенного товара, не позднее чем через десять дней со дня предъявления Покупателем соответствующего требования.<br /> <br /> Приемка товара по внешнему виду, комплектности, отсутствию механический повреждений в момент передачи товара Покупателю обязательна.<br /> <br /> Обращаем Ваше внимание на то, что установка и подключение светильников сотрудниками компании&nbsp; не осуществляется!<br /> <br /> <strong>ЦЕНЫ И ОПЛАТА:</strong><br /> <img src=\"http://www.svetilniki-online.ru/images/svet/ico_mcard.gif\" alt=\"MasterCard\" width=\"45\" height=\"27\" />&nbsp; <img src=\"http://www.svetilniki-online.ru/images/svet/ico_visa.gif\" alt=\"VISA\" width=\"45\" height=\"26\" /><br /> Цены на товары на сайте указаны в справочных целях и могут, отличатся от официальных прайс-листов, по причине большого ассортимента продукции.<br /> <br /> После формирования заявки согласования с менеджером наличия на складе и сроков поставки заказных позиций, с учетом объёмных накопительных скидок выписывается счёт на оплату от компании ООО\"МАГАЗИН СВЕТ\"</p>\r\n<p>Действует система объёмных и накопительных скидок:</p>\r\n<ul>\r\n<li>от 10 000 руб - 2%</li>\r\n<li>от 20 000 руб - 3%</li>\r\n<li>от 30 000 руб- 4%</li>\r\n<li>от 45 000 руб - 5%</li>\r\n<li>до 100 000 руб - 10%</li>\r\n<li>от 100 000 руб -цена договорная.</li>\r\n</ul>\r\n<p>Компания OOO &laquo;МАГАЗИН СВЕТ&raquo; осуществляет продажу товаров оптом и в розницу.<br /> <br /> Оплата товара для физических лиц производится за наличный расчёт в офисе компании ООО МАГАЗИН СВЕТ&nbsp;или переводом без открытия счёта через любое отделение коммерческого или сбербанка на территории России согласно выставленного счёта!</p>\r\n<p><br /> Оплата товара для юридических лиц по безналичному расчету через банки России согласно выставленного счёта!<br /> <br /> <strong>СРОКИ ПОСТАВКИ:</strong></p>\r\n<p>На товар имеющийся в наличии на складе в МОСКВЕ срок поставки 2-3 дня,<br /> <br /> <span class=\"fck_red\">На товар который идет под заказ или в другой город России сроки уточняйте у менеджеров.</span></p>\r\n<p>&nbsp;</p>');
/*!40000 ALTER TABLE `t_papers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_products_of_day`
--

DROP TABLE IF EXISTS `v_products_of_day`;
/*!50001 DROP VIEW IF EXISTS `v_products_of_day`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_products_of_day` (
  `prod_id` int(10) unsigned,
  `prod_pricev` int(10) unsigned,
  `prod_pricep` int(10) unsigned,
  `prod_name` varchar(45),
  `prod_pic` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_man`
--

DROP TABLE IF EXISTS `t_man`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_man` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `note` varchar(128) DEFAULT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_t_man_1` (`pic_id`),
  CONSTRAINT `FK_t_man_1` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_man`
--

LOCK TABLES `t_man` WRITE;
/*!40000 ALTER TABLE `t_man` DISABLE KEYS */;
INSERT INTO `t_man` VALUES (1,'Сами мутим',NULL,0);
/*!40000 ALTER TABLE `t_man` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_towns`
--

DROP TABLE IF EXISTS `t_towns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_towns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cap` varchar(45) NOT NULL,
  `visibled` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_towns`
--

LOCK TABLES `t_towns` WRITE;
/*!40000 ALTER TABLE `t_towns` DISABLE KEYS */;
INSERT INTO `t_towns` VALUES (1,'Оренбург',1);
/*!40000 ALTER TABLE `t_towns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_cat_list`
--

DROP TABLE IF EXISTS `v_cat_list`;
/*!50001 DROP VIEW IF EXISTS `v_cat_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_cat_list` (
  `cat_id` int(10) unsigned,
  `cat_name` varchar(32),
  `cat_pic` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_type_cat`
--

DROP TABLE IF EXISTS `t_prod_type_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_type_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shortname` varchar(32) NOT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_type_cat_1` (`pic_id`),
  CONSTRAINT `FK_t_prod_type_cat_1` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_type_cat`
--

LOCK TABLES `t_prod_type_cat` WRITE;
/*!40000 ALTER TABLE `t_prod_type_cat` DISABLE KEYS */;
INSERT INTO `t_prod_type_cat` VALUES (14,'Офис',316,'Офисное освещение'),(15,'Дом ',317,'Домашнее освещение');
/*!40000 ALTER TABLE `t_prod_type_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_users`
--

DROP TABLE IF EXISTS `t_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `town_id` int(11) NOT NULL,
  `birth_dt` datetime DEFAULT NULL,
  `reg_dt` datetime NOT NULL,
  `vis_dt` datetime DEFAULT NULL,
  `login` varchar(45) NOT NULL,
  `passwd` varchar(45) NOT NULL,
  `FIO` varchar(45) NOT NULL,
  `nick` varchar(45) NOT NULL,
  `skype` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `icq` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `avatar_ext` varchar(5) DEFAULT NULL,
  `gmt` varchar(6) NOT NULL DEFAULT '+03:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_t_users_t_user_status1` (`status`),
  KEY `fk_t_users_t_user_role1` (`role`),
  KEY `fk_t_users_t_towns1` (`town_id`),
  KEY `fk_t_users_t_gmt1` (`gmt`),
  CONSTRAINT `fk_t_users_t_gmt1` FOREIGN KEY (`gmt`) REFERENCES `t_gmt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_t_users_t_towns1` FOREIGN KEY (`town_id`) REFERENCES `t_towns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_t_users_t_user_role1` FOREIGN KEY (`role`) REFERENCES `t_user_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_t_users_t_user_status1` FOREIGN KEY (`status`) REFERENCES `t_user_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Пользователи';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_users`
--

LOCK TABLES `t_users` WRITE;
/*!40000 ALTER TABLE `t_users` DISABLE KEYS */;
INSERT INTO `t_users` VALUES (6,1,1,1,'1983-07-16 00:00:00','2012-12-04 08:37:48','2012-12-25 11:14:57','admin','*C466EF14D716078FCDA348CB3648F360A5E46719','wlad wlad','wlad wlad',NULL,NULL,NULL,NULL,NULL,'+03:00');
/*!40000 ALTER TABLE `t_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_pictures`
--

DROP TABLE IF EXISTS `v_pictures`;
/*!50001 DROP VIEW IF EXISTS `v_pictures`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_pictures` (
  `pic_id` int(10) unsigned,
  `pic_fname` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prm_cat_prod_user`
--

DROP TABLE IF EXISTS `v_prm_cat_prod_user`;
/*!50001 DROP VIEW IF EXISTS `v_prm_cat_prod_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prm_cat_prod_user` (
  `prmcat_id` int(10) unsigned,
  `prmcat_name` varchar(64),
  `prmcat_pic` varchar(56),
  `prod_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prod_image`
--

DROP TABLE IF EXISTS `v_prod_image`;
/*!50001 DROP VIEW IF EXISTS `v_prod_image`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_image` (
  `prod_id` int(11) unsigned,
  `pic_id` int(11) unsigned,
  `pic_fname` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_param`
--

DROP TABLE IF EXISTS `t_prod_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_param` (
  `id_prm` int(10) unsigned NOT NULL,
  `id_prod` int(10) unsigned NOT NULL,
  `val` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_prm`),
  KEY `FK_t_prod_param_2` (`id_prod`),
  CONSTRAINT `FK_t_prod_param_1` FOREIGN KEY (`id_prm`) REFERENCES `t_prod_prm_ref` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_prod_param_2` FOREIGN KEY (`id_prod`) REFERENCES `t_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_param`
--

LOCK TABLES `t_prod_param` WRITE;
/*!40000 ALTER TABLE `t_prod_param` DISABLE KEYS */;
INSERT INTO `t_prod_param` VALUES (94,17,'150'),(95,17,'500'),(96,17,'3'),(97,17,'галогеновые G9'),(98,17,'3'),(99,17,'можно'),(100,17,'галогеновая'),(101,17,'G9'),(102,17,'пальчиковая точечная'),(103,17,'220'),(104,17,'40'),(105,17,'2800-3200 K'),(106,17,'на 50%'),(107,17,'белый'),(108,17,'матовый'),(109,17,'стекло'),(110,17,'глянцевый'),(111,17,'металл'),(112,17,'I'),(113,17,'120'),(114,17,'20'),(115,17,'комнатная температура');
/*!40000 ALTER TABLE `t_prod_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_prod_main_params`
--

DROP TABLE IF EXISTS `v_prod_main_params`;
/*!50001 DROP VIEW IF EXISTS `v_prod_main_params`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_main_params` (
  `prm_name` varchar(64),
  `prm_val` varchar(64),
  `prod_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_prm_cat`
--

DROP TABLE IF EXISTS `t_prod_prm_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_prm_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ref_typeref` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_prm_cat_2` (`pic_id`),
  KEY `FK_t_prod_prm_cat_1` (`id_ref_typeref`) USING BTREE,
  CONSTRAINT `FK_t_prod_prm_cat_1` FOREIGN KEY (`id_ref_typeref`) REFERENCES `t_prod_type_ref` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_prod_prm_cat_2` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_prm_cat`
--

LOCK TABLES `t_prod_prm_cat` WRITE;
/*!40000 ALTER TABLE `t_prod_prm_cat` DISABLE KEYS */;
INSERT INTO `t_prod_prm_cat` VALUES (25,136,'Общие',0);
/*!40000 ALTER TABLE `t_prod_prm_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_menu_mode`
--

DROP TABLE IF EXISTS `v_menu_mode`;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_menu_mode` (
  `cat_id` int(10) unsigned,
  `cat_mode` int(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_user_role`
--

DROP TABLE IF EXISTS `t_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '1',
  `isworker` int(11) NOT NULL DEFAULT '0',
  `cap` varchar(45) NOT NULL,
  `iscommon` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_role`
--

LOCK TABLES `t_user_role` WRITE;
/*!40000 ALTER TABLE `t_user_role` DISABLE KEYS */;
INSERT INTO `t_user_role` VALUES (1,1,0,'Администратор сайта',0),(2,1,0,'Пользователи',1);
/*!40000 ALTER TABLE `t_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_products`
--

DROP TABLE IF EXISTS `t_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_man` int(10) unsigned NOT NULL,
  `id_type_grp` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pricev` int(10) unsigned NOT NULL DEFAULT '0',
  `pricep` int(10) unsigned NOT NULL DEFAULT '0',
  `d24` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_t_products_1` (`id_man`),
  KEY `FK_t_products_2` (`id_type_grp`) USING BTREE,
  KEY `FK_t_products_3` (`pic_id`),
  CONSTRAINT `FK_t_products_1` FOREIGN KEY (`id_man`) REFERENCES `t_man` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_products_2` FOREIGN KEY (`id_type_grp`) REFERENCES `t_prod_type_ref_grp` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_products_3` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_products`
--

LOCK TABLES `t_products` WRITE;
/*!40000 ALTER TABLE `t_products` DISABLE KEYS */;
INSERT INTO `t_products` VALUES (17,1,51,'111111',328,1256,2347,0);
/*!40000 ALTER TABLE `t_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_man_prod_type_ref_list`
--

DROP TABLE IF EXISTS `v_man_prod_type_ref_list`;
/*!50001 DROP VIEW IF EXISTS `v_man_prod_type_ref_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_man_prod_type_ref_list` (
  `typeref_id` int(10) unsigned,
  `typeref_name` varchar(64),
  `typeref_pic` varchar(56),
  `typeref_subcat_id` int(10) unsigned,
  `typeref_man_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_basket_detail`
--

DROP TABLE IF EXISTS `v_basket_detail`;
/*!50001 DROP VIEW IF EXISTS `v_basket_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_basket_detail` (
  `prod_id` int(10) unsigned,
  `prod_pricev` int(10) unsigned,
  `prod_pricep` int(10) unsigned,
  `prod_name` varchar(45),
  `prod_pic` varchar(56),
  `basket_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prod_params`
--

DROP TABLE IF EXISTS `v_prod_params`;
/*!50001 DROP VIEW IF EXISTS `v_prod_params`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_params` (
  `prod_id` int(10) unsigned,
  `prm_cat_id` int(10) unsigned,
  `prm_id` int(10) unsigned,
  `prm_name` varchar(64),
  `prm_pic` varchar(56),
  `prm_filled` int(10) unsigned,
  `prm_val` varchar(64)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_type_ref`
--

DROP TABLE IF EXISTS `t_prod_type_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_type_ref` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  `owner` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_type_ref_1` (`pic_id`),
  KEY `FK_t_prod_type_ref_2` (`owner`),
  CONSTRAINT `FK_t_prod_type_ref_1` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_prod_type_ref_2` FOREIGN KEY (`owner`) REFERENCES `t_prod_type_subcat` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_type_ref`
--

LOCK TABLES `t_prod_type_ref` WRITE;
/*!40000 ALTER TABLE `t_prod_type_ref` DISABLE KEYS */;
INSERT INTO `t_prod_type_ref` VALUES (136,'Бюджет',320,43),(137,'VIP ',321,44),(138,'Бюджет',323,45),(139,'VIP ',325,46);
/*!40000 ALTER TABLE `t_prod_type_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_pictures`
--

DROP TABLE IF EXISTS `t_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visible` int(10) unsigned NOT NULL DEFAULT '1',
  `extens` varchar(45) NOT NULL,
  `cat` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=329 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_pictures`
--

LOCK TABLES `t_pictures` WRITE;
/*!40000 ALTER TABLE `t_pictures` DISABLE KEYS */;
INSERT INTO `t_pictures` VALUES (0,0,'',0),(316,1,'jpg',1),(317,1,'jpg',1),(318,1,'jpg',2),(319,1,'jpg',2),(320,1,'jpg',3),(321,1,'jpg',3),(322,1,'jpg',2),(323,1,'jpg',3),(324,1,'jpg',2),(325,1,'jpg',3),(326,1,'jpg',5),(327,1,'jpg',5),(328,1,'jpg',5);
/*!40000 ALTER TABLE `t_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_basket`
--

DROP TABLE IF EXISTS `t_basket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_basket` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(45) NOT NULL,
  `dtc` datetime NOT NULL,
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='корзина клиента';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_basket`
--

LOCK TABLES `t_basket` WRITE;
/*!40000 ALTER TABLE `t_basket` DISABLE KEYS */;
INSERT INTO `t_basket` VALUES (69,'25817.87109375','2012-12-25 11:12:54','127.0.0.1'),(70,'976989.74609375','2012-12-25 11:13:04','127.0.0.1'),(71,'567169.189453125','2012-12-25 11:14:57','127.0.0.1');
/*!40000 ALTER TABLE `t_basket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_prod_prm_cat_list`
--

DROP TABLE IF EXISTS `v_prod_prm_cat_list`;
/*!50001 DROP VIEW IF EXISTS `v_prod_prm_cat_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_prm_cat_list` (
  `catprm_id` int(10) unsigned,
  `catprm_name` varchar(64),
  `catprm_pic` varchar(56),
  `catprm_typeref` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prm_cat_prod_admin`
--

DROP TABLE IF EXISTS `v_prm_cat_prod_admin`;
/*!50001 DROP VIEW IF EXISTS `v_prm_cat_prod_admin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prm_cat_prod_admin` (
  `prmcat_id` int(10) unsigned,
  `prmcat_name` varchar(64),
  `prmcat_pic` varchar(56),
  `prod_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_type_subcat`
--

DROP TABLE IF EXISTS `t_prod_type_subcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_type_subcat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_type_subcat_2` (`pic_id`),
  KEY `FK_t_prod_type_subcat_1` (`owner`),
  CONSTRAINT `FK_t_prod_type_subcat_1` FOREIGN KEY (`owner`) REFERENCES `t_prod_type_cat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_prod_type_subcat_2` FOREIGN KEY (`pic_id`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_type_subcat`
--

LOCK TABLES `t_prod_type_subcat` WRITE;
/*!40000 ALTER TABLE `t_prod_type_subcat` DISABLE KEYS */;
INSERT INTO `t_prod_type_subcat` VALUES (43,14,'Бюджет',318),(44,14,'VIP ',319),(45,15,'Бюджет',322),(46,15,'VIP ',324);
/*!40000 ALTER TABLE `t_prod_type_subcat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sessions`
--

DROP TABLE IF EXISTS `t_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sessions` (
  `id` varchar(15) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_activity` datetime NOT NULL,
  `antibot` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`,`ip`),
  KEY `fk_t_sessions_t_users1` (`user_id`),
  CONSTRAINT `fk_t_sessions_t_users1` FOREIGN KEY (`user_id`) REFERENCES `t_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Активные сессии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sessions`
--

LOCK TABLES `t_sessions` WRITE;
/*!40000 ALTER TABLE `t_sessions` DISABLE KEYS */;
INSERT INTO `t_sessions` VALUES ('529388427734','127.0.0.1',6,'2012-12-25 11:04:31',NULL),('734222412109','127.0.0.1',6,'2012-12-25 10:59:01',NULL);
/*!40000 ALTER TABLE `t_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_prod_msgs`
--

DROP TABLE IF EXISTS `t_prod_msgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_msgs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_prod` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `nick` varchar(45) NOT NULL,
  `msg` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_msgs_1` (`id_prod`),
  CONSTRAINT `FK_t_prod_msgs_1` FOREIGN KEY (`id_prod`) REFERENCES `t_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_msgs`
--

LOCK TABLES `t_prod_msgs` WRITE;
/*!40000 ALTER TABLE `t_prod_msgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_prod_msgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_prod_type_subcat_detail`
--

DROP TABLE IF EXISTS `v_prod_type_subcat_detail`;
/*!50001 DROP VIEW IF EXISTS `v_prod_type_subcat_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_type_subcat_detail` (
  `subcat_id` int(10) unsigned,
  `subcat_name` varchar(64),
  `subcat_pic` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_basket_data`
--

DROP TABLE IF EXISTS `t_basket_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_basket_data` (
  `bid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `cnt` decimal(10,0) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bid`,`pid`),
  KEY `FK_t_basket_data_2` (`pid`),
  CONSTRAINT `FK_t_basket_data_1` FOREIGN KEY (`bid`) REFERENCES `t_basket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_t_basket_data_2` FOREIGN KEY (`pid`) REFERENCES `t_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_basket_data`
--

LOCK TABLES `t_basket_data` WRITE;
/*!40000 ALTER TABLE `t_basket_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_basket_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_man_list`
--

DROP TABLE IF EXISTS `v_man_list`;
/*!50001 DROP VIEW IF EXISTS `v_man_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_man_list` (
  `man_id` int(10) unsigned,
  `man_name` varchar(64),
  `man_pic` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_image`
--

DROP TABLE IF EXISTS `t_prod_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_image` (
  `prod_id` int(10) unsigned NOT NULL,
  `pic_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pic_id`),
  KEY `Index_2` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_image`
--

LOCK TABLES `t_prod_image` WRITE;
/*!40000 ALTER TABLE `t_prod_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_prod_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_menu_mode_tmp1`
--

DROP TABLE IF EXISTS `v_menu_mode_tmp1`;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode_tmp1`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_menu_mode_tmp1` (
  `owner` int(10) unsigned,
  `cnt` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_menu_mode_tmp2`
--

DROP TABLE IF EXISTS `v_menu_mode_tmp2`;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode_tmp2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_menu_mode_tmp2` (
  `owner` int(10) unsigned,
  `cnt` bigint(21)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_products_detail`
--

DROP TABLE IF EXISTS `v_products_detail`;
/*!50001 DROP VIEW IF EXISTS `v_products_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_products_detail` (
  `prod_id` int(10) unsigned,
  `prod_name` varchar(45),
  `prod_pricev` int(10) unsigned,
  `prod_pricep` int(10) unsigned,
  `prod_man` int(10) unsigned,
  `prod_24` int(10),
  `prod_pic` varchar(56),
  `man_pic` varchar(56),
  `prod_type_grp` int(10) unsigned,
  `prod_type_ref` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_subcat_list`
--

DROP TABLE IF EXISTS `v_subcat_list`;
/*!50001 DROP VIEW IF EXISTS `v_subcat_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_subcat_list` (
  `subcat_id` int(10) unsigned,
  `subcat_name` varchar(64),
  `subcat_pic` varchar(56),
  `subcat_owner` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_account_req`
--

DROP TABLE IF EXISTS `t_account_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_account_req` (
  `req_id` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  `req_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`req_id`),
  KEY `fk_t_account_req_t_users1` (`user_id`),
  CONSTRAINT `fk_t_account_req_t_users1` FOREIGN KEY (`user_id`) REFERENCES `t_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Запросы на создание аккаунта';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_account_req`
--

LOCK TABLES `t_account_req` WRITE;
/*!40000 ALTER TABLE `t_account_req` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_account_req` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_man_detail`
--

DROP TABLE IF EXISTS `v_man_detail`;
/*!50001 DROP VIEW IF EXISTS `v_man_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_man_detail` (
  `man_id` int(10) unsigned,
  `man_name` varchar(64),
  `man_note` varchar(128),
  `man_pic` varchar(56)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prod_prm_ref_list`
--

DROP TABLE IF EXISTS `v_prod_prm_ref_list`;
/*!50001 DROP VIEW IF EXISTS `v_prod_prm_ref_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_prm_ref_list` (
  `prm_id` int(10) unsigned,
  `prm_name` varchar(64),
  `prm_main` int(10) unsigned,
  `prm_pic` varchar(56),
  `prm_cat_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prod_image_c`
--

DROP TABLE IF EXISTS `v_prod_image_c`;
/*!50001 DROP VIEW IF EXISTS `v_prod_image_c`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_image_c` (
  `prod_id` int(11) unsigned,
  `pic_id` int(11) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_prod_image_b`
--

DROP TABLE IF EXISTS `v_prod_image_b`;
/*!50001 DROP VIEW IF EXISTS `v_prod_image_b`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_image_b` (
  `prod_id` int(11) unsigned,
  `pic_id` int(11) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_prm_ref`
--

DROP TABLE IF EXISTS `t_prod_prm_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_prm_ref` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cat` int(10) unsigned NOT NULL,
  `id_pic` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `is_main` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_prm_ref_2` (`id_pic`),
  KEY `FK_t_prod_prm_ref_1` (`id_cat`),
  CONSTRAINT `FK_t_prod_prm_ref_1` FOREIGN KEY (`id_cat`) REFERENCES `t_prod_prm_cat` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_t_prod_prm_ref_2` FOREIGN KEY (`id_pic`) REFERENCES `t_pictures` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_prm_ref`
--

LOCK TABLES `t_prod_prm_ref` WRITE;
/*!40000 ALTER TABLE `t_prod_prm_ref` DISABLE KEYS */;
INSERT INTO `t_prod_prm_ref` VALUES (94,25,0,'Высота, мм',1),(95,25,0,'Диаметр, мм',1),(96,25,0,'Общее кол-во ламп',1),(97,25,0,'Лампы в комплекте',0),(98,25,0,'Количество плафонов',0),(99,25,0,'Возможность подключения диммера',1),(100,25,0,'Тип лампы 	',0),(101,25,0,'Тип цоколя лампы',0),(102,25,0,'Тип колбы лампы',0),(103,25,0,'Напряжение питания лампы, V',1),(104,25,0,'Максимальная мощность лампы, W',1),(105,25,0,'Цветовая температура, K',1),(106,25,0,'Экономичнее лампы накаливания',0),(107,25,0,'Цвет плафонов и подвесок',0),(108,25,0,'Тип поверхности плафонов',0),(109,25,0,'Материал плафонов и подвесок',0),(110,25,0,'Тип поверхности арматуры',0),(111,25,0,'Материал арматуры',0),(112,25,0,'Класс электробезопасности',0),(113,25,0,'Общая мощность, W',0),(114,25,0,'Степень пылевлагозащиты, IP',0),(115,25,0,'Диапазон рабочих температур',0);
/*!40000 ALTER TABLE `t_prod_prm_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_man_subcats`
--

DROP TABLE IF EXISTS `v_man_subcats`;
/*!50001 DROP VIEW IF EXISTS `v_man_subcats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_man_subcats` (
  `subcat_id` int(10) unsigned,
  `subcat_name` varchar(64),
  `subcat_pic` varchar(56),
  `man_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `t_prod_type_ref_grp`
--

DROP TABLE IF EXISTS `t_prod_type_ref_grp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_prod_type_ref_grp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `owner` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t_prod_type_ref_grp_1` (`owner`),
  CONSTRAINT `FK_t_prod_type_ref_grp_1` FOREIGN KEY (`owner`) REFERENCES `t_prod_type_ref` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_prod_type_ref_grp`
--

LOCK TABLES `t_prod_type_ref_grp` WRITE;
/*!40000 ALTER TABLE `t_prod_type_ref_grp` DISABLE KEYS */;
INSERT INTO `t_prod_type_ref_grp` VALUES (51,'Общая',136);
/*!40000 ALTER TABLE `t_prod_type_ref_grp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_prod_type_ref_detail`
--

DROP TABLE IF EXISTS `v_prod_type_ref_detail`;
/*!50001 DROP VIEW IF EXISTS `v_prod_type_ref_detail`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_prod_type_ref_detail` (
  `typeref_name` varchar(64),
  `typeref_owner` int(10) unsigned,
  `typeref_pic` varchar(56),
  `typeref_id` int(10) unsigned
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_products_of_day`
--

/*!50001 DROP TABLE IF EXISTS `v_products_of_day`*/;
/*!50001 DROP VIEW IF EXISTS `v_products_of_day`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_products_of_day` AS select `prod`.`id` AS `prod_id`,`prod`.`pricev` AS `prod_pricev`,`prod`.`pricep` AS `prod_pricep`,`prod`.`name` AS `prod_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prod_pic` from ((`t_products` `prod` join `t_prodofday` `pod` on((`pod`.`pid` = `prod`.`id`))) left join `t_pictures` `pic` on(((`prod`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_cat_list`
--

/*!50001 DROP TABLE IF EXISTS `v_cat_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_cat_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_cat_list` AS select `cat`.`id` AS `cat_id`,`cat`.`shortname` AS `cat_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `cat_pic` from (`t_prod_type_cat` `cat` left join `t_pictures` `pic` on(((`cat`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pictures`
--

/*!50001 DROP TABLE IF EXISTS `v_pictures`*/;
/*!50001 DROP VIEW IF EXISTS `v_pictures`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pictures` AS select `pic`.`id` AS `pic_id`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `pic_fname` from `t_pictures` `pic` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prm_cat_prod_user`
--

/*!50001 DROP TABLE IF EXISTS `v_prm_cat_prod_user`*/;
/*!50001 DROP VIEW IF EXISTS `v_prm_cat_prod_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prm_cat_prod_user` AS select `prmcat`.`id` AS `prmcat_id`,`prmcat`.`name` AS `prmcat_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prmcat_pic`,`prod`.`id` AS `prod_id` from (`t_products` `prod` join (`t_prod_prm_cat` `prmcat` left join `t_pictures` `pic` on(((`prmcat`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1))))) where `prmcat`.`id` in (select `t_prod_prm_ref`.`id_cat` from `t_prod_prm_ref` where `t_prod_prm_ref`.`id` in (select `t_prod_param`.`id_prm` from `t_prod_param` where (`t_prod_param`.`id_prod` = `prod`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_image`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_image`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_image`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_image` AS select `pi`.`prod_id` AS `prod_id`,`pi`.`pic_id` AS `pic_id`,`vp`.`pic_fname` AS `pic_fname` from (`v_prod_image_c` `pi` join `v_pictures` `vp` on((`vp`.`pic_id` = `pi`.`pic_id`))) order by `pi`.`pic_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_main_params`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_main_params`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_main_params`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_main_params` AS select `ppr`.`name` AS `prm_name`,`prm`.`val` AS `prm_val`,`prm`.`id_prod` AS `prod_id` from (`t_prod_prm_ref` `ppr` join `t_prod_param` `prm`) where ((`ppr`.`is_main` = 1) and (`prm`.`id_prm` = `ppr`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_menu_mode`
--

/*!50001 DROP TABLE IF EXISTS `v_menu_mode`*/;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_menu_mode` AS select `c`.`id` AS `cat_id`,(`tr`.`cnt` <= `sc`.`cnt`) AS `cat_mode` from ((`t_prod_type_cat` `c` left join `v_menu_mode_tmp1` `sc` on((`c`.`id` = `sc`.`owner`))) left join `v_menu_mode_tmp2` `tr` on((`c`.`id` = `tr`.`owner`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_man_prod_type_ref_list`
--

/*!50001 DROP TABLE IF EXISTS `v_man_prod_type_ref_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_man_prod_type_ref_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_man_prod_type_ref_list` AS select `typeref`.`id` AS `typeref_id`,`typeref`.`name` AS `typeref_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `typeref_pic`,`typeref`.`owner` AS `typeref_subcat_id`,`man`.`id` AS `typeref_man_id` from (`t_man` `man` join (`t_prod_type_ref` `typeref` left join `t_pictures` `pic` on(((`typeref`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1))))) where `typeref`.`id` in (select `trg`.`owner` from (`t_products` `mrg` join `t_prod_type_ref_grp` `trg` on((`mrg`.`id_type_grp` = `trg`.`id`))) where (`mrg`.`id_man` = `man`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_basket_detail`
--

/*!50001 DROP TABLE IF EXISTS `v_basket_detail`*/;
/*!50001 DROP VIEW IF EXISTS `v_basket_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_basket_detail` AS select `prod`.`id` AS `prod_id`,`prod`.`pricev` AS `prod_pricev`,`prod`.`pricep` AS `prod_pricep`,`prod`.`name` AS `prod_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prod_pic`,`bsk`.`bid` AS `basket_id` from ((`t_products` `prod` join `t_basket_data` `bsk` on((`bsk`.`pid` = `prod`.`id`))) left join `t_pictures` `pic` on(((`prod`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_params`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_params`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_params`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_params` AS select `prod`.`id` AS `prod_id`,`vpr`.`prm_cat_id` AS `prm_cat_id`,`vpr`.`prm_id` AS `prm_id`,`vpr`.`prm_name` AS `prm_name`,`vpr`.`prm_pic` AS `prm_pic`,`prm`.`id_prm` AS `prm_filled`,`prm`.`val` AS `prm_val` from ((`t_products` `prod` join `v_prod_prm_ref_list` `vpr` on((1 = 1))) left join `t_prod_param` `prm` on(((`prm`.`id_prm` = `vpr`.`prm_id`) and (`prm`.`id_prod` = `prod`.`id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_prm_cat_list`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_prm_cat_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_prm_cat_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_prm_cat_list` AS select `cp`.`id` AS `catprm_id`,`cp`.`name` AS `catprm_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `catprm_pic`,`cp`.`id_ref_typeref` AS `catprm_typeref` from (`t_prod_prm_cat` `cp` left join `t_pictures` `pic` on(((`cp`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prm_cat_prod_admin`
--

/*!50001 DROP TABLE IF EXISTS `v_prm_cat_prod_admin`*/;
/*!50001 DROP VIEW IF EXISTS `v_prm_cat_prod_admin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prm_cat_prod_admin` AS select `c`.`id` AS `prmcat_id`,`c`.`name` AS `prmcat_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prmcat_pic`,`p`.`id` AS `prod_id` from (((`t_products` `p` join `t_prod_type_ref_grp` `g` on((`g`.`id` = `p`.`id_type_grp`))) join `t_prod_prm_cat` `c` on((`c`.`id_ref_typeref` = `g`.`owner`))) left join `t_pictures` `pic` on(((`c`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_type_subcat_detail`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_type_subcat_detail`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_type_subcat_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_type_subcat_detail` AS select `subcat`.`id` AS `subcat_id`,`subcat`.`name` AS `subcat_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `subcat_pic` from (`t_prod_type_subcat` `subcat` left join `t_pictures` `pic` on(((`subcat`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_man_list`
--

/*!50001 DROP TABLE IF EXISTS `v_man_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_man_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_man_list` AS select `man`.`id` AS `man_id`,`man`.`name` AS `man_name`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `man_pic` from (`t_man` `man` left join `t_pictures` `pic` on(((`man`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_menu_mode_tmp1`
--

/*!50001 DROP TABLE IF EXISTS `v_menu_mode_tmp1`*/;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode_tmp1`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_menu_mode_tmp1` AS select `t_prod_type_subcat`.`owner` AS `owner`,count(1) AS `cnt` from `t_prod_type_subcat` group by `t_prod_type_subcat`.`owner` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_menu_mode_tmp2`
--

/*!50001 DROP TABLE IF EXISTS `v_menu_mode_tmp2`*/;
/*!50001 DROP VIEW IF EXISTS `v_menu_mode_tmp2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_menu_mode_tmp2` AS select `sc`.`owner` AS `owner`,count(1) AS `cnt` from (`t_prod_type_subcat` `sc` join `t_prod_type_ref` `tr` on((`tr`.`owner` = `sc`.`id`))) group by `sc`.`owner` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_products_detail`
--

/*!50001 DROP TABLE IF EXISTS `v_products_detail`*/;
/*!50001 DROP VIEW IF EXISTS `v_products_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_products_detail` AS select `prod`.`id` AS `prod_id`,`prod`.`name` AS `prod_name`,`prod`.`pricev` AS `prod_pricev`,`prod`.`pricep` AS `prod_pricep`,`prod`.`id_man` AS `prod_man`,`prod`.`d24` AS `prod_24`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prod_pic`,concat(ifnull(`picman`.`id`,'0'),'.',ifnull(`picman`.`extens`,'png')) AS `man_pic`,`prod`.`id_type_grp` AS `prod_type_grp`,`typeref`.`owner` AS `prod_type_ref` from ((((`t_products` `prod` join `t_man` `man` on((`man`.`id` = `prod`.`id_man`))) left join `t_pictures` `pic` on(((`prod`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) left join `t_pictures` `picman` on(((`man`.`pic_id` = `picman`.`id`) and (`pic`.`visible` = 1)))) left join `t_prod_type_ref_grp` `typeref` on((`prod`.`id_type_grp` = `typeref`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_subcat_list`
--

/*!50001 DROP TABLE IF EXISTS `v_subcat_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_subcat_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_subcat_list` AS select `subcat`.`id` AS `subcat_id`,`subcat`.`name` AS `subcat_name`,concat(`pic`.`id`,'.',`pic`.`extens`) AS `subcat_pic`,`subcat`.`owner` AS `subcat_owner` from (`t_prod_type_subcat` `subcat` left join `t_pictures` `pic` on(((`subcat`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_man_detail`
--

/*!50001 DROP TABLE IF EXISTS `v_man_detail`*/;
/*!50001 DROP VIEW IF EXISTS `v_man_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_man_detail` AS select `man`.`id` AS `man_id`,`man`.`name` AS `man_name`,`man`.`note` AS `man_note`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `man_pic` from (`t_man` `man` left join `t_pictures` `pic` on(((`man`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_prm_ref_list`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_prm_ref_list`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_prm_ref_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_prm_ref_list` AS select `prm`.`id` AS `prm_id`,`prm`.`name` AS `prm_name`,`prm`.`is_main` AS `prm_main`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `prm_pic`,`prm`.`id_cat` AS `prm_cat_id` from (`t_prod_prm_ref` `prm` left join `t_pictures` `pic` on(((`prm`.`id_pic` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_image_c`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_image_c`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_image_c`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_image_c` AS select `v_prod_image_b`.`prod_id` AS `prod_id`,`v_prod_image_b`.`pic_id` AS `pic_id` from `v_prod_image_b` group by `v_prod_image_b`.`prod_id`,`v_prod_image_b`.`pic_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_image_b`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_image_b`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_image_b`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_image_b` AS select `t_products`.`id` AS `prod_id`,`t_products`.`pic_id` AS `pic_id` from `t_products` union select `t_prod_image`.`prod_id` AS `prod_id`,`t_prod_image`.`pic_id` AS `pic_id` from `t_prod_image` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_man_subcats`
--

/*!50001 DROP TABLE IF EXISTS `v_man_subcats`*/;
/*!50001 DROP VIEW IF EXISTS `v_man_subcats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_man_subcats` AS select `subcat`.`id` AS `subcat_id`,`subcat`.`name` AS `subcat_name`,concat(`pic`.`id`,'.',`pic`.`extens`) AS `subcat_pic`,`man`.`id` AS `man_id` from (`t_man` `man` join (`t_prod_type_subcat` `subcat` left join `t_pictures` `pic` on(((`subcat`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1))))) where `subcat`.`id` in (select `ptr`.`owner` from `t_prod_type_ref` `ptr` where `ptr`.`id` in (select `trg`.`owner` from (`t_products` `mrg` join `t_prod_type_ref_grp` `trg` on((`mrg`.`id_type_grp` = `trg`.`id`))) where (`mrg`.`id_man` = `man`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_prod_type_ref_detail`
--

/*!50001 DROP TABLE IF EXISTS `v_prod_type_ref_detail`*/;
/*!50001 DROP VIEW IF EXISTS `v_prod_type_ref_detail`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_prod_type_ref_detail` AS select `tr`.`name` AS `typeref_name`,`tr`.`owner` AS `typeref_owner`,concat(ifnull(`pic`.`id`,'0'),'.',ifnull(`pic`.`extens`,'png')) AS `typeref_pic`,`tr`.`id` AS `typeref_id` from (`t_prod_type_ref` `tr` left join `t_pictures` `pic` on(((`tr`.`pic_id` = `pic`.`id`) and (`pic`.`visible` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'lights'
--

--
-- Dumping routines for database 'lights'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-26 10:32:49
