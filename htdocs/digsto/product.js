//global variables
var combinations = new Array();
var selectedCombination = new Array();
var globalQuantity = new Number;
var colors = new Array();
var input_save_customized_datas = '';

//check if a function exists
function function_exists(function_name)
{
        if (typeof function_name == 'string')
                return (typeof window[function_name] == 'function');
        return (function_name instanceof Function);
}

//execute oosHook js code
function oosHookJsCode()
{
        for (var i = 0; i < oosHookJsCodeFunctions.length; i++)
        {
                if (function_exists(oosHookJsCodeFunctions[i]))
                        setTimeout(oosHookJsCodeFunctions[i]+'()', 0);
        }
}

//add a combination of attributes in the global JS sytem
function addCombination(idCombination, arrayOfIdAttributes, quantity, price, ecotax, id_image, reference, unit_price, minimal_quantity)
{
        globalQuantity += quantity;

        var combination = new Array();
        combination['idCombination'] = idCombination;
        combination['quantity'] = quantity;
        combination['idsAttributes'] = arrayOfIdAttributes;
        combination['price'] = price;
        combination['ecotax'] = ecotax;
        combination['image'] = id_image;
        combination['reference'] = reference;
        combination['unit_price'] = unit_price;
        combination['minimal_quantity'] = minimal_quantity;
        combinations.push(combination);

}

// search the combinations' case of attributes and update displaying of availability, prices, ecotax, and image
function findCombination(firstTime)
{
refreshProductImages(0);
}

function updateColorSelect(id_attribute)
{
        if (id_attribute == 0)
        {
                refreshProductImages(0);
                return ;
        }
        // Visual effect
        $('#color_'+id_attribute).fadeTo('fast', 1, function(){ $(this).fadeTo('slow', 0, function(){ $(this).fadeTo('slow', 1, function(){}); }); });
        // Attribute selection
        $('#group_'+id_color_default+' option[value='+id_attribute+']').attr('selected', 'selected');
        $('#group_'+id_color_default+' option[value!='+id_attribute+']').removeAttr('selected');
        findCombination();
}


//update display of the large image
function displayImage(domAAroundImgThumb)
{
    if (domAAroundImgThumb.attr('href'))
    {
        var newSrc = domAAroundImgThumb.attr('href').replace('thickbox','large');
        if ($('#bigpic').attr('src') != newSrc)
        {
            $('#bigpic').fadeIn('fast', function(){
                $(this).attr('src', newSrc).show();
                if (typeof(jqZoomEnabled) != 'undefined' && jqZoomEnabled)
                        $(this).attr('alt', domAAroundImgThumb.attr('href'));
            });
        }
        $('#views_block li a').removeClass('shown');
        $(domAAroundImgThumb).addClass('shown');
    }
}

// Serialscroll exclude option bug ?
function serialScrollFixLock(event, targeted, scrolled, items, position)
{
        serialScrollNbImages = $('#thumbs_list li:visible').length;
        serialScrollNbImagesDisplayed = 3;

        var leftArrow = position == 0 ? true : false;
        var rightArrow = position + serialScrollNbImagesDisplayed >= serialScrollNbImages ? true : false;

        $('a#view_scroll_left').css('cursor', leftArrow ? 'default' : 'pointer').css('display', leftArrow ? 'none' : 'block').fadeTo(0, leftArrow ? 0 : 1);
        $('a#view_scroll_right').css('cursor', rightArrow ? 'default' : 'pointer').fadeTo(0, rightArrow ? 0 : 1).css('display', rightArrow ? 'none' : 'block');
        return true;
}



// Change the current product images regarding the combination selected
function refreshProductImages(id_product_attribute)
{
    $('#thumbs_list_frame').scrollTo('li:eq(0)', 700, {axis:'x'});

    vv=$('ul#thumbs_list_frame li');
    i=vv.size();
    vv.each(function(){$(this).show();});


    if (i > 0)
    {
        var thumb_width = $('#thumbs_list_frame >li').width()+parseInt($('#thumbs_list_frame >li').css('marginRight'));

        $('#thumbs_list_frame').width((parseInt((thumb_width)* i) - 10) + 'px'); //  Bug IE6, needs 3 pixels more ?
    }
    else
    {
//      $('#thumbnail_' + idDefaultImage).show();
//      displayImage($('#thumbnail_'+ idDefaultImage +' a'));
    }
    $('#thumbs_list').trigger('goto', 0);
    serialScrollFixLock('', '', '', '', 0);// SerialScroll Bug on goto 0 ?
}




//To do after loading HTML
$(document).ready(function()
{
        //init the serialScroll for thumbs
        $('#thumbs_list').serialScroll({
                items:'li:visible',
                prev:'a#view_scroll_left',
                next:'a#view_scroll_right',
                axis:'x',
                offset:0,
                start:0,
                stop:true,
                onBefore:serialScrollFixLock,
                duration:700,
                step: 2,
                lazy: true,
                lock: false,
                force:false,
                cycle:false
        });

        $('#thumbs_list').trigger('goto', 1);// SerialScroll Bug on goto 0 ?
        $('#thumbs_list').trigger('goto', 0);

        //hover 'other views' images management
        $('#views_block li a').hover(
                function(){displayImage($(this));},
                function(){}
        );

        //set jqZoom parameters if needed
        if (typeof(jqZoomEnabled) != 'undefined' && jqZoomEnabled)
        {
                $('img.jqzoom').jqueryzoom({
                        xzoom: 200, //zooming div default width(default width value is 200)
                        yzoom: 200, //zooming div default width(default height value is 200)
                        offset: 21 //zooming div default offset(default offset value is 10)
                        //position: "right" //zooming div position(default position value is "right")
                });
        }

        //add a link on the span 'view full size' and on the big image
        $('span#view_full_size, div#image-block img').click(function(){
                $('#views_block li a.shown').click();
        });

        refreshProductImages(0);


        $('.thickbox').fancybox({
                'hideOnContentClick': true,
                'transitionIn'  : 'elastic',
                'transitionOut' : 'elastic'
        });
});

