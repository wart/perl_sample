tinyMCE.addI18n(
{ ru:
  {
    common:{
    edit_confirm:"Do you want to use the WYSIWYG mode for this textarea?",
    apply:"���������",
    insert:"��������",
    update:"�������",
    cancel:"������",
    close:"�������",
    browse:"�����������",
    class_name:"Class",
    not_set:"-- �� ������� --",
    clipboard_msg:"�����������/�������/������� �� �������� ��� Mozilla and Firefox.\n������ ������ ������ �� ����?",
    clipboard_no_support:"��������� �� �������������� ����� ���������, ����������� ������� �������.",
    popup_blocked:"�������� ����������� ���� �������������, ��� ����������� ���������������� ���������� ��������� ����������.",
    invalid_data:"{#field} �� ���������",
    invalid_data_number:"{#field} ������ ���� ������� �����",
    invalid_data_min:"{#field} ������ ���� ����� ������ ��� {#min}",
    invalid_data_size:"{#field} ������ ���� ������ ��� ����������",
    more_colors:"������ ������"
  },
  colors:
  {
    '000000':'������',
    '993300':'Burnt orange',
    '333300':'Dark olive',
    '003300':'Dark green',
    '003366':'Dark azure',
    '000080':'Navy Blue',
    '333399':'������',
    '333333':'Very dark gray',
    '800000':'Maroon',
    'FF6600':'���������',
    '808000':'Olive',
    '008000':'�������',
    '008080':'Teal',
    '0000FF':'Blue',
    '666699':'Grayish blue',
    '808080':'Gray',
    'FF0000':'�������',
    'FF9900':'Amber',
    '99CC00':'Yellow green',
    '339966':'Sea green',
    '33CCCC':'Turquoise',
    '3366FF':'Royal blue',
    '800080':'Purple',
    '999999':'Medium gray',
    'FF00FF':'Magenta',
    'FFCC00':'�������',
    'FFFF00':'������',
    '00FF00':'����',
    '00FFFF':'����',
    '00CCFF':'Sky blue',
    '993366':'Brown',
    'C0C0C0':'�����������',
    'FF99CC':'Pink',
    'FFCC99':'Peach',
    'FFFF99':'������ ������',
    'CCFFCC':'Pale green',
    'CCFFFF':'Pale cyan',
    '99CCFF':'Light sky blue',
    'CC99FF':'Plum',
    'FFFFFF':'�����'
  },
  contextmenu:
  {
    align:"���������",
    left:"����",
    center:"�����",
    right:"�����",
    full:"������"
  },
  insertdatetime:
  {
    date_fmt:"%Y-%m-%d",
    time_fmt:"%H:%M:%S",
    insertdate_desc:"�������� ����",
    inserttime_desc:"�������� �����",
    months_long:"������,�������,����,������,���,����,����,������,��������,�������,������,�������",
    months_short:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec",
    day_long:"�����������,�����������,�������,�����,�������,�������,�������,�����������",
    day_short:"��,��,��,��,��,��,��,��"
  },
  print:
  {
    print_desc:"������"
  },
  preview:{
    preview_desc:"��������"
  },
  directionality:{
    ltr_desc:"����� �� �����",
    rtl_desc:"������ �� ����"
  },
  layer:{
    insertlayer_desc:"�������� ����� ����",
    forward_desc:"����������� ������",
    backward_desc:"����������� �����",
    absolute_desc:"����������� ���������� �������",
    content:"����� ����..."
  },
  save:{
    save_desc:"���������",
    cancel_desc:"������ ���� ���������"
  },
  nonbreaking:{
    nonbreaking_desc:"Insert non-breaking space character"
  },
  iespell:{
    iespell_desc:"��������� �������� ����������",
    download:"ieSpell �� ������. ������ ���������� ��� ������?"
  },
  advhr:{
    advhr_desc:"�������������� �������"
  },
  emotions:{
    emotions_desc:"������"
  },
  searchreplace:{
    search_desc:"�����",
    replace_desc:"�����/��������"
  },
  advimage:{
    image_desc:"��������/�������� �����������"
  },
  advlink:{
    link_desc:"��������/�������� ������"
  },
  xhtmlxtras:{
    cite_desc:"������",
    abbr_desc:"������������",
    acronym_desc:"Acronym",
    del_desc:"��������",
    ins_desc:"�������",
    attribs_desc:"��������/�������� ���������"
  },
  style:{
    desc:"������������� CSS �����"
  },
  paste:{
    paste_text_desc:"�������� ��� ����",
    paste_word_desc:"�������� �� �����",
    selectall_desc:"�������� ���",
    plaintext_mode_sticky:"Paste is now in plain text mode. Click again to toggle back to regular paste mode. After you paste something you will be returned to regular paste mode.",
    plaintext_mode:"Paste is now in plain text mode. Click again to toggle back to regular paste mode."
  },
  paste_dlg:{
    text_title:"����������� CTRL+V �� ����� ���������� ��� ������� ������ � ����.",
    text_linebreaks:"�������� ������� �����",
    word_title:"����������� CTRL+V �� ����� ���������� ��� ������� ������ � ����."
  },
  table:{
    desc:"�������� ����� �������",
    row_before_desc:"�������� ������ �����",
    row_after_desc:"�������� ������ �����",
    delete_row_desc:"������� ������",
    col_before_desc:"�������� ������� �����",
    col_after_desc:"�������� ������� �����",
    delete_col_desc:"������� �������",
    split_cells_desc:"���������� ����������� ������",
    merge_cells_desc:"���������� ������",
    row_desc:"�������� ������ �������",
    cell_desc:"�������� ������ �������",
    props_desc:"�������� �������",
    paste_row_before_desc:"�������� ������ ������� �����",
    paste_row_after_desc:"�������� ������ ������� �����",
    cut_row_desc:"�������� ������ �������",
    copy_row_desc:"���������� ������ �������",
    del:"������� �������",
    row:"Row",
    col:"Column",
    cell:"Cell"
  },
  autosave:{
    unload_msg:"��������� ������������� ���� ����� �������� ���� �� ��������� � ���� �������",
    restore_content:"Restore auto-saved content.",
    warning_message:"If you restore the saved content, you will lose all the content that is currently in the editor.\n\nAre you sure you want to restore the saved content?."
  },
  fullscreen:{
    desc:"����������� ������������� �����"
  },
  media:{
    desc:"Insert / edit embedded media",
    edit:"Edit embedded media"
  },
  fullpage:{
    desc:"�������� ���������"
  },
  template:{
    desc:"�������� ������"
  },
  visualchars:{
    desc:"Visual control characters on/off."
  },
  spellchecker:{
    desc:"Toggle spellchecker",
    menu:"Spellchecker settings",
    ignore_word:"������������ �����",
    ignore_words:"������������ ���",
    langs:"�����",
    wait:"��������� �����...",
    sug:"Suggestions",
    no_sug:"No suggestions",
    no_mpell:"No misspellings found.",
    learn_word:"Learn word"
  },
  pagebreak:{
    desc:"�������� ������ �������."
  },
  advlist:{
    types:"Types",
    def:"Default",
    lower_alpha:"Lower alpha",
    lower_greek:"Lower greek",
    lower_roman:"Lower roman",
    upper_alpha:"Upper alpha",
    upper_roman:"Upper roman",
    circle:"Circle",
    disc:"Disc",
    square:"Square"
  },
  aria:{
    rich_text_area:"Rich Text Area"
  },
  wordcount:{
    words: '�����: '
  }
});