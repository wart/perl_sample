
tinyMCE.init(
{
 // General options
  mode : "textareas",
  theme : "advanced",
  plugins : "autolink,lists,pagebreak,style,table,save,advhr,advimage,advlink,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
  language:"en",
  // Theme options
   theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
   theme_advanced_buttons2 : "cut,copy,paste,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,forecolor,backcolor",
   theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,|,fullscreen",
   theme_advanced_toolbar_location : "top",
   theme_advanced_toolbar_align : "left",
   theme_advanced_statusbar_location : "none",
   theme_advanced_resizing : true
        });
